<?php
$conn;
if (!isset($_SESSION))
	session_start();
/**
 * create a new DatabaseConnetion, if no one exists
 * @return PDO
 */
function connect() {
	if(!isset($conn)) {
		require_once(__DIR__ . '/../keys.php');

		$SERVER = getServerAddress();
		$DBNAME = getDatabaseName();
		$DB_USER = getDatabaseUser();
		$DB_PASSWORD = getDatabasePassword();

		$conn = new PDO ("mysql:host=$SERVER;dbname=$DBNAME;charset=utf8", $DB_USER, $DB_PASSWORD);

		$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	return $conn;
}