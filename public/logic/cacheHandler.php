<?php

function putIntoCache($content) {
  $fileName = uniqid().'.txt';

  file_put_contents($fileName, $content);

  return $fileName;
}

function getFromCache($fileName) {
  return file_get_contents($fileName);
}

function getFromCacheAndDelete($fileName) {
  $content = getFromCache($fileName);

  unlink($fileName);
  return $content;
}