<?php
require_once(__DIR__ . '/../dbConnector.php');
require_once(__DIR__ . '/../parser.php');
require_once(__DIR__ . '/../pollHandler.php');
require_once(__DIR__ . '/../userHandler.php');

if (isset($_REQUEST) and isset($_REQUEST['action'])) {
  switch ($_REQUEST['action']) {
    case 'assignGroupToPoll':
      $pollID = $_REQUEST['pollID'];
      $groupID = $_REQUEST['groupID'];
      assignPollToGroup($pollID, $groupID);

      break;
    case 'unassignGroupFromPoll':
      $pollID = $_REQUEST['pollID'];
      $groupID = $_REQUEST['groupID'];
      $res = unassignPollFromGroup($pollID, $groupID);

      break;
    case 'assignSubGroupToPoll':
      $pollID = $_REQUEST['pollID'];
      $subGroupID = $_REQUEST['subGroupID'];
      assignPollToSubGroup($pollID, $subGroupID);

      break;
    case 'unassignSubGroupFromPoll':
      $pollID = $_REQUEST['pollID'];
      $subGroupID = $_REQUEST['subGroupID'];
      $res = unassignPollFromSubGroup($pollID, $subGroupID);

      break;
    case 'createPoll':
      $title = $_REQUEST['title'];
      $description = $_REQUEST['description'];
      $finishDate = $_REQUEST['finishDate'];

      $res = createPoll($title, $description, $finishDate);

      break;
    case 'deletePoll':
      $res = deletePoll($_REQUEST['pollID']);

      break;

    case 'editTitleFromPoll':
      $pollID = $_REQUEST['pollID'];
      $title = $_REQUEST['title'];

      $res = editTitleFromPoll($pollID, $title);
      break;

    case 'editDateFromPoll':
      $pollID = $_REQUEST['pollID'];
      $date = $_REQUEST['date'];

      $res = editDateFromPoll($pollID, $date);
      break;

    case 'editDescriptionFromPoll':
      $pollID = $_REQUEST['pollID'];
      $description = $_REQUEST['description'];

      $res = editDescriptionFromPoll($pollID, $description);
      break;
  }
  echo $res;
}

function assignPollToSubGroup($pollID, $subgroupID)
{
  if (!getPermissions()['canManagePolls'] == 1 and !getPermissions()['isAdmin'] == 1) {
    return false;
  }

  $conn = connect();

  $stmt = $conn->prepare('INSERT INTO polls_for_subgroups(pollID, subgroupID) VALUES(:pollID, :subgroupID);');

  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);
  $stmt->bindParam(':subgroupID', $subgroupID, PDO::PARAM_INT);

  $stmt->execute();

  return true;
}

function assignPollToGroup($pollID, $groupID)
{
  if (!getPermissions()['canManagePolls'] == 1 and !getPermissions()['isAdmin'] == 1) {
    return false;
  }

  $conn = connect();

  $stmt = $conn->prepare('INSERT INTO polls_for_groups(pollID, groupID) VALUES(:pollID, :groupID);');

  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);
  $stmt->bindParam(':groupID', $groupID, PDO::PARAM_INT);

  $stmt->execute();
}

function unassignPollFromGroup($pollID, $groupID) {
  if (!getPermissions()['canManagePolls'] == 1 and !getPermissions()['isAdmin'] == 1) {
    return false;
  }

  $conn = connect();

  $stmt = $conn->prepare('DELETE FROM polls_for_groups WHERE pollID = :pollID AND groupID = :groupID');

  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);
  $stmt->bindParam(':groupID', $groupID, PDO::PARAM_INT);

  $stmt->execute();

  return true;
}

function unassignPollFromSubGroup($pollID, $subGroupID) {
  if (!getPermissions()['canManagePolls'] == 1 and !getPermissions()['isAdmin'] == 1) {
    return false;
  }

  $conn = connect();

  $stmt = $conn->prepare('DELETE FROM polls_for_subgroups WHERE pollID = :pollID AND subgroupID = :subgroupID');

  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);
  $stmt->bindParam(':subgroupID', $subGroupID, PDO::PARAM_INT);

  $stmt->execute();

  return true;
}

function createPoll($title, $description, $finishDate)
{
  if (!getPermissions()['canManagePolls'] == 1 and !getPermissions()['isAdmin'] == 1) {
    return "notEnoughPermissions";
  }

  if(!isValidPollTitle($title)) {
    return "valid_pollTitle";
  }

  if(!isValidDate($finishDate)) {
    return 'valid_date';
  }

  $conn = connect();
  $stmt = $conn->prepare('INSERT INTO polls(title, description, finishDate) VALUES (:title, :description, :finishDate)');

  $stmt->bindParam(':title', $title, PDO::PARAM_STR, 64);
  $stmt->bindParam(':description', $description, PDO::PARAM_STR);
  $stmt->bindParam(':finishDate', $finishDate, PDO::PARAM_INT);

  $stmt->execute();

  $stmt = $conn->prepare('SELECT pollID FROM polls WHERE title = :title AND description = :description AND finishDate = :finishDate');
  $stmt->bindParam(':title', $title, PDO::PARAM_STR);
  $stmt->bindParam(':description', $description, PDO::PARAM_STR);
  $stmt->bindParam(':finishDate', $finishDate, PDO::PARAM_INT);

  $stmt->execute();
  return $stmt->fetch()['pollID'];
}

/**
 * removes a poll
 * @param $pollID  the pollID of the poll to remove
 * @return string  if successfully removed 'true', else 'false'
 */
function deletePoll($pollID) {
  if (!getPermissions()['canManagePolls'] == 1 and !getPermissions()['isAdmin'] == 1) {
    return "notEnoughPermissions";
  }

  $conn = connect();

  $stmt = $conn->prepare('DELETE FROM `polls` WHERE `pollID` = :pollID');

  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);

  if ($stmt->execute())
    return true;
  else
    return false;
}

function editTitleFromPoll($pollID, $title) {
  if (!getPermissions()['canManagePolls'] == 1 and !getPermissions()['isAdmin'] == 1) {
    return false;
  }

  if(!isValidPollTitle($title)) {
    return false;
  }

  $conn = connect();

  $stmt = $conn->prepare('UPDATE polls SET title=:title WHERE pollID=:pollID;');

  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);
  $stmt->bindParam(':title', $title, PDO::PARAM_STR);

  $stmt->execute();

  return true;
}

function editDescriptionFromPoll($pollID, $description) {
  if (!getPermissions()['canManagePolls'] == 1 and !getPermissions()['isAdmin'] == 1) {
    return false;
  }

  if(!isValidPollDescription($description)) {
    return false;
  }

  $conn = connect();

  $stmt = $conn->prepare('UPDATE polls SET description=:description WHERE pollID=:pollID;');

  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);
  $stmt->bindParam(':description', $description, PDO::PARAM_STR);

  $stmt->execute();

  return true;
}

function editDateFromPoll($pollID, $date) {
  if (!getPermissions()['canManagePolls'] == 1 and !getPermissions()['isAdmin'] == 1) {
    return false;
  }

  /*if(!isValidDate($date)) {
    return false;
  }*/

  $conn = connect();

  $stmt = $conn->prepare('UPDATE polls SET finishDate=:finishDate WHERE pollID=:pollID;');

  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);
  $stmt->bindParam(':finishDate', $date, PDO::PARAM_INT);

  $stmt->execute();

  return true;
}