<?php

require_once(__DIR__ . '/../dbConnector.php');
require_once(__DIR__ . '/../emailHandler.php');
require_once(__DIR__ . '/../userHandler.php');
require_once(__DIR__ . '/../validationHandler.php');
require_once(__DIR__ . '/../userHandler/tokenHandler.php');

if (isset($_REQUEST['action'])) {
  switch ($_REQUEST['action']) {
    case 'login':
      $emailAddress = $_REQUEST['emailAddress'];
      $password = $_REQUEST['password'];
      $res = login($emailAddress, $password);
      break;

    case 'sendResetEmail':
      $emailAddress = $_REQUEST['emailAddress'];
      $res = sendResetEmail($emailAddress);
      break;

    case 'checkPasswordResetVerificationCode':
      $emailAddress = $_REQUEST['emailAddress'];
      $code = $_REQUEST['code'];
      $res = (activationCode(getUserIDByEmailAddress($emailAddress)) == $code);

      break;

    case 'resetPassword':
      $emailAddress = $_REQUEST['emailAddress'];
      $code = $_REQUEST['code'];
      $password = $_REQUEST['password'];
      $passwordAgain = $_REQUEST['passwordAgain'];

      $res = resetPassword($password, $passwordAgain, $emailAddress, $code);

      break;

    case 'signup':
      $emailAddress = $_REQUEST['emailAddress'];
      $birthdate = $_REQUEST['birthdate'];
      $password = $_REQUEST['password'];
      $passwordAgain = $_REQUEST['passwordAgain'];
      $title = $_REQUEST['title'];
      $firstname = $_REQUEST['firstname'];
      $surname = $_REQUEST['surname'];
      $streetname = $_REQUEST['streetname'];
      $streetnumber = $_REQUEST['streetnumber'];
      $zipcode = $_REQUEST['zipcode'];
      $location = $_REQUEST['location'];

      $res = signup($emailAddress, $password, $passwordAgain, $firstname, $surname, $birthdate, $title, $streetname, $streetnumber, $location, $zipcode);
      break;

    case 'activateAccount':
      $emailAddress = $_REQUEST['emailAddress'];
      $activationCode = $_REQUEST['activationCode'];

      $res = activateAccount($emailAddress, $activationCode);
      break;

    case 'stayLoggedIn':
      $emailAddress = $_REQUEST['emailAddress'];
      $password = $_REQUEST['password'];
      $browserName = $_REQUEST['browserName'];
      $browserVersion = $_REQUEST['browserVersion'];
      $userAgent = $_REQUEST['userAgent'];
      $operatingsystem = $_REQUEST['operatingsystem'];

      $res = stayLoggedIn($emailAddress, $password, $browserName, $browserVersion, $userAgent, $operatingsystem);
      break;

    case "loginWithToken":
      $emailAddress = $_REQUEST['emailAddress'];
      $token = $_REQUEST['token'];

      $res = login_with_token($emailAddress, $token);

      break;
  }

  echo $res;
}

function signup($email, $password, $passwordAgain, $firstname, $surname, $birthdate, $title, $streetname, $streetnumber, $location, $zipcode)
{
  if(!isEmail($email))
    return "notValidEmail";

  if(emailAddressAlreadyUsed($email)) {
    return "emailAddressAlreadyUsed";
  }

  if($password != $passwordAgain)
    return "passwordNotEquals";

  if(!isValidPassword($password))
    return "notValidPassword";

  if(!isValidFirstname($firstname))
    return "notValidFirstname";

  if(!isValidSurname($surname))
    return "notValidSurname";

  if(!isValidDate($birthdate))
    return "notValidBirthdate";

  if(!isValidUserTitle($title))
    return "notValidTitle";

  if(!isValidStreetname($streetname))
    return "notValidStreetname";

  if(!isValidStreetnumber($streetnumber))
    return "notValidStreetnumber";

  if(!isValidLocation($location))
    return "notValidLocation";

  if(is_numeric($zipcode)) {
    if(!isValidZipCode($zipcode))
      return "notValidZipcode";
  } else {
    $zipcode = null;
  }


  $joindate = date('Y-m-d');
  $conn = connect();

	$stmt = $conn->prepare("SELECT count(userID) FROM users");
	$stmt->execute();
	$first = $stmt->fetch() == 0 ? 1 : 0;

  $stmt = $conn->prepare('INSERT INTO users (firstname, surname, email, title, location, streetname, 
                                             streetnumber, zip_code, birthdate, joindate, active, hasBeenActivated, 
                                             activationCode, confirmedEmailAddress) 
                          VALUES (:firstname, :surname, :email, :title, :location, :streetname, 
                                  :streetnumber, :zip_code, :birthdate, :joindate, 0, :first, 0, 0)');

  $stmt->bindParam(':firstname', $firstname);
  $stmt->bindParam(':surname', $surname);
  $stmt->bindParam(':email', $email);
  $stmt->bindParam(':title', $title);
  $stmt->bindParam(':location', $location);
  $stmt->bindParam(':streetname', $streetname);
  $stmt->bindParam(':streetnumber', $streetnumber);
  $stmt->bindParam(':zip_code', $zipcode);
  $stmt->bindParam(':birthdate', $birthdate);
  $stmt->bindParam(':joindate', $joindate);
	$stmt->bindParam(':first', $first, PDO::PARAM_INT);

  $stmt->execute();

  $userID = getUserIDByEmailAddress($email);

  $hashed_password = hash("sha512", $password . $userID);

  $stmt = $conn->prepare('UPDATE users SET password = :password WHERE userID = :userID;');
  $stmt->bindParam(':userID', $userID);
  $stmt->bindParam(':password', $hashed_password);
  $stmt->execute();

  $code = rand(100000, 999999);

  activationCode($userID, $code);

  $stmt = $conn->prepare('UPDATE permissions SET isAdmin = :isAdmin WHERE userID = :userID;');
  $stmt->bindParam(':userID', $userID);
	$stmt->bindParam(':isAdmin', $first, PDO::PARAM_INT);
  $stmt->execute();

  sendActivationCodeToUser($email, $firstname . ' ' . $surname, $code);

  return true;
}

function activateAccount($email, $code) {
  if(activationCode(getUserIDByEmailAddress($email)) == $code) {
    $userID = getUserIDByEmailAddress($email);

    confirmedEmailAddress($userID, true);

    activationCode($userID, 0);

    return true;
  } else {
    return false;
  }
}

/**
 * checks, if the user is logged in
 *
 * @return bool returns true, if the user is logged in, else false
 */
function isLoggedIn()
{
  if (!isset($_SESSION)) {
    session_start();
  }

  if (isset($_SESSION['isLoggedIn'])) {
    if($_SESSION['isLoggedIn']) {
      if(isset($_COOKIE['token']) AND isset($_COOKIE['emailAddress'])) {
        if(checkToken(getUserIDByEmailAddress($_COOKIE['emailAddress']), $_COOKIE['token'])) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    } else {
      return false;
    }
  } else {
    return false;
  }
}

/**
 * performes the logout, if the user is signed-in
 */
function logout()
{
  if (!isset($_SESSION)) {
    session_start();
  }
  session_destroy();
}


/**
 * performes an login-attempt
 *
 * @param string $emailAddress    the email-address of the user
 * @param string $password the password of the user as a sha512-hash
 * @return bool     returns true, if the user was successfully signed-in, else false
 */
function login($emailAddress, $password) {
  if(!emailAddressAlreadyUsed($emailAddress)) {
    return "false";
  }

  $userID = getUserIDByEmailAddress($emailAddress);

  if (!hasBeenActivated($userID)) {
    return "notActivated";
  }

  $conn = connect();

  $hashed_password = hash('sha512', $password . $userID);

  $stmt = $conn->prepare('SELECT userID, firstname, surname, email, title, location, streetname, streetnumber, 
                                    zip_code, birthdate, joindate, active, hasBeenActivated, activationCode FROM users WHERE email = :email AND password = :password;');
  $stmt->bindParam(':email', $emailAddress);
  $stmt->bindParam(':password', $hashed_password);
  $stmt->execute();

  while ($row = $stmt->fetch()) {
    $_SESSION['userID'] = $row[0];
    $_SESSION['firstname'] = $row[1];
    $_SESSION['surname'] = $row[2];
    $_SESSION['email'] = $row[3];
    $_SESSION['title'] = $row[4];
    $_SESSION['location'] = $row[5];
    $_SESSION['streetname'] = $row[6];
    $_SESSION['streetnumber'] = $row[7];
    $_SESSION['zip_code'] = $row[8];
    $_SESSION['birthdate'] = $row[9];
    $_SESSION['joindate'] = $row[10];
    $_SESSION['active'] = $row[11] == 1 ? true : false;
    $_SESSION['hasBeenActivated'] = $row[12] == 1 ? true : false;

    $_SESSION['isLoggedIn'] = true;

    return "true";
    break;
  }

  return "false";
}

/**
 * performes a login-attempt with a token (which is stored as cookie in your browser if you choosed stay logged in)
 * @param string $emailAddress the email-address of the user
 * @param string $token the token of the user stored in a cookie
 * @return string returns the result
 */
function login_with_token($emailAddress, $token) {
  if(!emailAddressAlreadyUsed($emailAddress)) {
    return "false";
  }

  $userID = getUserIDByEmailAddress($emailAddress);

  if (!hasBeenActivated($userID)) {
    return "notActivated";
  }

  $conn = connect();

  $stmt = $conn->prepare('SELECT users.userID, firstname, surname, email, title, location, streetname, streetnumber, 
                                    zip_code, birthdate, joindate, active, hasBeenActivated, activationCode FROM users JOIN tokens ON users.userID = tokens.userID WHERE tokens.userID = :userID AND tokens.token = :token;');
  $stmt->bindParam(':userID', $userID);
  $stmt->bindParam(':token', $token);
  $stmt->execute();

  $loginSuccessful = false;

  while ($row = $stmt->fetch()) {
    if(!isset($_SESSION)) {
      session_start();
    }

    $_SESSION['userID'] = $row[0];
    $_SESSION['firstname'] = $row[1];
    $_SESSION['surname'] = $row[2];
    $_SESSION['email'] = $row[3];
    $_SESSION['title'] = $row[4];
    $_SESSION['location'] = $row[5];
    $_SESSION['streetname'] = $row[6];
    $_SESSION['streetnumber'] = $row[7];
    $_SESSION['zip_code'] = $row[8];
    $_SESSION['birthdate'] = $row[9];
    $_SESSION['joindate'] = $row[10];
    $_SESSION['active'] = $row[11] == 1 ? true : false;
    $_SESSION['hasBeenActivated'] = $row[12] == 1 ? true : false;

    $_SESSION['isLoggedIn'] = true;

    $loginSuccessful = true;
    break;
  }

  if($loginSuccessful) {
    $lastUsedTokenDate = date('Y-m-d');

    $stmt = $conn->prepare('UPDATE tokens SET lastUsedTokenDate = :lastUsedTokenDate WHERE token = :token;');

    $stmt->bindParam(':lastUsedTokenDate', $lastUsedTokenDate, PDO::PARAM_STR_CHAR);
    $stmt->bindParam(':token', $token, PDO::PARAM_STR_CHAR);

    $stmt->execute();

    return "true";
  }

  return "false";
}

/**
 * sends a verification-email to the given email-address
 *
 * @param string $email    the email-address
 * @return bool     returns true, if the email was sent successfully, else false (WorkInProgress)
 */
function sendResetEmail($email)
{
  if(!emailAddressAlreadyUsed($email)) {
    return false;
  }

  $userID = getUserIDByEmailAddress($email);

  $conn = connect();

  $code = rand(100000, 999999);

  $stmt = $conn->prepare('UPDATE users SET activationCode = :code where userID = :userID;');
  $stmt->bindParam(':userID', $userID);
  $stmt->bindParam(':code', $code);
  $stmt->execute();

  sendResetEmailToUser($email, getFirstnameAndLastnameByUserID($userID), $code);

  return true;
}

/**
 * sets the new password after requesting the password-reset
 *
 * @param string $password     cleartext, new password
 * @param string $passwordAgain    cleartext, new password 2nd time
 * @param string $email email-address
 * @param number $code     verificatiton code
 * @return bool     returns true, if the password reset was successful, else false
 */
function resetPassword($password, $passwordAgain, $email, $code)
{
  if(!isValidPassword($password)) {
    return "notValidPassword";
  }

  if ($password != $passwordAgain) {
    return "passwordAreNotEqual";
  }

  if (!emailAddressAlreadyUsed($email)) {
    return "emailAddressNotUsed";
  }

  $userID = getUserIDByEmailAddress($email);

  if (activationCode($userID) != $code) {
    return "activationCodeIncorrect";
  }

  $hashed_password = hash('sha512', $password . $userID);

  $conn = connect();

  $stmt = $conn->prepare('UPDATE users SET password = :password WHERE userID = :userID');
  $stmt->bindParam(':userID', $userID);
  $stmt->bindParam(':password', $hashed_password);
  $stmt->execute();

  activationCode($userID, 0);

  return true;
}

/**
 * creates a token for stay logged in
 * @param string $emailAddress the email-address from the user
 * @param string $password the password from the user
 * @param string $browserName the browser name of the browser
 * @param string $browserVersion the browser version
 * @param string $userAgent the user agent
 * @param string $operatingsystem the operating system the browser is running on
 * @return string returns the new generated token
 */
function stayLoggedIn($emailAddress, $password, $browserName, $browserVersion, $userAgent, $operatingsystem) {

  if(!emailAddressAlreadyUsed($emailAddress)) {
    return "emailAddressDoesNotExist";
  }

  $userID = getUserIDByEmailAddress($emailAddress);

  $hashed_password = hash('sha512', $password . $userID);

  $conn = connect();
  $stmt = $conn->prepare('SELECT password FROM users WHERE userID = :userID AND password = :password;');
  $stmt->bindParam(':userID', $userID);
  $stmt->bindParam(':password', $hashed_password);

  $stmt->execute();

  if (!count($stmt->fetchAll()) > 0)
    return "incorrectPassword";

  $createdDate = date('Y-m-d');
  $lastUsedTokenDate = date('Y-m-d');

  return createToken($userID, $createdDate, $lastUsedTokenDate, $browserName, $browserVersion, $userAgent, $operatingsystem);
}