<?php
require_once __DIR__ . '/userHandler.php';
require_once __DIR__ . '/dbConnector.php';

/**
 * gets all permissions of the given user or the current user, if no userID is provided
 *
 * @param null $userID the userID of the user to get the permissions, 
 * 											if null the current user's ID will be used, 
 * 											if -1 the names of all permissions will be returned
 * @return array with all permissions
 */
function getPermissions($userID = null){
	if (! isset ( $userID ))
		$userID = $_SESSION ['userID'];
	
	$keymode = false;
	if ($userID == '-1') {
		$userID = 1;
		$keymode = true;
	}
	
	if (! isset ( $userID ))
		return 'false';
	
	$conn = connect ();
	
	$stmt = $conn->prepare ( "SELECT * FROM permissions WHERE userID = :userID" );
	$stmt->bindParam ( ":userID", $userID, PDO::PARAM_STR );
	$stmt->execute ();
	
	$stmt = $stmt->fetch ( PDO::FETCH_ASSOC );
	
	if ($keymode) {
		$keymode = [ ];
		foreach ( $stmt as $key => $item )
			if ($key != 'userID')
				$keymode [] = $key;
		return $keymode;
	}
	
	return $stmt;
}

function isAdmin($userID = null){
	if(!isset($userID))
		$userID = $_SESSION['userID'];
	
	$conn = connect();
	$stmt = $conn->prepare("SELECT isAdmin = 1 FROM permissions WHERE userID = :userID");
	$stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
	$stmt->execute();
	
	return boolval($stmt->fetch()[0]);
}

function canManageUsers($userID = null){
	if(!isset($userID))
		$userID = $_SESSION['userID'];
		
		$conn = connect();
		$stmt = $conn->prepare("SELECT canManageUsers = 1 FROM permissions WHERE userID = :userID");
		$stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
		$stmt->execute();
		
		return boolval($stmt->fetch()[0]);
}

function canManagePolls($userID = null){
	if(!isset($userID))
		$userID = $_SESSION['userID'];
		
		$conn = connect();
		$stmt = $conn->prepare("SELECT canManagePolls = 1 FROM permissions WHERE userID = :userID");
		$stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
		$stmt->execute();
		
		return boolval($stmt->fetch()[0]);
}

function canManageOrganisation($userID = null){
	if(!isset($userID))
		$userID = $_SESSION['userID'];
		
		$conn = connect();
		$stmt = $conn->prepare("SELECT canManageOrganisation = 1 FROM permissions WHERE userID = :userID");
		$stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
		$stmt->execute();
		
		return boolval($stmt->fetch()[0]);
}

function canManageGroups($userID = null){
	if(!isset($userID))
		$userID = $_SESSION['userID'];
		
		$conn = connect();
		$stmt = $conn->prepare("SELECT canManageGroups = 1 FROM permissions WHERE userID = :userID");
		$stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
		$stmt->execute();
		
		return boolval($stmt->fetch()[0]);
}

function canManageSubgroups($userID = null){
	if(!isset($userID))
		$userID = $_SESSION['userID'];
		
		$conn = connect();
		$stmt = $conn->prepare("SELECT canManageSubgroups = 1 FROM permissions WHERE userID = :userID");
		$stmt->bindParam(":userID", $userID, PDO::PARAM_INT);
		$stmt->execute();
		
		return boolval($stmt->fetch()[0]);
}