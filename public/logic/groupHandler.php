<?php
require_once('dbConnector.php');

if (isset($_REQUEST) and isset($_REQUEST['action'])) {
	$res = '';
	switch ($_REQUEST['action']) {
		case 'editGroup':
			$res = updateGroup($_REQUEST['groupID'], $_REQUEST['name'], $_REQUEST['description']);
			break;
		case 'addGroup':
			$_REQUEST['name'] = $_REQUEST['name'] == '' ? null : $_REQUEST['name'];

			$res = createGroup($_REQUEST['name'], $_REQUEST['description']);
			break;
		case 'addSubgroup':
			$res = createSubgroup($_REQUEST['groupID'], $_REQUEST['name'], $_REQUEST['description']);
			break;
		case 'removeGroup':
			$res = removeGroup($_REQUEST['groupID']);
			break;
		case 'getGroup':
			$res = getGroups((int)$_REQUEST['groupID']);
			break;
		case 'moveUser':
			$res = moveUserIntoSubgroup($_REQUEST['userID'], $_REQUEST['currentSubgroupID'], $_REQUEST['newSubgroupID']);
			break;
	}
	echo $res;
}

/**
 * gets an array of all groups with all informations or the data of the specified group
 * @param null $groupID if not set, all groups will be returned, if set, just the specified group will be returned
 * @return array|mixed|string  data of the groups specified or all groups or 'false' on error
 */
function getGroups($groupID = null) {
	try {
		$conn = connect();

		$stmt = "SELECT * FROM groups" . (isset($groupID) ? " WHERE groupID = :groupID" : "");
		$stmt = $conn->prepare($stmt);
		if (isset($groupID))
			$stmt->bindParam(':groupID', $groupID, PDO::PARAM_INT);
		$stmt->execute();

		if (isset($groupID))
			return $stmt->fetch();
		else
			return $stmt->fetchAll();
	} catch (PDOException $e) {
		return 'false';
	}
}

/**
 * gets all groups and subgrous the user is in
 * @param $userID  the userID of the user to get
 * @return array|string  group and subgroup-data or 'false' on error
 */
function getGroupsOfUser($userID = null) {
	if (!isset($userID))
		$userID = $_SESSION['userID'];
	try {
		$conn = connect();

		$stmt = $conn->prepare("SELECT groups.*, subgroups.subgroupID, subgroups.description as subgroupDescription FROM user_is_member_of 
													LEFT JOIN subgroups ON user_is_member_of.subgroupID = subgroups.subgroupID
													LEFT JOIN groups ON subgroups.groupID = groups.groupID
													ORDER BY groups.groupID, subgroups.subgroupID");
		$stmt->bindParam(':userID', $userID, PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->fetchAll();
	} catch (PDOException $e) {
		return 'false';
	}
}

/**
 * gets all subgroups and their corresponding groups
 * @return array|string  array of subgroups with groups
 */
function getGroupsAndSubgroups() {
	try {
		$conn = connect();

		$stmt = $conn->prepare("SELECT subgroups.*, groups.name group_name FROM subgroups												
													LEFT JOIN groups ON subgroups.groupID = groups.groupID
													ORDER BY subgroups.groupID, subgroups.subgroupID");
		$stmt->execute();

		return $stmt->fetchAll();
	} catch (PDOException $e) {
		return 'false';
	}
}

/**
 * gets the subgroup with the given subgroupID or all subgroups
 * @param subgroupID|null $subgroupID the subgroupID of the subgroup to get or null, if all subgroups should be listed
 * @return array|mixed|string          the subgroup-data, or an array of subgroups or false in case of an error
 */
function getSubgroups($subgroupID = null) {
	try {
		$conn = connect();

		$stmt = "SELECT * FROM subgroups" . (isset($subgroupID) ? " WHERE subgroupID = :subgroupID" : "") . " ORDER BY groupID, subgroupID";
		$stmt = $conn->prepare($stmt);
		if (isset($subgroupID))
			$stmt->bindParam(':subgroupID', $subgroupID, PDO::PARAM_INT);
		$stmt->execute();

		if (isset($subgroupID))
			return $stmt->fetch();
		else
			return $stmt->fetchAll();
	} catch (PDOException $e) {
		return 'false';
	}
}

/**
 * gets all member of the specified subgroupID
 * @param $subgroupID    subgroupID of the subgroup to get the users from
 * @return array|string  array of users in the subgroup
 */
function getMembersOfSubgroup($subgroupID) {
	try {
		$conn = connect();

		$stmt = "SELECT * FROM user_is_member_of, users WHERE user_is_member_of.subgroupID = :subgroupID AND users.userID = user_is_member_of.userID ORDER BY users.firstname, users.surname";
		$stmt = $conn->prepare($stmt);
		if (isset($subgroupID))
			$stmt->bindParam(':subgroupID', $subgroupID, PDO::PARAM_INT);
		$stmt->execute();

		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	} catch (PDOException $e) {
		return $e->getMessage();
		return 'false';
	}
}

/**
 * creates a group
 * @param string $name the name of the group
 * @param string $description the description of the group
 * @return string                if successful 'true' else 'false'
 */
function createGroup($name, $description) {
	try {
		$conn = connect();

		$stmt = $conn->prepare('INSERT INTO `groups` VALUES (NULL , :name, :description)');
		$stmt->bindParam(':name', $name, PDO::PARAM_STR, 32);
		$stmt->bindParam(':description', $description, PDO::PARAM_STR);

		$stmt->execute();
	} catch (PDOException $e) {
		echo "<pre>";
		var_dump($e);
		return 'false';
	}
	return 'true';
}

/**
 * creates a new subgroup for the specified group
 * @param int $groupID 					groupID of the group to create a new subgroup in
 * @param string $name 					the name of the new subgroup
 * @param string $description 	the description of the new subgroup
 * @return string			 					'true' when successfully created the subgroup, else 'false'
 */
function createSubgroup($groupID, $name, $description) {
	try {
		$conn = connect();

		$stmt = $conn->prepare("INSERT INTO subgroups (`subgroupID`, `name`, `groupID`, `description`) VALUES (NULL, :name ,:groupID, :description)");
		$stmt->bindParam(':groupID', $groupID, PDO::PARAM_INT);
		$stmt->bindParam(':name', $name, PDO::PARAM_STR);
		$stmt->bindParam(':description', $description, PDO::PARAM_STR);
		$stmt->execute();

		return 'true';
	}catch (PDOException $e){
		$e->getMessage();
		return 'false';
	}
}

/**
 * updates the specified group
 * @param $groupID  groupID of the group to update
 * @param $name  new name of the group
 * @param $description  new description of the group
 * @return string  'true' when successfully updated the group, else 'false'
 */
function updateGroup($groupID, $name, $description) {
	try {
		$conn = connect();

		$stmt = $conn->prepare("UPDATE groups SET `name` = :name, `description` = :description WHERE groupID = :groupID ");
		$stmt->bindParam(':groupID', $groupID, PDO::PARAM_INT);
		$stmt->bindParam(':name', $name, PDO::PARAM_STR, 32);
		$stmt->bindParam(':description', $description, PDO::PARAM_STR);

		$stmt->execute();
	} catch (PDOException $e) {
		return 'false';
	}
	return 'true';
}

/**
 * removes a group
 * @param $groupID  groupID of the group to remove
 * @return string  'true' when successfully removed the group, else 'false'
 */
function removeGroup($groupID) {
	try {
		$conn = connect();

		$stmt = $conn->prepare("DELETE FROM groups WHERE groupID = :groupID");
		$stmt->bindParam(':groupID', $groupID, PDO::PARAM_INT);

		$stmt->execute();
	} catch (PDOException $e) {
		return 'false';
	}
	return 'true';
}

function getMembersOfSubgroupAsTable($subgroupID) {
	$users = getMembersOfSubgroup($subgroupID);
	// generates an alert when the subgroup is empty
	if (count($users) == 0)
		return parseAlert("<b>Ooops!</b><br />Seems empty in here...", "warning", false);

	$table = parseTableHead(parseTableData('Vorname', 'class="col-5"') .
			parseTableData('Nachname', 'class="col-6"') .
			((getPermissions()['canManageGroups'] || getPermissions()['isAdmin']) ? parseTableData('', 'class="col-1"') : '')
	);
	foreach ($users as $user) {
		$btnMove = "<button style='color: rgba(0,0,0,0.7);' class='btn btn-primary float-right' onclick='moveUser(" . $user['userID'] . ", $subgroupID" .")' title='Benutzer in andere Abteilung/Gruppe verschieben'> 
                  <i class='fas fa-ellipsis-v'></i> 
                </button>";
		$table .= parseTableRow(parseTableData($user['firstname'], "onclick='showProfileOf(" . $user['userID'] . ")' class='col-5'") .
				parseTableData($user['surname'], "onclick='showProfileOf(" . $user['userID'] . ")' class='col-6'") .
				((getPermissions()['canManageGroups'] || getPermissions()['isAdmin']) ? parseTableData($btnMove, "class='col-5'") : '')
		);
	}
	return parseTable($table, 'class="table table-bordered table-hover"');
}

/**
 * Moves the specified user into the specified subgroup
 * @param int $userID        			userID of the user to move
 * @param int $currentSubgroupID  subgroupID of the user is currently in
 * @param int $newSubgroupID  		subgroupID of the user to move to
 * @return string        					'true' if successfully moved, else 'false'
 */
function moveUserIntoSubgroup($userID, $currentSubgroupID, $newSubgroupID) {

	try {
		$conn = connect();
		$stmt = $conn->prepare("UPDATE user_is_member_of SET subgroupID = :newSubgroupID WHERE userID = :userID and subgroupID = :currentSubgroupID");
		$stmt->bindParam(':newSubgroupID', $newSubgroupID, PDO::PARAM_INT);
		$stmt->bindParam(':userID', $userID, PDO::PARAM_INT);
		$stmt->bindParam(':currentSubgroupID', $currentSubgroupID, PDO::PARAM_INT);
		$stmt->execute();
		return 'true';
	} catch (PDOException $e) {
		echo $e->getMessage();
		return 'false';
	}
}