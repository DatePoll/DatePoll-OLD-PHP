<?php
require_once('dbConnector.php');

if (isset($_REQUEST['action'])) {
	switch ($_REQUEST['action']) {
		case 'edit':
			$res = updateOrganisation($_REQUEST['name'], $_REQUEST['description'], $_REQUEST['homepageLink'], $_REQUEST['foundingDate']);
			break;
	}

	if (isset($res))
		echo $res;
}

/**
 * gets all information about the organisation
 * @return mixed	array with information of the organisation
 */
function getOrganisation()
{
	$conn = connect();

	$stmt = $conn->prepare("SELECT * FROM organisation");
	$stmt->execute();

	return $stmt->fetch();
}

/**
 * updates the organisation information
 * @param $name	new name of the organisation
 * @param $description	new description of the organisation
 * @param $homepageLink	new homepage-link of the organisation
 * @param $foundingDate new foundig-date of the organisation
 * @return string	'true' when successfully updated the organisation, else 'false'
 */
function updateOrganisation($name, $description, $homepageLink, $foundingDate)
{
	try {
		$conn = connect();

		$stmt = $conn->prepare('UPDATE organisation SET name = :name, description = :description, homepageLink = :homepageLink, foundingDate = :foundingDate');
		$stmt->bindParam(':name', $name, PDO::PARAM_STR, 64);
		$stmt->bindParam(':description', $description, PDO::PARAM_STR);
		$stmt->bindParam(':homepageLink', $homepageLink, PDO::PARAM_STR, 128);
		$stmt->bindParam(':foundingDate', $foundingDate, PDO::PARAM_STR);
		$stmt->execute();

	} catch (PDOException $e) {
		return 'false';
	}
	return 'true';
}

/**
 * gets the count of members of the organisation
 * @return mixed number of members
 */
function countMembers()
{
	$conn = connect();

	$stmt = $conn->prepare('SELECT count(userID) FROM users WHERE hasBeenActivated = 1');
	$stmt->execute();

	return $stmt->fetch();
}

/**
 * gets the count of active members of the organisation
 * @return mixed	number of active members
 */
function countActiveMembers()
{
	$conn = connect();

	$stmt = $conn->prepare('SELECT count(userID) FROM users WHERE active = 1');
	$stmt->execute();

	return $stmt->fetch();
}