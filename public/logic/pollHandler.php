<?php
require_once(__DIR__ . '/dbConnector.php');
require_once(__DIR__ . '/parser.php');
require_once(__DIR__ . '/pollHandler/administration.php');

if (isset($_REQUEST) and isset($_REQUEST['action'])) {
  $res = '';
  switch ($_REQUEST['action']) {
    case 'editPoll':
      $_REQUEST['groupID'] = $_REQUEST['groupID'] == '' ? null : $_REQUEST['groupID'];
      $_REQUEST['subgroupID'] = $_REQUEST['subgroupID'] == '' ? null : $_REQUEST['subgroupID'];

      $res = updatePoll($_REQUEST['pollID'], $_REQUEST['groupID'], $_REQUEST['subgroupID'], $_REQUEST['title'], $_REQUEST['description'], $_REQUEST['finishDate']);
      break;
    case 'getPoll':
      $res = getPolls((int)$_REQUEST['pollID']);
      break;
    case 'voteForPoll':
      $res = vote($_SESSION['userID'], $_REQUEST['pollID'], $_REQUEST['decisionID']);
      break;
    case 'updateVoteForPoll':
      $res = updateVote($_SESSION['userID'], $_REQUEST['pollID'], $_REQUEST['decisionID']);
      break;
    case 'analyzePoll':
      $res = analyzePoll($_REQUEST['pollID']);
      break;
    case 'getPollByTitleDescriptionAndDate':
      $res = getPollByTitleDescriptionAndDate($_REQUEST['title'], $_REQUEST['description'], $_REQUEST['date']);
      break;
  }
  echo $res;
}

/**
 * @param $pollID        the pollID of the poll to update
 * @param $groupID      the new groupID of the group the poll should be for
 * @param $subgroupID   the new subgroupID of the subgroup the poll should be for
 * @param $title        the new title of the poll
 * @param $description  the new description of the poll
 * @param $finishDate   the new finish-date of the poll
 * @return string       if successfully updated 'true', else 'false'
 */
function updatePoll($pollID, $groupID, $subgroupID, $title, $description, $finishDate)
{
  try {
    $conn = connect();

    $stmt = $conn->prepare('UPDATE `polls` SET `groupID` = :groupID, `subgroupID` = :subgroupID, `title` = :title, `description` = :description, `finishDate` = :finishDate WHERE `polls`.`pollID` = :pollID');

    $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);
    $stmt->bindParam(':groupID', $groupID, PDO::PARAM_INT);
    $stmt->bindParam(':subgroupID', $subgroupID, PDO::PARAM_INT);
    $stmt->bindParam(':title', $title, PDO::PARAM_STR, 64);
    $stmt->bindParam(':description', $description, PDO::PARAM_STR);
    $stmt->bindParam(':finishDate', $finishDate, PDO::PARAM_STR);

    $stmt->execute();
    return 'true';
  } catch (PDOException $e) {
    //return $e->getMessage();
    return 'false';
  }
  return 'true';
}

function getPollByTitleDescriptionAndDate($title, $description, $date)
{
  $conn = connect();

  $stmt = $conn->prepare('SELECT pollID FROM polls WHERE title = :title AND description = :description AND finishDate = :finishDate');
  $stmt->bindParam(':title', $title, PDO::PARAM_STR);
  $stmt->bindParam(':description', $description, PDO::PARAM_STR);
  $stmt->bindParam(':finishDate', $date, PDO::PARAM_INT);

  $stmt->execute();
  return $stmt->fetch()['pollID'];
}

/**
 * gets the poll specified of all polls
 * @param pollID|null $pollID the pollID of the poll to get the data from or null to get all polls
 * @return array|mixed        the poll or an array of polls
 */
function getPolls($pollID = null)
{
  $conn = connect();

  $stmt = "SELECT * FROM polls " . (isset($pollID) ? 'WHERE pollID = :pollID' : '') . " ORDER BY finishDate";
  $stmt = $conn->prepare($stmt);
  if (isset($pollID))
    $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);

  $stmt->execute();

  if (isset($pollID))
    return $stmt->fetch();
  else
    return $stmt->fetchAll();
}

function getGroupsForPoll($pollID)
{
  $conn = connect();

  $stmt = $conn->prepare('SELECT * FROM polls_for_groups WHERE pollID = :pollID;');
  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);

  $stmt->execute();

  return $stmt->fetchAll();
}

function getSubGroupForPoll($pollID)
{
  $conn = connect();

  $stmt = $conn->prepare('SELECT * FROM polls_for_subgroups WHERE pollID = :pollID;');
  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);

  $stmt->execute();

  return $stmt->fetchAll();
}

function getGroupAssignedToPoll($pollID)
{
  $conn = connect();

  $stmt = $conn->prepare('SELECT groups.name, groups.groupID FROM polls_for_groups JOIN groups 
                                    ON polls_for_groups.groupID = groups.groupID WHERE polls_for_groups.pollID = :pollID');
  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);

  $stmt->execute();
  return $stmt->fetchAll();
}

function getSubGroupAssignedToPoll($pollID)
{
  $conn = connect();

  $stmt = $conn->prepare('SELECT subgroups.name, subgroups.subgroupID FROM polls_for_subgroups JOIN subgroups 
                                    ON polls_for_subgroups.subgroupID = subgroups.subgroupID WHERE polls_for_subgroups.pollID = :pollID');
  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);

  $stmt->execute();
  return $stmt->fetchAll();
}

function getUnassignedGroupsForPoll($pollID)
{
  $conn = connect();

  $stmt = $conn->prepare('SELECT * FROM groups WHERE groupID NOT IN 
                                      (SELECT groupID FROM polls_for_groups pfg WHERE pfg.pollID = :pollID);');
  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);

  $stmt->execute();

  return $stmt->fetchAll();
}

function getUnassignedSubGroupsForPoll($pollID)
{
  $conn = connect();

  $stmt = $conn->prepare('SELECT * FROM subgroups WHERE subgroupID NOT IN 
                                    (SELECT subgroupID FROM polls_for_subgroups pfg WHERE pfg.pollID = :pollID);');
  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);

  $stmt->execute();

  return $stmt->fetchAll();
}

/**
 * gets all polls, where the finish-date is in the future
 * @return array  array of polls
 */
function listOpenPolls()
{
  $userID = $_SESSION['userID'];

  $conn = connect();

  $stmt = $conn->prepare("SELECT polls.* FROM polls LEFT JOIN user_voted_for_poll ON polls.pollID = user_voted_for_poll.pollID 
				WHERE polls.pollID NOT IN 
					(SELECT DISTINCT pollID FROM user_voted_for_poll WHERE userID = :userID) AND polls.finishDate > CURRENT_TIMESTAMP
				ORDER BY polls.finishDate");
  $stmt->bindParam(':userID', $userID, PDO::PARAM_INT);
  $stmt->execute();

  return $stmt->fetchAll();
}

/**
 * gets all polls and the decision the user voted for
 * @return array  array of polls and decisions the user voted for
 */
function listPollsVotedFor()
{
  $userID = $_SESSION['userID'];

  $conn = connect();

  $stmt = $conn->prepare("SELECT polls.pollID, polls.title, polls.description, polls.finishDate, polls_for_groups.groupID, polls_for_subgroups.subgroupID, user_voted_for_poll.decisionID 
				FROM polls 
					LEFT JOIN user_voted_for_poll ON polls.pollID = user_voted_for_poll.pollID
					LEFT JOIN polls_for_groups ON polls.pollID = polls_for_groups.pollID 
					LEFT JOIN polls_for_subgroups ON polls.pollID = polls_for_subgroups.pollID
				WHERE user_voted_for_poll.userID = :userId AND polls.pollID IN 
					(SELECT DISTINCT pollID FROM user_voted_for_poll WHERE userID = :userID) AND polls.finishDate > CURRENT_TIMESTAMP
					ORDER BY polls.finishDate");
  $stmt->bindParam(':userID', $userID, PDO::PARAM_INT);
  $stmt->bindParam(':userId', $userID, PDO::PARAM_INT);
  $stmt->execute();

  return $stmt->fetchAll();
}

/**
 * places a vote for the specified poll
 * @param $userID      the userID of the user voting
 * @param $pollID      the pollID of the poll to vote for
 * @param $decisionID  the decisionID of the decision the user chose
 * @return string      if successfully voted 'true', else 'false'
 */
function vote($userID, $pollID, $decisionID)
{
  try {
    $conn = connect();

    $stmt = $conn->prepare('INSERT INTO `user_voted_for_poll` VALUES (NULL, :pollID, :userID, :decisionID)');

    $stmt->bindParam(':userID', $userID, PDO::PARAM_INT);
    $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);
    $stmt->bindParam(':decisionID', $decisionID, PDO::PARAM_INT);

    $stmt->execute();
  } catch (PDOException $e) {
    echo $e->getMessage();
    return 'false';
  }
  return 'true';
}

/**
 * updates a vote for the specified poll
 * @param $userID      the userID of the user voted
 * @param $pollID      the pollID of the poll to update the vote for
 * @param $decisionID  the decisionID of the decision the user chose
 * @return string      if successfully updated 'true', else 'false'
 */
function updateVote($userID, $pollID, $decisionID)
{
  try {
    $conn = connect();

    $stmt = $conn->prepare('UPDATE `user_voted_for_poll` SET decisionID = :decisionID WHERE pollID = :pollID AND userID = :userID');

    $stmt->bindParam(':userID', $userID, PDO::PARAM_INT);
    $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);
    $stmt->bindParam(':decisionID', $decisionID, PDO::PARAM_INT);

    $stmt->execute();
  } catch (PDOException $e) {
    return 'false';
  }
  return 'true';
}


/**
 * removes the assignment of the specified poll to the specified group
 * @param $pollID      the pollID of the poll to remove the assignment
 * @param $subgroupID  the subgroupID of the subgroup to remove the assignment of the poll
 */
function removePollFromGroup($pollID, $subgroupID)
{
  //remove all votes from users from the provided group from the voted table here

  $conn = connect();

  $stmt = $conn->prepare('UPDATE `polls` SET subgroupID = NULL WHERE pollID = :pollID');
  $stmt->bindParam(':pollID', $pollID);
  $stmt->bindParam(':subgroupID', $groupID);

  $stmt->execute();

  $stmt = $conn->prepare('DELETE FROM `user_voted_for_poll` WHERE userID IN (SELECT userID from `user_is_member_of` WHERE subgroupID = :subgroupID)');
  $stmt->bindParam(':subgroupID', $subgroupID, PDO::PARAM_INT);

  $stmt->execute();
}

/**
 * gets the decisions for yes/no questions
 * @return array  array of decisions
 */
function getDecision($decisionID)
{
  $conn = connect();

  $stmt = $conn->prepare('SELECT * FROM decision WHERE decisionID = :decisionID');
  $stmt->bindParam(':decisionID', $decisionID, PDO::PARAM_INT);

  $stmt->execute();
  return $stmt->fetch();
}

/**
 * gets the decisions for yes/no questions
 * @return array  array of decisions
 */
function getYesNoDecisions()
{
  $conn = connect();

  $stmt = $conn->prepare('SELECT * FROM decision WHERE name in ("Ja", "Nein")');

  $stmt->execute();
  return $stmt->fetchAll();
}

/**
 * get all answers for the specified poll
 * @param $pollID  int pollID to get the answers of
 * @return array      array of answers (entries in user_voted_for_poll) matching the pollID
 */
function getVotesForPoll($pollID)
{
  $conn = connect();

  $stmt = $conn->prepare('SELECT * FROM user_voted_for_poll WHERE pollID = :pollID ORDER BY userID');
  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);

  $stmt->execute();
  return $stmt->fetchAll();
}

/**
 * gets all users, which didn't vote for the specified poll
 * @param $pollID  the poll to get the users for
 * @return array  array of users
 */
function getUsersDidntVoteFor($pollID)
{
  $conn = connect();
  $stmt = $conn->prepare("select * from users where userID not in (select userID from user_voted_for_poll where pollID = :pollID)");
  $stmt->bindParam(':pollID', $pollID, PDO::PARAM_INT);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


function analyzePoll($pollID)
{
  $poll = getPolls($pollID);
  $yesNo = getYesNoDecisions();
  $decisions = getVotesForPoll($pollID);
  $evilUsers = getUsersDidntVoteFor($pollID);

  $yes = null;
  $badgeYes = 0;
  $no = null;
  $badgeNo = 0;
  $nv = null;
  $badgeNv = 0;
  foreach ($decisions as $decision) {
    $user = getUsers($decision['userID']);
    $user = parseTableRow(parseTableData($user['firstname']) . parseTableData($user['surname']));

    switch (getDecision($decision['decisionID'])['name']) {
      case 'Ja':
        $yes .= $user;
        $badgeYes = $badgeYes + 1;
        break;
      case 'Nein':
        $no .= $user;
        $badgeNo = $badgeNo + 1;
        break;
    }
  }

  foreach ($evilUsers as $user) {
    $nv .= parseTableRow(parseTableData($user['firstname']) . parseTableData($user['surname']));
    $badgeNv = $badgeNv + 1;
  }

  $yes = isset($yes) ? parseTable($yes, 'class="table"') : parseAlert('<b>Ooops!</b><br />Dafür hat noch niemand mit <b>Ja</b> gestimmt.', 'danger', false);
  $no = isset($no) ? parseTable($no, 'class="table"') : parseAlert('<b>Super!</b><br />Dafür hat noch niemand mit <b>Nein</b> gestimmt.', 'success', false);
  $nv = isset($nv) ? parseTable($nv, 'class="table"') : parseAlert('<b>Super!</b><br />Dafür haben alle betroffenen abgestimmt.', 'success', false);
  $yeshead = parseDiv($yesNo[1]['name'] . parseBadge($badgeYes, 'badge-light'), 'text-white w-100 p-2');
  $nohead = parseDiv($yesNo[0]['name'] . parseBadge($badgeNo, 'badge-light'), 'text-white w-100 p-2');
  $nvhead = parseDiv('Ausstehend' . parseBadge($badgeNv, 'badge-light'), 'text-white w-100 p-2');

  $accordion = parseAccordion([
    ['id' => 'yes', 'header' => $yeshead, 'body' => $yes, 'class' => 'bg-success'],
    ['id' => 'no', 'header' => $nohead, 'body' => $no, 'class' => 'bg-danger'],
    ['id' => 'nv', 'header' => $nvhead, 'body' => $nv, 'class' => 'bg-warning']
  ]);
  $content = $accordion;

  $modal = parseModal($poll['title'], $content, 'analyze');

  return $modal;
}