<?php

require_once __DIR__ . '/../../vendor/autoload.php';

$router = new \Bramus\Router\Router();

$router->set404(function () {
  header('refresh:0.01;url=/404');
});

$router->all('/', function() {
  header('Location: ../startpage.php');
});

$router->all('/main', function() {
  header('Location: ../main.php');
});

$router->all('/login', function () {
  header('Location: ../sites/authentication/login.php');
});

$router->all('/logout', function () {
  header('Location: ../sites/authentication/logout.php');
});

$router->all('/signup', function () {
	header('Location: ../sites/authentication/signup.php');
});

$router->get('/signup(/[^/]+(/\d{6})?)?', function ($emailAddress = null, $verificationCode = null) {
  if($emailAddress != null AND $verificationCode != null) {
    header('Location: ../../sites/authentication/signup.php?inputEmail=' . $emailAddress . '&inputVerificationCode=' . $verificationCode);
  } else {
    header('refresh:0.01;url=/404');
  }
});

$router->all('/reset', function () {
  header('Location: ../sites/authentication/forgotPassword.php');
});

$router->get('/reset(/[^/]+(/\d{6})?)?', function ($emailAddress = null, $verificationCode = null) {
  if($emailAddress != null AND $verificationCode != null) {
    header('Location: ../../sites/authentication/forgotPassword.php?inputEmail=' . $emailAddress . '&inputVerificationCode=' . $verificationCode);
  } else {
    header('refresh:0.01;url=/404');
  }
});


$router->all('/profile', function () {
	header('Location: ../sites/usermanagement/profile_main.php');
});

$router->all('/404', function () {
  header('Location: ../404.html');
});

$router->run();
// EOF