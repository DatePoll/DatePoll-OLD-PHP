<?php
require_once(__DIR__ . '/dbConnector.php');
require_once(__DIR__ . '/emailHandler.php');
require_once(__DIR__ . '/permissionHandler.php');
require_once(__DIR__ . '/userHandler/profile_settings.php');
require_once(__DIR__ . '/userHandler/authentication.php');

if (isset($_REQUEST['action'])) {
	switch ($_REQUEST['action']) {
		case 'setPermission':
			if (getPermissions()['isAdmin'])
				$res = setPermission($_REQUEST['userID'], $_REQUEST['key'], $_REQUEST['bool']);
			else
				$res = 'notPermitted';
			break;
		case 'addUser':
			$res = createUser($_REQUEST);
			break;
		case 'editUser':
			$res = updateUser($_REQUEST);
			break;
		case 'removeUser':
			$res = removeUser($_REQUEST['userID']);
			break;
	}
	echo $res;
}

/**
 * creates a new user
 *
 * @param $userdata associative array containing all fields matching the table 'users', except userID
 * @return boolean  false, if $userdata is not set
 */
function createUser($userdata) {
	if (!isset($userdata))
		return 'false';

	$userdata['password'] = getDefaultPassword();
	$userdata['zip_code'] = (int)$userdata['zip_code'];
	$userdata['active'] = 1;

	try {
		$conn = connect();

		$stmt = $conn->prepare('INSERT INTO `users` (`userID`, `password`, `firstname`, `surname`, `email`, `title`, `location`, `streetname`, `streetnumber`, `zip_code`, `birthdate`, `joindate`, `active`, `hasBeenActivated`, `activationCode`, `confirmedEmailAddress`, `userNotes`, `adminNotes`) VALUES (NULL, NULL, :firstname, :surname, :email, :title, :location, :streetname, :streetnumber, :zip_code, :birthdate, :joindate, :active, 1, 0, 0, null, null)');

		$stmt->bindParam(':firstname', $userdata['firstname'], PDO::PARAM_STR, 128);
		$stmt->bindParam(':surname', $userdata['surname'], PDO::PARAM_STR, 128);
		$stmt->bindParam(':email', $userdata['email'], PDO::PARAM_STR, 128);
		$stmt->bindParam(':title', $userdata['title'], PDO::PARAM_STR, 64);
		$stmt->bindParam(':location', $userdata['location'], PDO::PARAM_STR, 128);
		$stmt->bindParam(':streetname', $userdata['streetname'], PDO::PARAM_STR, 128);
		$stmt->bindParam(':streetnumber', $userdata['streetnumber'], PDO::PARAM_STR, 16);
		$stmt->bindParam(':zip_code', $userdata['zip_code'], PDO::PARAM_INT, 11);
		$stmt->bindParam(':birthdate', $userdata['birthdate'], PDO::PARAM_STR, 10);
		$stmt->bindParam(':joindate', $userdata['joindate'], PDO::PARAM_STR, 10);
		$stmt->bindParam(':active', $userdata['active'], PDO::PARAM_INT, 4);

		$stmt->execute();

		$stmt = $conn->prepare('SELECT userID FROM users WHERE email = :email');
		$stmt->bindParam(':email', $userdata['email']);
		$stmt->execute();
		$userID = $stmt->fetch()[0];

		$hashed_password = hash("sha512", $userdata['password'] . $userID);

		$stmt = $conn->prepare('UPDATE users SET password = :password WHERE userID = :userID;');
		$stmt->bindParam(':userID', $userID);
		$stmt->bindParam(':password', $hashed_password);
		$stmt->execute();

		$code = rand(100000, 999999);

		$stmt = $conn->prepare('UPDATE users SET activationCode = :code WHERE userID = :userID;');
		$stmt->bindParam(':userID', $userID);
		$stmt->bindParam(':code', $code);
		$stmt->execute();

		sendActivationCodeToUser($userdata['email'], $userdata['firstname'] . " " . $userdata['surname'], $code);
	} catch (PDOException $e) {
		return 'false';
	}
	return 'true';
}

/**
 * update a user
 *
 * @param $userdata associative array containing the changed data and the userID of the user to update in $userdata['userID']
 * @return boolean  false, if $userdata is not set
 */
function updateUser($userdata) {
	if (!isset($userdata))
		return 'false';

	$permissions = getPermissions();
	// Darf der Benutzer seine eigenen Notizen sehen?
	if ($permissions['isAdmin'] || $_REQUEST['userID'] != $_SESSION['userID']) {
		//Hat der Benutzer eine Berechtigung?
		if ($permissions['isAdmin'] ||
				$permissions['canManageUsers'] ||
				$permissions['canManagePolls'] ||
				$permissions['canManageOrganisation'] ||
				$permissions['canManageGroups'] ||
				$permissions['canManageSubgroups'])
			$permissions = true;
		else
			$permissions = false;
	} else
		$permissions = false;

	try {
		$userdata['zip_code'] = (int)$userdata['zip_code'];
		$userdata['userID'] = (int)$userdata['userID'];

		$conn = connect();

		$stmt = $conn->prepare('UPDATE `users` SET `firstname`= :firstname,`surname`= :surname,`email`= :email,`title`= :title,`location`= :location,`streetname`= :streetname,`streetnumber`= :streetnumber,`zip_code`= :zip_code,`birthdate`= :birthdate,`joindate`= :joindate, `userNotes` = :userNotes' . ($permissions ? ', `adminNotes` = :adminNotes' : '') . ' WHERE `userID` = :userID');

		$stmt->bindParam(':firstname', $userdata['firstname'], PDO::PARAM_STR, 128);
		$stmt->bindParam(':surname', $userdata['surname'], PDO::PARAM_STR, 128);
		$stmt->bindParam(':email', $userdata['email'], PDO::PARAM_STR, 128);
		$stmt->bindParam(':title', $userdata['title'], PDO::PARAM_STR, 64);
		$stmt->bindParam(':location', $userdata['location'], PDO::PARAM_STR, 128);
		$stmt->bindParam(':streetname', $userdata['streetname'], PDO::PARAM_STR, 128);
		$stmt->bindParam(':streetnumber', $userdata['streetnumberr'], PDO::PARAM_STR, 16);
		$stmt->bindParam(':zip_code', $userdata['zip_code'], PDO::PARAM_INT, 11);
		$stmt->bindParam(':birthdate', $userdata['birthdate'], PDO::PARAM_STR);
		$stmt->bindParam(':joindate', $userdata['joindate'], PDO::PARAM_STR);
		$stmt->bindParam(':userNotes', $userdata['userNotes'], PDO::PARAM_STR, 2000);
		$stmt->bindParam(':adminNotes', $userdata['adminNotes'], PDO::PARAM_STR, 2000);
		$stmt->bindParam(':userID', $userdata['userID'], PDO::PARAM_INT);

		$stmt->execute();
		active($userdata['userID'], $userdata['active'] == 'true');
		hasBeenActivated($userdata['userID'], $userdata['hasBeenActivated'] == 'true');
	} catch (PDOException $e) {
		echo $e->getMessage();
		return 'false';
	}
	return 'true';
}

/**
 * removes a user
 *
 * @param $userID   userID of the user to remove
 */
function removeUser($userID) {
	try {
		$conn = connect();

		$stmt = $conn->prepare("DELETE FROM users WHERE userID = :userID");
		$stmt->bindParam(':userID', $userID);

		$stmt->execute();
		return 'true';
	} catch (PDOException $e) {
		echo $e->getMessage();
		return 'false';
	}
}

function editNotes($userdata) {
	try {
		$conn = connect();
		$stmt = $conn->prepare('UPDATE users SET userNotes = :userNotes, adminNotes = :adminNotes WHERE userID = :userID');

		$stmt->bindParam(':userNotes', $userdata['userNotes'], PDO::PARAM_STR, 2000);
		$stmt->bindParam(':adminNotes', $userdata['adminNotes'], PDO::PARAM_STR, 2000);
		$stmt->bindParam(':userID', $userdata['userID'], PDO::PARAM_INT);

		$stmt->execute();
		return 'true';
	} catch (PDOException $e) {
		echo $e->getMessage();
	}
	return 'false';
}

/**
 * checks, if the user with the given ID is existing
 *
 * @param $userID ID-number to check
 * @return boolean  returns true, if the user exists
 */
function existUser($userID) {
	$conn = connect();

	$stmt = $conn->prepare("SELECT userID FROM `users` WHERE userID = :userID");

	$stmt->bindParam(':userID', $userID);

	$stmt->execute();

	if (count($stmt->fetchAll()) > 0)
		return true;
	else
		return false;
}

/**
 * Gets the content of the table 'users' and returns it.
 * @param $userID  optional userID of the user to return
 * @return array  associative array with all users and columns
 */
function getUsers($userID = null) {
	$stmt = 'SELECT * FROM users';
	if (isset($userID))
		$stmt .= ' WHERE userID = :userID';

	$conn = connect();

	$stmt = $conn->prepare($stmt);
	if (isset($userID))
		$stmt->bindParam(':userID', $userID, PDO::PARAM_INT);
	$stmt->execute();

	if (isset($userID))
		return $stmt->fetch();
	return $stmt->fetchAll();
}

/**
 * set/get, if the user is an active member
 *
 * @param $userID   the user to set/get the active-state
 * @param $active   sets the given user active/inactive state in case of true/false, retruns the current state if null
 * @return string   returns the current 'active' state if $active is null
 */
function active($userID, $active = null) {
	$conn = connect();

	if (isset($active)) {
		if ($active || !$active) {
			$stmt = $conn->prepare("UPDATE users SET active = :active WHERE userID = :userID");

			$stmt->bindParam(':userID', $userID);
			$stmt->bindParam(':active', $active, PDO::PARAM_INT);

			$stmt->execute();
		}
	} else {
		$stmt = $conn->prepare("SELECT active FROM `users` WHERE userID = :userID");

		$stmt->bindParam(':userID', $userID);

		$stmt->execute();

		return $stmt->fetch()[0] == 1 ? true : false;
	}
}

/**
 * set/get, if the user's account has been activated
 *
 * @param string $userID   the user to set/get the activation-state
 * @param bool $state    sets the given user active/inactive state in case of true/false, retruns the current state if null
 * @return string   returns the current state if $state is null
 */
function hasBeenActivated($userID, $state = null) {
	$conn = connect();
	if (isset($state)) {
		if ($state || !$state) {
			$stmt = $conn->prepare("UPDATE users SET hasBeenActivated = :state WHERE userID = :userID");

			$stmt->bindParam(':userID', $userID);
			$stmt->bindParam(':state', $state, PDO::PARAM_INT);

			$stmt->execute();
		}
	} else {
		$stmt = $conn->prepare("SELECT hasBeenActivated FROM `users` WHERE userID = :userID");

		$stmt->bindParam(':userID', $userID);

		$stmt->execute();
		return $stmt->fetch()[0] == 1 ? true : false;
	}

}

/**
 * set/get, if the user has confirmed his email address
 *
 * @param int $userID   the user to set/get the email address confirmed state
 * @param bool $state    sets the given user email confirmed state in case of true/false, retruns the current state if null
 * @return string   returns the current state if $state is null
 */
function confirmedEmailAddress($userID, $state = null) {
	$conn = connect();

	if (isset($state)) {
		if ($state || !$state) {
			$stmt = $conn->prepare("UPDATE users SET confirmedEmailAddress = :state WHERE userID = :userID");

			$stmt->bindParam(':userID', $userID);
			$stmt->bindParam(':state', $state);

			$stmt->execute();
		}
	} else {
		$stmt = $conn->prepare("SELECT confirmedEmailAddress FROM `users` WHERE userID = :userID");

		$stmt->bindParam(':userID', $userID);

		$stmt->execute();

		return $stmt->fetch()[0] == 1 ? true : false;
	}
}

/**
 * list all phone-numbers of the given user
 */
function getTelephoneNumbers() {
	$conn = connect();

	$stmt = $conn->prepare("SELECT number, label from telephone_numbers WHERE userID = :userID");
	$stmt->bindParam(':userID', $_SESSION['userID']);

	$stmt->execute();

  $stmt = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $stmt;
}

/**
 * Lists all users not having the provided permission
 *
 * @param $permission  name of the permission to not have
 * @return array|bool  array of userdata including the correspondign permissions or false, if the permission doesn't exist
 */
function getUsersNotHavingPermission($permission) {
	$isValid = false;
	$cols = getPermissions('-1');

	foreach ($cols as $col) {
		if ($permission == $col) {
			$isValid = $permission;
		}
	}

	if ($isValid) {
		$conn = connect();

		$stmt = $conn->prepare("SELECT users.* FROM users LEFT JOIN permissions ON users.userID = permissions.userID 
																				WHERE users.userID NOT IN 
																				(SELECT userID FROM permissions WHERE " . $isValid . " = 1 OR isAdmin = 1)
																				ORDER BY users.firstname ASC");
		$stmt->execute();
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}
	return false;
}

/**
 * sets a specified permission to the specified value of a user
 *
 * @param $userID  of the user to set the permission
 * @param $permission  to set
 * @param $value to set (boolean)
 * @return string  'true' or 'false' if successful or not
 */
function setPermission($userID, $permission, $value) {
	try {
		$value = $value == 'true' ? true : false;

		$isValid = false;
		$cols = getPermissions('-1');

		foreach ($cols as $col) {
			if ($permission == $col) {
				$isValid = $permission;
			}
		}

		if ($isValid) {
			$conn = connect();

			$stmt = $conn->prepare("UPDATE `permissions` SET `" . $isValid . "` = :value WHERE `permissions`.`userID` = :userID ");
			$stmt->bindParam(':value', $value, PDO::PARAM_INT);
			$stmt->bindParam(':userID', $userID, PDO::PARAM_INT);
			$stmt->execute();
		} else {
			return 'false';
		}
	} catch (PDOException $e) {
		return 'false';
	}
	return 'true';
}

/**
 * gets all data of users, who can manage users
 * @return array  array of userdata and permission of users, who can manage users + name "firstname surname"
 */
function getUserManagers() {
	$conn = connect();

	$stmt = $conn->query("SELECT *, concat(firstname, ' ', surname) as name FROM users, permissions WHERE users.userID = permissions.userID AND (permissions.canManageUsers = 1 OR permissions.isAdmin = 1) ORDER BY isAdmin DESC, canManageUsers DESC, users.firstname ASC", PDO::FETCH_ASSOC);
	$stmt->execute();

	return $stmt->fetchAll();
}

/**
 * gets all data of users, who can manage groups
 * @return array  array of userdata and permission of users, who can manage groups + name "firstname surname"
 */
function getGroupManagers() {
	$conn = connect();

	$stmt = $conn->query("SELECT *, concat(firstname, ' ', surname) as name FROM users, permissions WHERE users.userID = permissions.userID AND (permissions.canManageGroups = 1 OR permissions.isAdmin = 1) ORDER BY isAdmin DESC, canManageGroups DESC, users.firstname ASC", PDO::FETCH_ASSOC);
	$stmt->execute();

	return $stmt->fetchAll();
}

/**
 * gets all data of users, who can manage polls
 * @return array  array of userdata and permission of users, who can manage polls + name "firstname surname"
 */
function getPollManagers() {
	$conn = connect();

	$stmt = $conn->query("SELECT *, concat(firstname, ' ', surname) as name FROM users, permissions WHERE users.userID = permissions.userID AND (permissions.canManagePolls = 1 OR permissions.isAdmin = 1) ORDER BY isAdmin DESC, canManagePolls DESC, users.firstname ASC", PDO::FETCH_ASSOC);
	$stmt->execute();

	return $stmt->fetchAll();
}

/**
 * gets all data of users, who can manage the organistaion
 * @return array  array of userdata and permission of users, who can manage the organisation + name "firstname surname"
 */
function getOrganisationManagers() {
	$conn = connect();

	$stmt = $conn->query("SELECT *, concat(firstname, ' ', surname) as name FROM users, permissions WHERE users.userID = permissions.userID AND (permissions.canManageOrganisation = 1 OR permissions.isAdmin = 1) ORDER BY isAdmin DESC, canManageOrganisation DESC, users.firstname ASC", PDO::FETCH_ASSOC);
	$stmt->execute();

	return $stmt->fetchAll();
}

/**
 * Check if email address is already in use
 * @param string $email Email Address
 * @return boolean  returns true when email is already used
 */
function emailAddressAlreadyUsed($email) {
	$conn = connect();

	$stmt = $conn->prepare('SELECT userID FROM users WHERE email = :email;');
	$stmt->bindParam(':email', $email);
	$stmt->execute();
	$emailAddressIsUsed = $stmt->rowCount();

	return ($emailAddressIsUsed > 0);
}

/**
 * Get activation code
 * @param $userID int id for user
 * @param $code int if code is not set the function will return the activation code
 * @return boolean  returns activation code
 */
function activationCode($userID, $code = -11) {
	$conn = connect();

	if ($code != -11) {
		$stmt = $conn->prepare("UPDATE users SET activationCode = :code WHERE userID = :userID");

		$stmt->bindParam(':userID', $userID);
		$stmt->bindParam(':code', $code);

		$stmt->execute();

	} else {
		$stmt = $conn->prepare("SELECT activationCode FROM `users` WHERE userID = :userID");

		$stmt->bindParam(':userID', $userID);

		$stmt->execute();
		return $stmt->fetch()[0];
	}
}

function emailAddress($userID, $emailAddress = null) {
	$conn = connect();

	if (isset($emailAddress)) {
		$stmt = $conn->prepare("UPDATE users SET email = :email WHERE userID = :userID");

		$stmt->bindParam(':userID', $userID);
		$stmt->bindParam(':email', $emailAddress);

		$stmt->execute();

	} else {
		$stmt = $conn->prepare("SELECT email FROM `users` WHERE userID = :userID");

		$stmt->bindParam(':userID', $userID);

		$stmt->execute();
		return $stmt->fetch()[0];
	}
}

/**
 * Get UserID by Email Address
 * @param $email Email Address
 * @return boolean  returns the userID
 */
function getUserIDByEmailAddress($emailAddress) {
	$conn = connect();

	$stmt = $conn->prepare('SELECT userID FROM users WHERE email = :email;');
	$stmt->bindParam(':email', $emailAddress);
	$stmt->execute();

	$userID = $stmt->fetch()[0];

	return $userID;
}

function getEmailByUserID($userID) {
	$conn = connect();

	$stmt = $conn->prepare('SELECT email FROM users WHERE userID = :userID;');
	$stmt->bindParam(':userID', $userID);
	$stmt->execute();

	$emailAddress = $stmt->fetch()[0];

	return $emailAddress;
}

function getFirstnameAndLastnameByUserID($userID) {
	$conn = connect();

	$stmt = $conn->prepare('SELECT firstname, surname FROM users WHERE userID = :userID;');
	$stmt->bindParam(':userID', $userID);
	$stmt->execute();

	while ($row = $stmt->fetch()) {
		$name = $row[0] . ' ' . $row[1];
		return $name;
	}
}

/**
 * Performes a basic check for accessing a page in DatePoll
 *  -  is the user logged in?
 *  -  is the email of the user verified?
 *  -  is the user confirmed by an admin (hasbBeenActivated)?
 *
 * @return bool  true, if the user is allowed to access a basic page
 */
function canAccessBasicPage() {
	if (!isset($_SESSION))
		session_start();
	return isLoggedIn() and hasBeenActivated($_SESSION['userID']) and confirmedEmailAddress($_SESSION['userID']);
}

/**
 * @param $month
 * @return array
 */
function getBirthdayOfTheMonth($month = null) {
	if(!isset($month) or ($month > 12 or $month < 1))
		$month = date('n'); //numeric month without leading zeros 1-12

	$conn = connect();
	$stmt = $conn->prepare("SELECT userID, firstname, surname, birthdate FROM users WHERE MONTH(birthdate) = :month ORDER BY DAY(birthdate)");
	$stmt->bindParam(':month', $month, PDO::PARAM_INT);
	$stmt->execute();
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
}