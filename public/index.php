<?php

error_reporting(-1);
ini_set('display_errors', true);

require_once('./logic/userHandler.php');

if (!isLoggedIn()) {
	header('Location: ./startpage.php?do=logInFirst');
	die();
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<meta name="description" content="Dies ist der Terminplan des ..."/>
	<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
	<link rel="icon" href="img/favicon.ico" type="image/x-icon">
	<title>DatePoll</title>

	<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/datepoll.css"/>
  <link rel="stylesheet" href="node_modules/animate.css/animate.min.css">
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<a class="navbar-brand" href="#!/home">DatePoll</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
	        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">

		<ul class="navbar-nav mr-auto">
			<li class="nav-item active">
				<a class="nav-link" href="#!/home"><i class="fas fa-home"></i>  Home <span class="sr-only">(current)</span></a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="#!/poll/vote")">
					<i class="fas fa-calendar-check"></i> Abstimmungen
				</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="#!/group">
					<i class="fas fa-users"></i> Abteilungen & Gruppen
				</a>
			</li>

			<li class="nav-item">
				<a class="nav-link" href="#!/organisation">
					<i class="fas fa-sitemap"></i> Mein Verein
				</a>
			</li>

			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#!/admin/poll" id="navbarManagement" role="button" data-toggle="dropdown"
             aria-haspopup="true" aria-expanded="false">
					<i class="fas fa-cogs"></i> Verwaltung
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarManagement" id="navbarManagementSupport">
					<?php
						$permissions = getPermissions();
						if($permissions['canManagePolls'] == 1 or $permissions['isAdmin'] == 1) {
							?>
							<a class="dropdown-item" href="#!/admin/poll">
								<i class="fas fa-calendar-check"></i> Abstimmungen</a>
							<?php
						}
						if($permissions['canManageUsers'] == 1 or $permissions['isAdmin'] == 1) {
							?>
							<a class="dropdown-item" href="#!/admin/user">
								<i class="fas fa-user"></i> Benutzer</a>
							<?php
						}
						if($permissions['canManageGroups'] == 1 or $permissions['isAdmin'] == 1) {
							?>
							<a class="dropdown-item" href="#!/admin/group">
								<i class="fas fa-users"></i> Abteilungen & Gruppen</a>
							<?php
						}
						if($permissions['canManageOrganisation'] == 1 or $permissions['isAdmin'] == 1) {
							?>
							<a class="dropdown-item" href="#!/admin/organisation">
								<i class="fas fa-sitemap"></i> Verein</a>
							<?php
						}
						if($permissions['isAdmin'] == 1) {
							?>
						<a class="dropdown-item" href="#!/admin/permission">
							<i class="fas fa-tasks"></i> Rechte</a>
							<?php
						}
					if($permissions['isAdmin'] == 1) {
						?>
						<a class="dropdown-item" href="#!/admin/settings">
							<i class="fas fa-cog"></i> Einstellungen</a>
						<?php
					}
					?>
				</div>
			</li>

		</ul>

    <form class="form-inline my-2 my-lg-0" style="margin-right: 10px;">
      <input class="form-control mr-sm-2" type="search" placeholder="Suchbegriff eingeben" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">
        <i class="fas fa-search"></i> Suchen
      </button>
    </form>

    <ul class="navbar-nav right">
      <li class="nav-item" style="margin-right: 10px;">

      </li>
      <li class="nav-item dropleft">
          <button type="button" class="btn btn-secondary dropdown-toggle" role="button" data-toggle="dropdown" id="navbarSettings"
                  aria-haspopup="true" aria-expanded="false" style="margin-right: 10px;" href="#!/profile/edit/data">
            <i class="fas fa-cog "></i>
          </button>
          <div class="dropdown-menu" aria-labelledby="navbarSettings" id="navbarSettingsSupport">
            <a class="dropdown-item" href="#!/profile/edit/data">
              <i class="fas fa-user-circle"></i> Deine Daten
            </a>
            <a class="dropdown-item" href="#!/profile/edit/password">
              <i class="fas fa-lock"></i> Passwort ändern
            </a>
            <a class="dropdown-item" href="#!/profile/edit/email">
              <i class="fas fa-envelope"></i> Email ändern
            </a>
            <a class="dropdown-item" href="#!/profile/edit/telephonenumber">
              <i class="fas fa-phone"></i> Telefonnummern verwalten
            </a>
            <a class="dropdown-item" href="#!/profile/edit/tokens">
              <i class="fas fa-address-card"></i> Angemeldete Geräte verwalten
            </a>
	          <a class="dropdown-item" href="#!/profile/edit/other">
		          <i class="fas fa-info-circle"></i> Andere Informationen
	          </a>

        </div>

      </li>
      <li class="nav-item">
        <a class="btn btn-danger" href="#!/auth/logout" ><i class="fas fa-sign-out-alt"></i></a>
      </li>
    </ul>

	</div>
</nav>

<div class="container-fluid" style="padding-top: 15px;">
	<div class="card">
		<h1 class="mx-auto">Hallo <?php echo $_SESSION['firstname'] . ' ' . $_SESSION['surname']; ?>!</h1>
		<div class="alert-danger">
			<h2>Um diese Seite nutzen zu können, benötigst du JavaScript!</h2>
		</div>
	</div>
</div>

<?php require_once ('js/jsImports.php'); getScripts();?>
</body>
</html>