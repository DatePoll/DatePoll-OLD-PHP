<?php
require_once (__DIR__ . '/logic/userHandler.php');
require_once (__DIR__ . '/logic/parser.php');
?>

<!DOCTYPE html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <meta name="description" content="Dies ist der Terminplan des ..."/>
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="icon" href="img/favicon.ico" type="image/x-icon">
  <title>DatePoll</title>

  <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="css/startpage.css"/>
  <link rel="stylesheet" href="node_modules/animate.css/animate.min.css">

  <style>
    .carousel-inner img {
      width: 100%;
      height: 100%;
    }
  </style>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">DatePoll</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
          aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
    </ul>
    <div class="my-2 my-lg-0" style="margin-right: 10px;">
      <button class="btn btn-secondary my-2 my-sm-0"
              onclick="load_modal('sites/authentication/signupModal.php', 'modalContainer', function() {
                $('#enterInputs').collapse('show');
              })"
        ><i class="fas fa-hand-point-right"></i> Register</button>
    </div>
    <div class="my-2 my-lg-0">
      <button class="btn btn-primary my-2 my-sm-0"
              onclick="load_modal('sites/authentication/loginModal.php', 'modalContainer')"
        >Login <i class="fas fa-sign-in-alt"></i></button>
    </div>
  </div>
</nav>

<div id="startpageSlide" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#startpageSlide" data-slide-to="0" class="active"></li>
    <li data-target="#startpageSlide" data-slide-to="1"></li>
    <li data-target="#startpageSlide" data-slide-to="2"></li>
  </ul>

  <!-- The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="img/startpage/slider/slide_1.jpg" alt="Slider 1" width="4592" height="1956">
      <div class="carousel-caption text-left">
        <p class="slider-headline-1">DatePoll</p>
        <p class="slider-headline-2">Termine einfach schneller finden: Verkürze mit dem Online-Terminplaner DatePoll
          radikal den Prozess der Terminabstimmung für Personengruppen. Völlig kostenlos!</p>
        <p>
          <a href="https://github.com/fschaupp/DatePoll" target="_blank" class="btn btn-primary btn-lg">Erfahre mehr!</a>
        </p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/startpage/slider/slide2_cut.jpg" alt="Slider 2" width="4592" height="1956">
      <div class="carousel-caption text-left">
        <p class="slider-headline-1">Zweiter Text</p>
        <p class="slider-headline-2">Wuiiiiii</p>
      </div>
    </div>
    <div class="carousel-item">
      <img src="img/startpage/slider/slide3_cut.jpg" alt="Slider 3" width="4592" height="1956">
      <div class="carousel-caption text-left">
        <p class="slider-headline-1">Dritter Text</p>
        <p class="slider-headline-2">Wuiiiiiiuiuiui</p>
      </div>
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#startpageSlide" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#startpageSlide" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

<div class="container-fluid">
  <div style="margin-top: 15px;"></div>
  <?php echo parseStandardAlerts(); ?>

  <div class="row" style="margin-left: 15px; margin-right: 15px; margin-bottom: 15px;">
    <div class="card-deck">
      <div class="card">
        <img class="card-img-top" src="img/startpage/card_1.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Geschwindigkeit</h5>
          <p class="card-text">Mit großen Gruppen ist es schwer Termine zu finden! DatePoll bietet die Möglichkeit die Terminfind trastisch zu vereinfachen!</p>
          <!--<p class="card-text"><small class="text-muted">Test</small></p>-->
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="img/startpage/card2.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Titel 2</h5>
          <p class="card-text">Viel catchy text.</p>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="img/startpage/card3.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Titel 3</h5>
          <p class="card-text">noch mehr catchy text</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="modalContainer"></div>

<!--Scripts needed for Bootstrap-->
<script type="application/javascript" src="node_modules/jquery/dist/jquery.slim.min.js"></script>
<script type="application/javascript" src="node_modules/popper.js/dist/umd/popper.min.js"></script>
<script type="application/javascript" src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<!--Script for FontAwesome-->
<script type="application/javascript" src="js/fontawesome-all.js"></script>

<!--Basic JS for DatePoll-->
<script type="application/javascript" src="js/serverHandler.js"></script>
<script type="application/javascript" src="js/sites/authentication.js"></script>
<script type="application/javascript" src="js/animate.js"></script>
<script type="application/javascript" src="js/cookieHandler.js"></script>

<script type="application/javascript">

  function checkValue() {
    let max = 9999;
    let min = 1000;

    let element = document.getElementById("inputZipcode");
    if (element.value > max) {
      element.value = max;
    }
    if (element.value < min) {
      element.value = min;
    }
  }

  <?php

  if(isset($_REQUEST['do'])) {
    switch ($_REQUEST['do']) {
      case 'logInFirst':
        echo 'load_modal("sites/authentication/loginModal.php", "modalContainer");';

        break;
      case "passwordReset":
        if(isset($_REQUEST['authenticationCode']) AND isset($_REQUEST['emailAddress'])) {
          $email = $_REQUEST['emailAddress'];
          $code = $_REQUEST['authenticationCode'];

          if(activationCode(getUserIDByEmailAddress($email)) == $code) {
            echo '
              let toExecuteAfterOpen = function() {
                let inputEmailAddress = document.getElementById("inputEmailAddress");
                let inputCode = document.getElementById("inputCode");
            
                inputEmailAddress.value = "'.$email.'";
                inputCode.value = "'. $code .'";
            
                $("#enterEmail").collapse("hide");
                hide(document.getElementById("sendPasswordResetVerificationButton"));
            
                $("#enterNewPasswords").collapse("show");
                unhide(document.getElementById("resetPasswordButton"));
              };
          
              load_modal("sites/authentication/forgotPasswordModal.php", "modalContainer", toExecuteAfterOpen);
            ';
          }
        }

        break;
      case "signup":
        if(isset($_REQUEST['authenticationCode']) AND isset($_REQUEST['emailAddress'])) {
          $email = $_REQUEST['emailAddress'];
          $code = $_REQUEST['authenticationCode'];

          if(activationCode(getUserIDByEmailAddress($email)) == $code) {
            $userID = getUserIDByEmailAddress($email);

            confirmedEmailAddress($userID, true);

            activationCode($userID, 0);
            echo '
            
            let toExecuteAfterOpen = function() {
              $("#enterInputs").collapse("hide");
              $("#enterActivationCode").collapse("hide");
              $("#verificationSuccess").collapse("show");
              
              hide(document.getElementById("closeButton"));
              hide(document.getElementById("signupButton"));
              hide(document.getElementById("activationCodeSendButton"));
              unhide(document.getElementById("finishButton"));
            };
          
            load_modal("sites/authentication/signupModal.php", "modalContainer", toExecuteAfterOpen);
            ';
          } else {
            echo 'console.log("Activationcode is wrong");';
          }
        }

        break;

      case "logout":
        //Destroy session
        if(isLoggedIn()) {
          logout();
        }

        //Destroy all cookies via js
        echo '
          logout();
        ';

        break;
      default:
        echo 'console.log("Startpage - Unkown action: '. $_REQUEST['action'] . '");';
        break;
    }
  }
  ?>

  //Correct the url so the parameters aren't displayed and on reload it won't execute anything
  let stateObj = { startpage: "startpage" };
  history.pushState(stateObj, "Startpage", "startpage.php");

  stayLoggedIn();
</script>

</body>

<?php
if(isLoggedIn()) {
  header('Location: ./index.php');
  die();
}
?>