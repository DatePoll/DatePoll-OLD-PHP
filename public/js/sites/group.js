/**
 * Loads the modal to add a group
 */
function addGroup() {
  load_modal('sites/group/add.php', 'modalContainer');
}

/**
 * submits the add dialog
 * @param button    the submit-button of the modal
 */
function addGroupSubmit(button) {
  var form = button.parentElement.getElementsByTagName('input');
  var name = form[0].value;
  var description = form[1].value;
  var action = form[2].value;

  var cfunc = function (xhttp) {
    //console.log(xhttp.responseText);
    if (xhttp.responseText == 'true')
      load_page("sites/group/admin.php?successful=add");
    else
      load_modal("sites/group/add.php?error=true", 'modalContainer');
  };

  xhr_post('../logic/groupHandler.php', cfunc, 'action=' + action +
    '&name=' + name +
    '&description=' + description);
}

/**
 * Loads the modal to add a group
 */
function addSubgroup(groupID) {
  $('.modal').modal('hide');
  load_modal('sites/group/addSubgroup.php?groupID=' + groupID, 'modalContainer');
}

/**
 * submits the add dialog
 * @param button    the submit-button of the modal
 */
function addSubgroupSubmit(button) {
  var form = button.parentElement.getElementsByTagName('input');
  var name = form[0].value;
  var description = form[1].value;
  var action = form[2].value;
  var groupID = form[3].value;

  var cfunc = function (xhttp) {
    //console.log(xhttp.responseText);
    if (xhttp.responseText == 'true'){
      $('.modal').modal('hide');
      getGroupDetails(groupID);
    }
    else
      load_modal("sites/group/addSubgroup.php?error=true", 'modalContainer');
  };

  xhr_post('../logic/groupHandler.php', cfunc, 'action=' + action +
    '&name=' + name +
    '&description=' + description +
    '&groupID=' + groupID);
}

/**
 * loads the modal for editing a group
 * @param groupID    groupID of the group to edit
 */
function editGroup(groupID) {
  load_modal("sites/group/edit.php?groupID=" + groupID, 'modalContainer');
}

/**
 * submits the edit dialog
 * @param button    the submit-button of the modal
 */
function editGroupSubmit(button) {
  var form = button.parentElement.getElementsByTagName('input');
  var name = form[0].value;
  var description = form[1].value;
  var groupID = form[2].value;
  var action = form[3].value;

  var cfunc = function (xhttp) {
    if (xhttp.responseText == 'true')
      load_page("sites/group/admin.php?successful=edit");
    else
      load_modal("sites/group/edit.php?error=true", 'modalContainer');
  };

  xhr_post('../logic/groupHandler.php', cfunc, 'action=' + action +
    '&groupID=' + groupID +
    '&name=' + name +
    '&description=' + description);
}

/**
 * deletes the given group
 * @param groupID    groupID of the group to delete
 */
function deleteGroup(groupID) {
  xhr_post('../logic/groupHandler.php', function (xhttp) {
    if (xhttp.responseText == 'true')
      load_page("sites/group/admin.php?successful=remove");
    else
      load_page("sites/group/admin.php?error=remove");
  }, 'action=removeGroup&groupID=' + groupID);
}

/**
 * gets the details of the given group and loads it into the container with the id 'groupDetail' and enables the container (display: block)
 * @param groupID    groupID of the group to get the details of
 */
function getGroupDetails(groupID) {
  xhr_post('../sites/group/detail.php', function (xhttp) {
    var detail = document.getElementById('groupDetail');
    detail.innerHTML = xhttp.responseText;
    detail.parentElement.parentElement.style.display = 'block';
  }, 'actiton=getGroup&groupID=' + groupID);
}

function moveUser(userID, currentSubgroupID){
  $('.modal').modal('hide');
  load_modal('sites/group/moveUserModal.php?userID=' + userID + '&currentSubgroupID=' + currentSubgroupID, 'modalContainer');
}

function moveUserSubmit(userID, currentSubgroupID, newSubgroupID){
  let cfunc = function(xhttp){
    if(xhttp.responseText == 'true'){
      $('#DatePollModal').modal('hide');
      location.reload();
    }
    else
      console.error(xhttp.responseText);
  };

  xhr_post('../logic/groupHandler.php', cfunc, 'action=moveUser' +
    '&userID=' + userID + 
    '&currentSubgroupID=' + currentSubgroupID +
    '&newSubgroupID=' + newSubgroupID);
}

function manageSubgroup(groupID){
  load_modal("sites/group/subgroupDetail.php?groupID=" + groupID, 'modalContainer');
}