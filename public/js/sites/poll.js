function analyzePoll(pollID){
  load_modal("../../logic/pollHandler.php?action=analyzePoll&pollID="+pollID, 'modalContainer');
}

/**
 * votes for a poll
 * @param pollID        the pollID of the poll the user want to vote for
 * @param decisionID    the decisionID of the decition the user took
 */
function voteFor(pollID, decisionID) {
  xhr_post('../logic/pollHandler.php', function (xhttp) {
    if (xhttp.responseText = 'true')
      load_page('../sites/poll/vote/vote.php?successful=voteFor');
    else
      load_page('../sites/poll/vote/vote.php?error=voteFor');
  }, 'action=voteForPoll' +
    '&pollID=' + pollID +
    '&decisionID=' + decisionID);
}

/**
 * changes the decision you took on a poll
 * @param pollID        pollID of the poll the user voted for
 * @param decisionID    the new decisionID of the decision the user now want to put
 */
function updateVoteFor(pollID, decisionID) {
  xhr_post('../logic/pollHandler.php', function (xhttp) {
    if (xhttp.responseText == 'true')
      load_page('../sites/poll/vote/vote.php?successful=updateVoteFor');
    else
      load_page('../sites/poll/vote/vote.php?error=updateVoteFor');
  }, 'action=updateVoteForPoll' +
    '&pollID=' + pollID +
    '&decisionID=' + decisionID);
}