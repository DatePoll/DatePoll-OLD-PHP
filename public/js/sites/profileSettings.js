function editUserDataByHimself(button) {
  let form = button.parentElement.parentElement.getElementsByTagName('input');
  let title = form[0].value;
  let firstname = form[1].value;
  let surname = form[2].value;
  let birthdate = form[3].value;
  let streetname = form[4].value;
  let streetnumber = form[5].value;
  let zip_code = form[6].value;
  let location = form[7].value;

  /*
     Possible alerts:
       - dataNotSet
       - unknownError
       - valid_firstname
       - valid_surname
       - valid_birthdate
       - valid_title
       - valid_streetname
       - valid_location
       - valid_streetnumber
       - valid_zipcode
    */

  let cfunc = function (xhttp) {

    switch (xhttp.responseText) {
      case 'successful':
        showSuccessAlert("Deine Daten wurden erfolgreich bearbeitet!");

        load_main_content_and_change_active('./sites/user/settings/changeUserdata.php', 'changeUserdata');
        break;
      case 'dataNotSet':
        showErrorAlert('Es wurden keine Daten gesetzt!');
        break;
      case 'unknownError':
        showErrorAlert('Es kam zu einen PDO Error!');
        break;
      case 'valid_firstname':
        showErrorAlert('Der Vorname darf nicht kleiner als <strong>2</strong> Zeichen und nicht größer als ' +
          '<strong>128</strong> Zeichen sein. Außerdem darf er nicht leer und aus Leerzeichen bestehen!');
        shake(document.getElementById("inputGroupFirstname"));
        break;
      case 'valid_surname':
        showErrorAlert('Der Nachname darf nicht kleiner als <strong>2</strong> Zeichen und nicht größer als ' +
          '<strong>128</strong> Zeichen sein. Außerdem darf er nicht leer und aus Leerzeichen bestehen!');
        shake(document.getElementById("inputGroupSurname"));
        break;
      case 'valid_birthdate':
        showErrorAlert('Das Geburtsdatum ist kein Datum!');
        shake(document.getElementById("inputGroupBirthdate"));
        break;
      case 'valid_title':
        showErrorAlert('Der Titel darf nicht kleiner als <strong>1</strong> Zeichen und nicht größer als ' +
          '<strong>64</strong> Zeichen sein!');
        shake(document.getElementById("inputGroupTitle"));
        break;
      case 'valid_streetname':
        showErrorAlert('Der Straßenname darf nicht kleiner als <strong>1</strong> Zeichen und nicht größer als ' +
          '<strong>128</strong> Zeichen sein!');
        shake(document.getElementById("inputGroupStreetname"));
        break;
      case 'valid_location':
        showErrorAlert('Dein Wohnort darf nicht kleiner als <strong>1</strong> Zeichen und nicht größer als ' +
          '<strong>128</strong> Zeichen sein!');
        shake(document.getElementById("inputGroupLocation"));
        break;
      case 'valid_streetnumber':
        showErrorAlert('Deine Hausnummer darf nicht kleiner als <strong>1</strong> Zeichen und nicht größer als ' +
          '<strong>16</strong> Zeichen sein!');
        shake(document.getElementById("inputStreetnumber"));
        break;
      case 'valid_zipcode':
        showErrorAlert('Die Postleitzahl muss aus <strong>4</strong> Nummern bestehen!');
        shake(document.getElementById("inputZipcode"));
        break;
      default:
        showErrorAlert('Es wurde eine unbekannte Fehlermeldung returnt!');
        break;
    }
  };

  xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=editUserByHimself' +
    '&birthdate=' + birthdate +
    '&title=' + title +
    '&firstname=' + firstname +
    '&surname=' + surname +
    '&streetname=' + streetname +
    '&streetnumber=' + streetnumber +
    '&zip_code=' + zip_code +
    '&location=' + location);
}

function userDataChange(input) {
  let form = input.parentElement.parentElement.parentElement.parentElement.parentElement.getElementsByTagName('input');
  let title = form[0].value;
  let firstname = form[1].value;
  let surname = form[2].value;
  let birthdate = form[3].value;
  let streetname = form[4].value;
  let streetnumber = form[5].value;
  let zip_code = form[6].value;
  let location = form[7].value;

  let profileSaveButton = document.getElementById('profileSaveButton');

  let valuesChanged = false;

  if (title !== document.getElementById("oldValue_inputTitle").innerText) {
    valuesChanged = true;
  }
  if (firstname !== document.getElementById("oldValue_inputFirstname").innerText) {
    valuesChanged = true;
  }
  if (surname !== document.getElementById("oldValue_inputSurname").innerText) {
    valuesChanged = true;
  }
  if (birthdate !== document.getElementById("oldValue_inputBirthdate").innerText) {
    valuesChanged = true;
  }
  if (streetname !== document.getElementById("oldValue_inputStreetname").innerText) {
    valuesChanged = true;
  }
  if (streetnumber !== document.getElementById("oldValue_inputStreetnumber").innerText) {
    valuesChanged = true;
  }
  if (zip_code !== document.getElementById("oldValue_inputZipcode").innerText) {
    valuesChanged = true;
  }
  if (location !== document.getElementById("oldValue_inputLocation").innerText) {
    valuesChanged = true;
  }

  if (valuesChanged) {
    unhide(profileSaveButton);
  } else {
    hide(profileSaveButton);
  }
}

function checkZipCode(zip_code) {
  let max = 9999;
  let min = 1000;

  if (zip_code.value > max) {
    zip_code.value = max;
  }
  if (zip_code.value < min) {
    zip_code.value = min;
  }
}

function changeInputs() {
  if (document.getElementById('constantInputBox').classList.contains('locked')) {
    document.getElementById('constantInputBox').classList.remove('locked');

    document.getElementById('profileEditButton').classList.remove('btn-outline-primary');
    document.getElementById('profileEditButton').classList.add('btn-primary');

    enableInputs();
  } else {
    document.getElementById('constantInputBox').classList.add('locked');

    document.getElementById('profileEditButton').classList.add('btn-outline-primary');
    document.getElementById('profileEditButton').classList.remove('btn-primary');

    disableInputs();
  }
}

function enableInputs() {
  let inputs = document.getElementsByClassName('inputBox');
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].disabled = false;
  }
}

function disableInputs() {
  let inputs = document.getElementsByClassName('inputBox');
  for (let i = 0; i < inputs.length; i++) {
    inputs[i].disabled = true;
  }
}

function oldPasswordChange() {
  let oldPassword = document.getElementById("inputOldPassword");
  let newPassword = document.getElementById("inputNewPassword");
  let newPasswordAgain = document.getElementById("inputNewPasswordAgain");

  let passwordIncorrect = document.getElementById("passwordIncorrect");

  let icon = document.getElementById("passwordLock");

  if (oldPassword.value.length > 5) {
    let cfunc = function (xhttp) {

      if (xhttp.responseText === "1") {
        showSuccessAlert("Altes Passwort korrekt!");

        enable(newPassword);
        enable(newPasswordAgain);
        disable(oldPassword);
        hide(passwordIncorrect);


        icon.classList.remove("fa-lock");
        icon.classList.add("fa-lock-open");
      } else {
        unhide(passwordIncorrect);

        disable(newPassword);
        disable(newPasswordAgain);
        enable(oldPassword);

        icon.classList.add("fa-lock");
        icon.classList.remove("fa-lock-open");
      }
    };

    xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=checkPassword&password=' + oldPassword.value);

  } else if(oldPassword.value.length === 0) {

    disable(newPassword);
    disable(newPasswordAgain);
    enable(oldPassword);
    hide(passwordIncorrect);

    icon.classList.add("fa-lock");
    icon.classList.remove("fa-lock-open");

  } else {
    disable(newPassword);
    disable(newPasswordAgain);
    enable(oldPassword);
    unhide(passwordIncorrect);

    icon.classList.add("fa-lock");
    icon.classList.remove("fa-lock-open");
  }
}

function newPasswordChange() {
  let newPassword = document.getElementById("inputNewPassword");
  let newPasswordAgain = document.getElementById("inputNewPasswordAgain");
  let sendPasswordVerificationButton = document.getElementById("sendPasswordVerificationButton");
  let passwordNotEqualsAlert = document.getElementById("passwordNotEquals");
  let passwordInvalid = document.getElementById("passwordInvalid");

  if (newPassword.value.length > 5) {

    if(newPassword.value !== newPasswordAgain.value) {
      unhide(passwordNotEqualsAlert);
      unhide(passwordInvalid);
      hide(sendPasswordVerificationButton);

    } else {
      hide(passwordNotEqualsAlert);

      let cfunc = function (xhttp) {
        if (xhttp.responseText === "1") {
          hide(passwordInvalid);
          unhide(sendPasswordVerificationButton);

        } else {
          unhide(passwordInvalid);
          hide(sendPasswordVerificationButton);

        }
      };

      xhr_post('../../logic/validationHandler.php', cfunc, 'action=isValidPassword&password=' + newPassword.value);
    }

  } else if(newPassword.length === 0) {
    hide(passwordNotEqualsAlert);
    hide(passwordInvalid);
    hide(sendPasswordVerificationButton);

  } else {
    hide(passwordNotEqualsAlert);
    unhide(passwordInvalid);
    hide(sendPasswordVerificationButton)

  }
}

function sendPasswordVerificationCode() {
  let oldPassword = document.getElementById("inputOldPassword");
  let newPassword = document.getElementById("inputNewPassword");
  let newPasswordAgain = document.getElementById("inputNewPasswordAgain");

  let cfunc = function (xhttp) {

    if (xhttp.responseText === 'successful') {
      disable(oldPassword);
      disable(newPassword);
      disable(newPasswordAgain);

      let code = document.getElementById("inputPasswordChangeCode");
      enable(code);

      let sendPasswordVerificationButton = document.getElementById("sendPasswordVerificationButton");
      hide(sendPasswordVerificationButton);

      showSuccessAlert("Email wurde versendet! Bitte überprüfe dein Postfach.")

    } else {
      showErrorAlert("Email wurde nicht versendet!")
    }
  };

  xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=sendPasswordChangeEmail');
}


function passwordCodeChange() {
  let code = document.getElementById("inputPasswordChangeCode");
  let changePasswordButton = document.getElementById("changePasswordButton");
  let codeIncorrect = document.getElementById("codeIncorrect");

  if (code.value > 99999 && code.value < 1000000) {
    let cfunc = function (xhttp) {

      if (xhttp.responseText === "true") {
        showSuccessAlert("Code korrekt!");

        unhide(changePasswordButton);
        hide(codeIncorrect);
      } else {
        unhide(codeIncorrect);
        hide(changePasswordButton);
      }
    };

    xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=checkCode&code=' + code.value);


  } else {
    hide(changePasswordButton);
    unhide(codeIncorrect);
  }
}

function changePassword() {
  let oldPassword = document.getElementById("inputOldPassword");
  let newPassword = document.getElementById("inputNewPassword");
  let newPasswordAgain = document.getElementById("inputNewPasswordAgain");

  let code = document.getElementById("inputPasswordChangeCode");

  let sendPasswordVerificationButton = document.getElementById("sendPasswordVerificationButton");
  let changePasswordButton = document.getElementById("changePasswordButton");

  let icon = document.getElementById("passwordLock");

  let cfunc = function (xhttp) {

    switch (xhttp.responseText) {
      case 'successful':
        showSuccessAlert("Dein Passwort wurde erfolgreich bearbeitet!");

        enable(oldPassword);
        disable(newPassword);
        disable(newPasswordAgain);
        disable(code);

        resetInput(oldPassword);
        resetInput(newPassword);
        resetInput(newPasswordAgain);
        resetInput(code);

        hide(sendPasswordVerificationButton);
        hide(changePasswordButton);

        icon.classList.add("fa-lock");
        icon.classList.remove("fa-lock-open");

        break;
      case 'oldPasswordIncorrect':
        showErrorAlert('Dein altes Passwort ist inkorrekt!');

        enable(oldPassword);
        disable(newPassword);
        disable(newPasswordAgain);
        disable(code);

        resetInput(oldPassword);
        resetInput(code);

        icon.classList.add("fa-lock");
        icon.classList.remove("fa-lock-open");

        shake(document.getElementById("inputGroupOldPassword"));
        break;
      case 'newPasswordInvalid':
        showErrorAlert(' Dein neues Passwort darf nicht kleiner als <strong>6</strong> Zeichen sein und nicht größer als ' +
          '<strong>512</strong> Zeichen sein!');

        disable(oldPassword);
        enable(newPasswordAgain);
        enable(newPassword);
        disable(code);

        resetInput(newPassword);
        resetInput(newPasswordAgain);

        shake(document.getElementById("inputGroupNewPassword"));

        break;
      case 'newPasswordsNotEqual':
        showErrorAlert('Deine neuen Passwörter sind nicht gleich!');

        disable(oldPassword);
        enable(newPasswordAgain);
        enable(newPassword);
        disable(code);

        resetInput(newPassword);
        resetInput(newPasswordAgain);

        shake(newPassword);
        shake(newPasswordAgain);

        break;
      case 'verificationCodeIncorrect':
        showErrorAlert('Dein Verifizierungscode ist inkorrekt! Überprüfe ihn bitte erneut!');
        disable(oldPassword);
        disable(newPasswordAgain);
        disable(newPassword);
        enable(code);

        resetInput(code);

        shake(code);

        break;
      default:
        showErrorAlert('Es wurde eine unbekannte Meldung returnt!');
        break;
    }
  };

  xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=changePasswordByHimself' +
    '&oldPassword=' + oldPassword.value +
    '&newPassword=' + newPassword.value +
    '&newPasswordAgain=' + newPasswordAgain.value +
    '&verificationCode=' + code.value);
}

function editOtherDataByHimself() {
  let userNotes = $('#userNotes').val();
  let adminNotes = $('#adminNotes').val();
  let userID = $('#inputUserIDOtherChange').val();

  let cfunc = function (xhttp) {
    if(xhttp.responseText === 'true')
      showSuccessAlert('Beschreibung/Notizen erfolgreich geändert!');
    else {
      showErrorAlert('Da hat was nicht funktioniert!');
      console.log(xhttp.responseText);
    }
  };

  xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=editNotes' +
    '&userNotes=' + userNotes +
    '&adminNotes=' + adminNotes +
    '&userID=' + userID);
}

function emailChangePasswordChange() {
  let inputPassword = document.getElementById("inputEmailChangePassword");
  let newEmail = document.getElementById("inputNewEmail");
  let passwordInCorrect = document.getElementById("passwordInCorrect");
  let passwordToTiny = document.getElementById("passwordToTiny");

  if(inputPassword.value.length > 5) {
    hide(passwordToTiny);

    let cfunc = function (xhttp) {
      if (xhttp.responseText === "1") {
        passwordInCorrect.hidden = true;
        showSuccessAlert("Passwort korrekt!");

        enable(newEmail);
        disable(inputPassword);

        let icon = document.getElementById("passwordLock");
        icon.classList.add("fa-lock-open");
        icon.classList.remove("fa-lock");
      } else {
        unhide(passwordInCorrect);
        disable(newEmail);
      }
    };

    xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=checkPassword&password=' + inputPassword.value);
  } else {
    unhide(passwordToTiny);
  }
}

function newEmailChange() {
  let newEmail = document.getElementById("inputNewEmail");
  let sendEmailVerificationButton = document.getElementById("sendEmailVerificationCodeButton");
  let emailNotAnEmail = document.getElementById("emailNotAnEmail");

  let cfunc = function (xhttp) {
    if (xhttp.responseText === "1") {
      hide(emailNotAnEmail);
      unhide(sendEmailVerificationButton);

    } else {
      unhide(emailNotAnEmail);
      enable(newEmail);
      hide(sendEmailVerificationButton);
    }
  };

  xhr_post('../../logic/validationHandler.php', cfunc, 'action=isEmail&email=' + newEmail.value);
}

function sendEmailVerificationCode() {
  let newEmail = document.getElementById("inputNewEmail");
  let sendEmailVerificationButton = document.getElementById("sendEmailVerificationCodeButton");
  let emailChangeCode = document.getElementById("inputEmailChangeCode");

  let cfunc = function (xhttp) {
    if (xhttp.responseText === "successful") {
      disable(newEmail);
      hide(sendEmailVerificationButton);
      enable(emailChangeCode);

      showSuccessAlert("Email wurde versendet!");

    } else {
      enable(newEmail);
      unhide(sendEmailVerificationButton);
      disable(emailChangeCode);

      showErrorAlert("Email konnte nicht versendet werden!");
    }
  };

  xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=sendEmailChangeEmail');
}

function emailCodeChange() {
  let code = document.getElementById("inputEmailChangeCode");
  let changeEmailButton = document.getElementById("changeEmailButton");
  let codeIncorrect = document.getElementById("codeIncorrect");

  if (code.value > 99999 && code.value < 1000000) {
    let cfunc = function (xhttp) {

      if (xhttp.responseText === "true") {
        showSuccessAlert("Code korrekt!");
        hide(codeIncorrect);

        unhide(changeEmailButton);
      } else {
        unhide(codeIncorrect);
        hide(changeEmailButton);
      }
    };

    xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=checkCode&code=' + code.value);


  } else {
    hide(changeEmailButton);

    showErrorAlert("Der Code muss eine Ziffer zwischen 99999 und 1000000 sein.")
  }
}

function changeEmail() {
  let password = document.getElementById("inputEmailChangePassword");
  let email = document.getElementById("inputNewEmail");
  let code = document.getElementById("inputEmailChangeCode");

  let changeEmailButton = document.getElementById("changeEmailButton");

  let codeIncorrect = document.getElementById("codeIncorrect");

  let icon = document.getElementById("passwordLock");

  let cfunc = function (xhttp) {

    switch (xhttp.responseText) {
      case 'successful':
        showSuccessAlert("Email-Adresse erfolgreich auf <b>" + email.value + "</b> geändert!");

        hide(changeEmailButton);

        disable(email);
        disable(code);
        enable(password);

        resetInput(email);
        resetInput(code);
        resetInput(password);

        icon.classList.remove("fa-lock-open");
        icon.classList.add("fa-lock");

        break;

      case 'passwordIncorrect':
        showErrorAlert("Dein Passwort ist leider inkorrekt!");
        shake(password);

        disable(email);
        disable(code);
        enable(password);

        icon.classList.remove("fa-lock-open");
        icon.classList.add("fa-lock");

        break;

      case 'invalidEmail':
        showErrorAlert("Deine Email-Addresse ist leider invalide!");
        shake(email);

        enable(email);
        disable(code);
        disable(password);

        resetInput(email);

        break;

      case 'verificationCodeIncorrect':
        showErrorAlert("Dein Verifizierungscode ist leider invalide");
        shake(code);

        disable(email);
        enable(code);
        disable(password);

        resetInput(code);

        break;

      default:
        showErrorAlert("Irgendwas ist schiefgegangen");

        break;
    }

    hide(codeIncorrect);

  };

  xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=changeEmailByHimself&password=' + password.value
    + '&email=' + email.value + '&code=' + code.value);
}

function telephoneNumberChange() {
  let number = document.getElementById("inputTelephoneNumber");
  let label = document.getElementById("inputLabel");

  let addTelephoneNumberButton = document.getElementById("addTelephoneNumberButton");

  let telephoneNumberInvalid = document.getElementById("telephoneNumberInvalid");
  let labelIncorrect = document.getElementById("labelIncorrect");
  let telephoneNumberAlreadyExists = document.getElementById("telephoneNumberAlreadyExists");

  let cfunc = function (xhttp) {
    if(xhttp.responseText === "1") {
      hide(labelIncorrect);

      let cfunc_ = function (xhttp_) {
        if(xhttp_.responseText === "1") {
          hide(telephoneNumberInvalid);

          let cfunc__ = function (xhttp__) {
            if(xhttp__.responseText === "1") {
              unhide(telephoneNumberAlreadyExists);
              hide(addTelephoneNumberButton);
            } else {
              hide(telephoneNumberAlreadyExists);
              unhide(addTelephoneNumberButton);
            }
          };

          xhr_post('../../logic/userHandler/profile_settings.php', cfunc__, 'action=telephoneNumberAlreadyExsits' +
            '&telephoneNumber=' + number.value);

        } else {
          unhide(telephoneNumberInvalid);
          hide(telephoneNumberAlreadyExists);
          hide(addTelephoneNumberButton);
        }
      };

      xhr_post('../../logic/validationHandler.php', cfunc_, 'action=isTelephoneNumber&telephonenumber=' + number.value);

    } else  {
      unhide(labelIncorrect);
      hide(addTelephoneNumberButton);
    }
  };

  xhr_post('../../logic/validationHandler.php', cfunc, 'action=isValidLabel&label=' + label.value);
}

function addTelephoneNumber() {
  let number = document.getElementById("inputTelephoneNumber");
  let label = document.getElementById("inputLabel");

  let addTelephoneNumberButton = document.getElementById("addTelephoneNumberButton");

  let cfunc = function (xhttp) {
    hide(addTelephoneNumberButton);

    switch (xhttp.responseText) {
      case 'telephoneNumberIsNotANumber':
        showErrorAlert("Deine Telefonnummer ist leider invalide!");
        shake(number);

        resetInput(number);

        break;
      case 'labelIsNotValid':
        showErrorAlert("Dein Label ist leider invalide!");
        shake(label);

        resetInput(label);

        break;
      case 'telephoneNumberAlreadyExsits':
        showErrorAlert("Die Telefonnummer ist schon eingetragen!");
        shake(number);

        resetInput(number);

        break;
      case 'successful':
        showSuccessAlert("<strong>" + number.value + "</strong> wurde erfolgreich gespeichert!");

        load_main_content_and_change_active('./sites/user/settings/manageTelephoneNumber.php', 'manageTelephoneNumber');

        break;

      default:
        showErrorAlert("Unknown Response: " + xhttp.responseText);
        break;
    }
  };

  xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=addTelephoneNumber&label=' + label.value + '&telephoneNumber=' + number.value);
}

function removeTelephoneNumber(telephoneNumber) {
  let cfunc = function (xhttp) {
    switch (xhttp.responseText) {
      case 'telephoneNumberDoesNotExist':
        showErrorAlert("Diese Telefonnummer exestiert nicht!");
        load_main_content_and_change_active('./sites/user/settings/manageTelephoneNumber.php', 'manageTelephoneNumber');

        break;
      case 'successful':
        showSuccessAlert("<strong>" + telephoneNumber + "</strong> wurde erfolgreich gelöscht!");
        load_main_content_and_change_active('./sites/user/settings/manageTelephoneNumber.php', 'manageTelephoneNumber');

        break;

      default:
        showErrorAlert("Unknown Response: " + xhttp.responseText);
        break;
    }
  };

  xhr_post('../../logic/userHandler/profile_settings.php', cfunc, 'action=removeTelephoneNumber&telephoneNumber=' + telephoneNumber);
}

/**
 * removes a token from the profile settings
 * @param token string token which should be removed
 */
function removeToken(token) {
  xhr_post('../../../logic/userHandler/tokenHandler.php', function (xhttp) {
    showSuccessAlert("Das Gerät wurde erfolgreich abgemeldet!");
    load_main_content_and_change_active('./sites/user/settings/manageTokens.php', 'manageTokens');
    clearTooltips();
  }, 'action=deleteToken&token=' + token);
}