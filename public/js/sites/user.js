/**
 * Loads the modal to add a user
 */
function addUser() {
  load_modal('sites/user/add.php', 'modalContainer');
}

/**
 * submits the add dialog
 * @param button    the submit-button of the modal
 */
function addUserSubmit(button) {
  var form = button.parentElement.getElementsByTagName('input');
  var email = form[0].value;
  var birthdate = form[1].value;
  var title = form[2].value;
  var firstname = form[3].value;
  var surname = form[4].value;
  var streetname = form[5].value;
  var streetnumber = form[6].value;
  var zip_code = form[7].value;
  var location = form[8].value;
  var joindate = form[9].value;
  var action = form[10].value;

  var cfunc = function (xhttp) {
    //console.log(xhttp.responseText);
    if (xhttp.responseText == 'true')
      load_page("sites/user/admin.php?successful=add");
    else
      load_modal("sites/user/add.php?error=true", 'modalContainer');
  };

  xhr_post('../logic/userHandler.php', cfunc, 'action=' + action +
    '&email=' + email +
    '&birthdate=' + birthdate +
    '&title=' + title +
    '&firstname=' + firstname +
    '&surname=' + surname +
    '&streetname=' + streetname +
    '&streetnumber=' + streetnumber +
    '&zip_code=' + zip_code +
    '&location=' + location +
    '&joindate=' + joindate
  );
}

/**
 * loads the modal for editing a user
 * @param userID    userID of the user to edit
 */
function editUser(userID) {
  load_modal("sites/user/settings/edit.php?userID=" + userID, 'modalContainer');
}

/**
 * submits the edit dialog
 * @param button    the submit-button of the modal
 */
function editUserSubmit(button) {
  var form = button.parentElement.getElementsByTagName('input');
  var email = form[0].value;
  var birthdate = form[1].value;
  var title = form[2].value;
  var firstname = form[3].value;
  var surname = form[4].value;
  var streetname = form[5].value;
  var streetnumber = form[6].value;
  var zip_code = form[7].value;
  var location = form[8].value;
  var joindate = form[9].value;
  var isActive = form[10];
  var hasBeenActivated = form[11];
  var userID = form[12].value;
  var action = form[13].value;
  var userNotes = $('#userNotes').val();
  var adminNotes = $('#adminNotes').val();

  var cfunc = function (xhttp) {
    if (xhttp.responseText == 'true')
      load_page("sites/user/admin.php?successful=edit");
    else
      load_modal("sites/user/edit.php?error=true", 'modalContainer');
  };

  xhr_post('../logic/userHandler.php', cfunc, 'action=' + action +
    '&email=' + email +
    '&birthdate=' + birthdate +
    '&title=' + title +
    '&firstname=' + firstname +
    '&surname=' + surname +
    '&streetname=' + streetname +
    '&streetnumber=' + streetnumber +
    '&zip_code=' + zip_code +
    '&location=' + location +
    '&active=' + (isActive.checked ? 'true' : 'false') +
    '&hasBeenActivated=' + (hasBeenActivated.checked ? 'true' : 'false') +
    '&userID=' + userID +
    '&joindate=' + joindate +
    '&userNotes=' + userNotes +
    '&adminNotes=' + adminNotes
  );
}

/**
 * deletes the given user
 * @param userID    userID of the user to delete
 */
function deleteUser(userID) {
  xhr_post('../logic/userHandler.php', function (xhttp) {
    if (xhttp.responseText == 'true')
      load_page("sites/user/admin.php?successful=remove");
    else
      load_page("sites/user/admin.php?error=remove");
  }, 'action=removeUser&userID=' + userID);
}

/**
 * gets the permissions of the given user and loads it into the container with the id 'permissionDetail' and enables the container (display: block)
 * @param userID    the userID of the user to get the permissions of
 */
function getUserPermissions(userID) {
  xhr_post('../sites/permission/detail.php', function (xhttp) {
    var detail = document.getElementById('permissionDetail');
    detail.innerHTML = xhttp.responseText;
    detail.parentElement.parentElement.style.display = 'block';
  }, 'permissionOf=' + userID);
}

/**
 * gets the permissions of the given user and loads it into the container with the id 'permissionDetail' and enables the container (display: block)
 * @param userID        the userID of the user to get the permissions of
 * @param permission    the permissions to get like 'isAdmin,canManageUsers'
 */
function getUserPermission(userID, permission) {
  xhr_post('../sites/permission/detail.php', function (xhttp) {
    var detail = document.getElementById('permissionDetail');
    detail.innerHTML = xhttp.responseText;
    detail.parentElement.parentElement.style.display = 'block';
  }, 'permissionOf=' + userID +
    '&permission=' + permission);
}

/**
 * gives the given user the given permission (like specified in bool) and reloads the container afterwards
 * notice: this loads the page with all permissions listed
 * @param key       the key of the permission as string
 * @param userID    the userId of the user to give the permission to
 * @param boolean   'true' or 'false' whether the user should have the permission or not
 */
function setPermission(key, userID, boolean) {
  xhr_post('../logic/userHandler.php',
    function (xhttp) {
      if (xhttp.responseText == 'true')
        getUserPermissions(userID);
      else {
        console.error('Failed!' + xhttp.responseText);
        getUserPermissions(userID);
      }
    },
    'action=setPermission' +
    '&userID=' + userID +
    '&key=' + key +
    '&bool=' + boolean);

}

/**
 * gives the given user the given permission (like specified in bool) and reloads the container afterwards
 * notice: this loads the page with the permissions specified in param 'permission'
 * @param key           the key of the permission as string
 * @param userID        the userId of the user to give the permission to
 * @param boolean       'true' or 'false' whether the user should have the permission or not
 * @param permission    the permissions to list after the action, permissions like 'isAdmin,canManageUsers'
 */
function setPermissions(key, userID, boolean, permission) {
  xhr_post('../logic/userHandler.php',
    function (xhttp) {
      if (xhttp.responseText == 'true')
        getUserPermission(userID, permission);
      else {
        console.error('Failed!' + xhttp.responseText);
        getUserPermission(userID, permission);
      }
    },
    'action=setPermission' +
    '&userID=' + userID +
    '&key=' + key +
    '&bool=' + boolean +
    '&permission=' + permission);

}

/**
 * Loads the modal to add a manager
 * @param permission    the permissions to add as string like 'canManageUsers'
 */
function addManager(permission) {
  load_modal('sites/permission/assign.php?permission=' + permission, 'modalContainer');
}

function showProfileOf(userID){
  load_modal('../../sites/user/publicProfile.php?userID=' + userID, 'modalContainer');
}