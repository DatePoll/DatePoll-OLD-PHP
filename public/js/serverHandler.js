/**
 * Loads the given page into the main-area of the page
 * @param url   url of the page as string (current directory: /js)
 */
function load_page(url) {
  var cfunc = function (xhttp) {
    document.getElementsByClassName("container-fluid")[0].innerHTML = xhttp.responseText;
    $('[data-toggle="tooltip"]').tooltip();
  };
  request(url, "", cfunc);
}

function load_page_with_sideMenu_and_main_content(page, menu, main_content, active = null) {
  let cfunc = function (xhttp) {
    document.getElementsByClassName("container-fluid")[0].innerHTML = xhttp.responseText;
    $('[data-toggle="tooltip"]').tooltip();

    load_main_content(main_content);
    load_sideMenu(menu, active);
  };
  request(page, "", cfunc);
}

function load_main_content_and_change_active(main_content, active) {
  load_main_content(main_content);

  refreshSideMenu(active);
}

function load_sideMenu(url, active = null) {
  var cfunc = function (xhttp) {
    document.getElementById("sideMenu").innerHTML = xhttp.responseText;
    $('[data-toggle="tooltip"]').tooltip();

    if (active != null) {
      refreshSideMenu(active);
    }
  };
  request(url, "", cfunc);
}

function refreshSideMenu(active) {
  let activeItem = null;
  let listGroupItems = document.getElementsByClassName("sideMenuItem");

  for (let i = 0; i < listGroupItems.length; i++) {
    if (listGroupItems[i].getAttribute("data-name") === active) {
      activeItem = listGroupItems[i];
    }

    listGroupItems[i].classList.remove("active");
  }

  if (activeItem != null)
    activeItem.classList.add("active");

}

function load_main_content(url) {
  var cfunc = function (xhttp) {
    document.getElementById("main_content").innerHTML = xhttp.responseText;
    $('[data-toggle="tooltip"]').tooltip();
  };
  request(url, "", cfunc);
}

/**
 * Loads a modal page into the given container and activates ist
 * Notice:  the modal loaded has to have the id='DatePollModal', otherwise you have to enable it yourself!
 * @param url   as string to load (current directory: root-directory of the page)
 * @param modal id of the container to load into
 * @param toExecuteOnOpen a function which is executed when modal opens
 * @param toExecuteAfterModalClose a function which is executed on modal close
 */
function load_modal(url, modal, toExecuteOnOpen = null, toExecuteAfterModalClose = null) {
  let cfunc = function (xhttp) {
    document.getElementById(modal).innerHTML = xhttp.responseText;

    $('#DatePollModal').modal('show'); //show the modal after loading it (has to be a #DatePollModal)

    if(toExecuteAfterModalClose != null) {
      $('#DatePollModal').on('hide.bs.modal', toExecuteAfterModalClose);
    }

    $('[data-toggle="tooltip"]').tooltip();

    if(toExecuteOnOpen != null) {
      setTimeout(toExecuteOnOpen, 150);
    }
  };
  request("../" + url, "", cfunc);
}

/**
 * performs an XML-HTTP-Request with the given parameters
 * @param url       url to call as string
 * @param params    parameters URL-Encoded like ?param=value&param2=value2...
 * @param callback  function(xhttp) to call after request
 */
function request(url, params, callback) {
  xhr(url + params, callback);
}

/**
 * performs an XML-HTTP-POST-Request with the given parameters
 * @param url       url to call as string
 * @param params    parameters URL-Encoded like param=value&param2=value2...
 * @param callback  function(xhttp) to call after request
 */
function request_post(url, params, callback) {
  xhr_post(url, callback, params);
}

/**
 * performs an XML-HTTP-Request
 * @param url       url to call as string
 * @param cfunc     function(xhttp) to call after request
 */
function xhr(url, cfunc) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      cfunc(xhttp);
    }
  };
  xhttp.open("GET", url, true);
  xhttp.send();
}

/**
 * performs an XML-HTTP-POST-Request with the given parameters
 * @param url       url to call as string
 * @param cfunc     function(xhttp) to call after request
 * @param param     parameters as string like param=value&param2=value2...
 */
function xhr_post(url, cfunc, param) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
      cfunc(xhttp);
    }
  };
  xhttp.open("POST", url, true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(param);
}

function optionalDropdown(element) {
  if (element.childElementCount == 0)
    element.parentElement.hidden = true;
  else
    element.parentElement.hidden = false;
}

//optionalDropdown(document.getElementById('navbarManagementSupport'));

/**
 * hides the container specified (display: none)
 * @param detail    the id of the container to hide
 */
function hideDetails(detail) {
  document.getElementById(detail).parentElement.parentElement.style.display = 'none';
}

/**
 * Saves the server-settings
 */
function saveSettings() {
  var form = document.getElementsByTagName('input');

  var settings = {
    "dbHost": $('#settingsDBHost').val(),
    "dbName": $('#settingsDBName').val(),
    "dbUser": $('#settingsDBUser').val(),
    "dbPassword": $('#settingsDBPass').val(),
    "mailHost": $('#settingsMailHost').val(),
    "mailUser": $('#settingsMailUser').val(),
    "mailPassword": $('#settingsMailPass').val(),
    "mailPort": Number($('#settingsMailPort').val()),
    "mailEncryption": $('#settingsMailEncryption').val(),
    "mailContactAddress": $('#settingsMailContact').val(),
    "userDefaultPassword": $('#settingsUserPW').val()
  };

  var cfunc = function (xhttp) {
    if (xhttp.responseText == 'true') {
      console.log("success!");
    } else
      console.error(xhttp.responseText);
  };

  xhr_post("logic/settingsHandler.php", cfunc, "json=" + JSON.stringify(settings, null, "\t") + "&action=save");

}

function showErrorAlert(text) {
  let alert = document.getElementById("errorAlert");
  alert.hidden = false;
  let alterText = document.getElementById("errorAlertText");
  alterText.innerHTML = text;

  $('#errorAlertCollapse').collapse('show');

  setTimeout(function() {
    $("#errorAlertCollapse").collapse('hide');
  }, 5000);
}

function showSuccessAlert(text) {
  let alter = document.getElementById("successAlert");
  alter.hidden = false;
  let alertText = document.getElementById("successAlertText");
  alertText.innerHTML = text;

  $('#successAlertCollapse').collapse('show');

  setTimeout(function() {
    $("#successAlertCollapse").collapse('hide');
  }, 5000);
}

function showModalErrorAlert(text) {
  let alert = document.getElementById("modalErrorAlert");
  alert.hidden = false;
  let alterText = document.getElementById("modalErrorAlertText");
  alterText.innerHTML = text;

  $('#modalErrorAlertCollapse').collapse('show');

  setTimeout(function() {
    $("#modalErrorAlertCollapse").collapse('hide');
  }, 5000);
}

function showModalSuccessAlert(text) {
  let alter = document.getElementById("modalSuccessAlert");
  alter.hidden = false;
  let alertText = document.getElementById("modalSuccessAlertText");
  alertText.innerHTML = text;

  $('#modalSuccessAlertCollapse').collapse('show');

  setTimeout(function() {
    $('#modalSuccessAlertCollapse').collapse('hide');
  }, 5000);
}

function hide(element) {
  element.hidden = true;
}

function unhide(element) {
  element.hidden = false;
}

function disable(element) {
  element.disabled = true;
}

function enable(element) {
  element.disabled = false;
}

function resetInput(input) {
  input.value = "";
}

function clearTooltips() {
  $('.tooltip').remove();
}

function loadDependencies() {
  $('[data-toggle="tooltip"]').tooltip();
}