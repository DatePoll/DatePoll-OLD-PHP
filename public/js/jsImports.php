<?php

function getScripts() {
	$scripts = '
<!--Scripts needed for Bootstrap-->
<script type="application/javascript" src="node_modules/jquery/dist/jquery.slim.min.js"></script>
<script type="application/javascript" src="node_modules/popper.js/dist/umd/popper.min.js"></script>
<script type="application/javascript" src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="application/javascript" src="node_modules/navigo/lib/navigo.min.js"></script>

<!--Script for FontAwesome-->
<script type="application/javascript" src="js/fontawesome-all.js"></script>


<!--Basic JS for DatePoll-->
<script type="application/javascript" src="js/serverHandler.js"></script>
<script type="application/javascript" src="js/animate.js"></script>
<script type="application/javascript" src="js/route.js"></script>
<script type="application/javascript" src="js/sites/organisation.js"></script>
<script type="application/javascript" src="js/sites/group.js"></script>
<script type="application/javascript" src="js/sites/poll.js"></script>
<script type="application/javascript" src="js/sites/profileSettings.js"></script>
<script type="application/javascript" src="js/sites/user.js"></script>
<script type="application/javascript" src="js/sites/administration/poll.js"></script>


<!--Additions-->
<script type="application/javascript">
    
    function checkValue() {
			var max = 9999;
			var min = 1000;
	
			var element = document.getElementById("inputZipcode");
			if (element.value > max) {
				element.value = max;
			}
			if (element.value < min) {
				element.value = min;
			}
    }

    $(document).ready(function () {
        $(\'[data-toggle="tooltip"]\').tooltip()
        $(\'.alert\').alert();
    });

</script>
	';

	echo $scripts;
}

?>