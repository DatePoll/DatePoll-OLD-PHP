var root = '/#!';
var useHash = false; // Defaults to: false
var hash = '#!'; // Defaults to: '#'
var router = new Navigo(root, useHash, hash);

var admin = function (param) {

  switch (param.page) {
    case 'poll':
      load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/administration/menu.php', 'sites/administration/poll.php', 'poll');

      break;
    case 'user':
      load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/administration/menu.php', 'sites/user/admin.php', 'user');

      break;
    case 'group':
      load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/administration/menu.php', 'sites/group/admin.php', 'group');

      break;
    case 'organisation':
      load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/administration/menu.php', 'sites/basic/organisation.php', 'organisation');

      break;
    case 'permission':
      load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/administration/menu.php', 'sites/permission/admin.php', 'permission');

      break;
    case 'settings':
      load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/administration/menu.php', 'sites/basic/settings.php', 'settings');

      break;
    default:
      load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/administration/menu.php', 'sites/administration/poll.php', 'poll');

      break;
  }
};

var profile = function (param) {

  switch (param.action) {
    case 'edit':
      switch (param.page) {
        case 'data':
          load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/user/settings/menu.php', 'sites/user/settings/changeUserdata.php', 'changeUserdata');

          break;
        case 'password':
          load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/user/settings/menu.php', 'sites/user/settings/changePassword.php', 'changePassword');

          break;
        case 'email':
          load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/user/settings/menu.php', 'sites/user/settings/changeEmail.php', 'changeEmail');

          break;
        case 'telephonenumber':
          load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/user/settings/menu.php', 'sites/user/settings/manageTelephoneNumber.php', 'manageTelephoneNumber');

          break;
        case 'tokens':
          load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/user/settings/menu.php', 'sites/user/settings/manageTokens.php', 'manageTokens');

          break;
        case 'other':
          load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/user/settings/menu.php', 'sites/user/settings/changeOther.php', 'changeOther');

          break;
        default:
          load_page_with_sideMenu_and_main_content('sites/main.php', 'sites/user/settings/menu.php', 'sites/user/settings/changeUserdata.php', 'changeUserdata');

          break;
      }
  }
};

var auth = function (param) {

  switch (param.action) {
    case 'logout':
      window.location.href = "startpage.php?do=logout";
      break;
  }
};


router
  .on({
    'home': function () {
      load_page('sites/basic/home.php');
    },
    'poll/vote': function () {
      load_page('sites/poll/vote/vote.php');
    },
    'group': function () {
      load_page('sites/group/user/listGroups.php');
    },
    'organisation': function () {
      load_page('sites/basic/organisationPage.php');
    },
    'admin/:page': admin,
    'profile/:action/:page': profile,
    'auth/:action': auth,
    '*': function () {
      router.navigate('/home');
    }
  })
  .resolve();

router.notFound(function (query) {
  load_page('404.html');
});