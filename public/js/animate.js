function shake(div) {
  div.classList.add("animated");
  div.classList.add("shake");

  setTimeout(function () {
    div.classList.remove("animated");
    div.classList.remove("shake");
  }, 5000);
}

function bounceIn(div) {
  div.classList.add("animated");
  div.classList.add("bounceIn");

  setTimeout(function () {
    div.classList.remove("animated");
    div.classList.remove("bounceIn");
  }, 1000);
}

function bounceOut(div) {
  div.classList.add("animated");
  div.classList.add("bounceOut");

  setTimeout(function () {
    div.classList.remove("animated");
    div.classList.remove("bounceOut");
  }, 1000);

  //Wait until the animation is finished
  setTimeout(function () {
    hide(div);
  }, 1000);
}

function slideInDown(div) {
  div.classList.add("animated");
  div.classList.add("slideInDown");

  setTimeout(function () {
    div.classList.remove("animated");
    div.classList.remove("slideInDown");
  }, 1000);
}

function slideOutUp(div) {
  div.classList.add("animated");
  div.classList.add("slideOutUp");

  setTimeout(function () {
    div.classList.remove("animated");
    div.classList.remove("slideOutUp");
  }, 1000);

  //Wait until the animation is finished
  setTimeout(function () {
    hide(div);
  }, 1000);
}

function slideIn(div) {
  div.classList.add("animated");
  div.classList.add("slideInDown");

  setTimeout(function () {
    div.classList.remove("animated");
    div.classList.remove("slideInDown");
  }, 2000);
}