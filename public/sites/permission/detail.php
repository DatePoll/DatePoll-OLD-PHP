<?php
require_once('../../logic/userHandler.php');
require_once('../../logic/parser.php');
if(isset($_REQUEST['permissionOf'])){
	$user = getPermissions($_REQUEST['permissionOf']);
	$table = parseTableHead(parseTableData('Recht') . parseTableData('Status'));

	$keymode = '';
	if(isset($_REQUEST['permission']))
		$keymode = ', \''.$_REQUEST['permission'].'\'';

	foreach ($user as $key => $permission){
		if($key == 'userID')
			continue;

		if(isset($_REQUEST['permission']) and !in_array($key, explode(",", $_REQUEST['permission'])))
			continue;

		$table .= parseTableRow(parseTableData($key) .
				parseTableData(
						parseTag('input', '', "type='checkbox' name='$key' ".
								(boolval($permission) ? 'checked' : '') .
								' onchange="setPermission'.($keymode != '' ? 's' : '').'(\''. $key .'\', '.$_REQUEST['permissionOf'].', this.checked'.$keymode.')"'
						)
				)
		);
	}
	$table = parseTable($table, 'class="table table-bordered table-hover"');

	echo $table;
}
?>
