<?php
if (!isset($_REQUEST['groupID'])) {
	echo false;
	die();
}
require_once('../../logic/groupHandler.php');
require_once('../../logic/parser.php');

$form = "
<form>
	<div class='form-group'>
		<label for='subgroupName'>Name: </label>
		<input type='text' class='form-control' name='subgroupName' placeholder='Name der Gruppe' id='subgroupName' required>
	</div>
		<div class='form-group'>
		<label for='subgroupDescription'>Beschreibung: </label>
		<input type='text' class='form-control' name='subgroupDescription' placeholder='Beschreibung der Gruppe' id='subgroupDescription'>
	</div>
	<button type='button' class='btn btn-primary' onclick='addSubgroupSubmit(this)' data-dismiss='modal'>Gruppe erstellen</button>
	<button type='button' class='btn btn-secondary' data-dismiss='modal'>Abbrechen</button>
	
	<input type='hidden' id='action' value='addSubgroup'>
	<input type='hidden' id='groupID' value='".$_REQUEST['groupID']."'>
</form>";

echo parseModal('Neue Gruppe für Abteilung ' . $_REQUEST['groupID'] . ' erstellen', $form, 'addSubgroup');