<?php
require_once('../../logic/userHandler.php');
require_once('../../logic/groupHandler.php');
require_once('../../logic/parser.php');

if (!canAccessBasicPage()) {
	echo false;
	die();
}
if (!getPermissions()['canManageGroups'] == 1 and !getPermissions()['isAdmin'] == 1) {
	return false;
	die();
}
?>
	<input id="contentTitle" type="hidden" data-title="edit/groups"/>

<h1 class="card-title" style="text-align: center;"><i class="fas fa-users"></i> Abteilungen & Gruppen</h1>
<div class="card-deck">

	<div class="col-sm-12 col-md-6" style="max-height: 100%;">
		<div class="card">
			<div class="card-header" style="text-align: center;">
				Abteilungen
				<button class="btn btn-outline-success float-right" onclick="addGroup()">
					<i class="fas fa-plus"></i>
				</button>
			</div>
			<div class="card-body text-center">
				<?php
				$table = parseTableHead(parseTableData('groupID') . parseTableData('Name') . parseTableData('Beschreibung') . parseTableData(''));
				$groups = getGroups();
				foreach ($groups as $group) {
					$btnEdit = parseTag("button", parseTag("i", "", "class='fas fa-edit'"),
							"class = 'btn btn-primary' onclick='editGroup(" . $group['groupID'] . ")'");
					$btnDelete = parseTag("button", parseTag("i", "", "class='fas fa-trash'"),
							"class = 'btn btn-danger' onclick='deleteGroup(" . $group['groupID'] . ")'");
					$detail = 'onclick="getGroupDetails(' . $group['groupID'] . ')"';
					$row = "";

					$row .= parseTableData($group['groupID'], $detail);
					$row .= parseTableData($group['name'], $detail);
					$row .= parseTableData($group['description'], $detail);
					$row .= parseTableData( $btnEdit . $btnDelete);

					$row = parseTableRow($row);
					$table .= $row;
				}
				$table = parseTable($table, 'class="table table-bordered table-hover"');

				echo $table;
				?>
			</div>
		</div>
	</div>

	<div class="col-md-6" style="display: none;">
		<div class="card">
			<div class="card-header" style="text-align: center;">
				Details
				<button class="btn btn-outline-warning float-right" onclick="hideDetails('groupDetail')">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="card-body text-center" id="groupDetail">
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="card">
			<div class="card-header" style="text-align: center;">
				Gruppen-Verwalter
				<button class="btn btn-outline-success float-right" onclick="addManager('canManageGroups')">
					<i class="fas fa-plus"></i>
				</button>
			</div>
			<div class="card-body text-center">
				<?php
				$table = parseTableHead(parseTableData('Name') . parseTableData('Email-Adresse'));
				$users = getGroupManagers();
				foreach ($users as $user) {
					$detail = 'onclick="getUserPermission(' . $user['userID'] . ', \'isAdmin,canManageGroups\')"';
					$row = "";

					$row .= parseTableData($user['name'], $detail);
					$row .= parseTableData($user['email'], $detail);

					$row = parseTableRow($row);
					$table .= $row;
				}
				$table = parseTable($table, 'class="table table-bordered table-hover"');

				echo $table;
				?>
			</div>
		</div>
	</div>

	<div class="col-md-6 col-md-6" style="max-height: 100%; display: none;">
		<div class="card">
			<div class="card-header text-center">
				Rechte
				<button class="btn btn-outline-warning float-right" onclick="hideDetails('permissionDetail')">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="card-body text-center" id="permissionDetail"></div>
		</div>
	</div>

	<div id="modalContainer"></div>
</div>
<?php
if (isset($_GET['successful'])) {
	switch ($_GET['successful']) {
		case 'add':
			echo parseAlert('<strong>Anlegen erfolgreich!</strong>', 'success', true);
			break;
		case 'edit':
			echo parseAlert('<strong>Bearbeiten erfolgreich!</strong>', 'success', true);
			break;
		case 'remove':
			echo parseAlert("<strong>Löschen erfolgreich!</strong>", 'success', true);
			break;
		case 'moveUser':
			echo parseAlert("<strong>Benutzer verschieben erfolgreich!</strong>", 'success', true);
			break;
	}
}
if (isset($_GET['error'])) {
	switch ($_GET['error']) {
		case 'remove':
			echo parseAlert("<strong>Löschen fehlgeschlagen!</strong>", 'danger', true);
			break;
	}
}
?>