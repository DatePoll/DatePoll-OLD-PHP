<?php
if(!isset($_GET['groupID'])){
	echo false;
	die();
}
require_once ('../../logic/groupHandler.php');
	$group = getGroups($_REQUEST['groupID']);

?>
<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="ModalTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="Modal">Gruppe <?php echo $_GET['groupID']; ?> bearbeiten</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<?php
				if (isset($_REQUEST['$error']))
					echo '
							<li class="list-group-item">
	              <div class="alert alert-danger alert-dismissible fade show" role="alert">
	                <strong>Bearbeiten fehlgeschlagen!</strong> Bitte überprüfe deine Eingaben nochmals!
	              </div>
	            </li>';
				?>

				<form method="post" action="#">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" id="name" name="name" aria-describedby="titleHelp"
						       placeholder="Name der Gruppe eingeben" value="<?php echo $group['name']; ?>" required>
					</div>

					<div class="form-group">
						<label for="description">Beschreibung</label>
						<input type="text" class="form-control" id="description" name="description" aria-describedby="titleHelp"
						       placeholder="Beschreibung der Gruppe eingeben (optional)" value="<?php echo (isset($group['description']) ? $group['description'] : ''); ?>">
					</div>

					<button type="button" class="btn btn-primary" onclick="editGroupSubmit(this)" data-dismiss="modal">Änderungen speichern</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>

					<input type="hidden" id="groupID" value="<?php echo $_GET['groupID']; ?>">
					<input type="hidden" id="action" value="editGroup">
				</form>
			</div>
		</div>
	</div>
</div>