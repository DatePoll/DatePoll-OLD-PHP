<?php
require_once('../../../logic/userHandler.php');
require_once('../../../logic/groupHandler.php');
require_once('../../../logic/parser.php');

if (!canAccessBasicPage()) {
	echo false;
	die();
}

$myGroups = getGroupsAndSubgroups();
?>
<input id="contentTitle" type="hidden" data-title="groups"/>

<h1 class="card-title" style="text-align: center;"><i class="fas fa-users"></i> Abteilungen & Gruppen anzeigen</h1>
<div class="card-deck" id="accordion">
	<?php
	$prev_grp = $myGroups[0];
	$cards = '';
	$tmp = array();
	foreach ($myGroups as $group) {
		// if the group changed parse the subgroup-cards into the group-card and put it into temp
		if ($group['groupID'] != $prev_grp['groupID']) {
			// create the group-card header
			$header = parseDiv($prev_grp['group_name'], 'p-2 text-center text-primary', 'data-toggle="collapse" data-target="#collapse' . $prev_grp['groupID'] . '"  aria-expanded="false" aria-controls="collapse' . $prev_grp['groupID'] . '"');
			// add the new group-card to the group-card-buffer
			$cards .= '<div class="col-12" style="padding: 0; margin: 0;">' . parseCard(parseCardHeader($header, '', 'id="heading' . $prev_grp['groupID'] . '"'),
							'<div id="collapse' . $prev_grp['groupID'] . '" class="collapse" aria-labelledby="heading' . $prev_grp['groupID'] . '" data-parent="#accordion">' .
							parseCardBody(parseAccordion($tmp)) .
							'</div>',
							'') . '</div>';
			$tmp = array();
			$prev_grp = $group;
		}
		// get the subgroup-card of the current group and add it to the subgroup-card-buffer
		array_push($tmp, ['id' => $group['subgroupID'], 'header' => parseDiv($group['name'], 'p-2 text-warning  w-100'), 'body' => '' . getMembersOfSubgroupAsTable($group['subgroupID'])]);
	}
	if ($tmp != '') {
		$header = parseDiv($group['group_name'], 'p-2 text-primary text-center', ' data-toggle="collapse" data-target="#collapse' . $group['groupID'] . '" aria-controls="collapse' . $group['groupID'] . '"');
		$cards .= '<div class="col-12" style="padding: 0; margin: 0;">' .
				parseCard(
						parseCardHeader(
								$header,
								'',
								'id="heading' . $group['groupID'] . '"'
						),
						'<div id="collapse' . $group['groupID'] . '" class="collapse" aria-labelledby="heading' . $group['groupID'] . '" data-parent="#accordion">' .
						parseCardBody(parseAccordion($tmp)) .
						'</div>',
						'')
				. '</div>';
	}
	echo $cards;
	?>

	<div id="modalContainer"></div>
</div>

<?php
if (isset($_GET['successful'])) {
	switch ($_GET['successful']) {
		case 'moveUser':
			echo parseAlert('<strong>Anlegen erfolgreich!</strong>', 'success', true);
			break;
	}
}

?>

<div id="modalContainer"></div>
