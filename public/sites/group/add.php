<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="groupModalTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="groupModalTitle">Gruppe anlegen</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<?php
				if (isset($_REQUEST['$error']))
					echo '
							<li class="list-group-item">
	              <div class="alert alert-danger" role="alert">
	                <strong>Anlegen fehlgeschlagen!</strong> Bitte überprüfe deine Eingaben nochmals!
	              </div>
	            </li>';
				?>

				<form method="post" action="#">
					<div class="form-group">
						<label for="name">Titel</label>
						<input type="text" class="form-control" id="name" name="name" aria-describedby="titleHelp"
						       placeholder="Titel der Gruppe eingeben" required>
					</div>

					<div class="form-group">
						<label for="description">Beschreibung</label>
						<input type="text" class="form-control" id="description" name="description" aria-describedby="titleHelp"
						       placeholder="Beschreibung der Gruppe eingeben (optional)">
					</div>

					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="addGroupSubmit(this)">Änderungen speichern</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>

					<input type="hidden" id="action" value="addGroup">
				</form>
			</div>
		</div>
	</div>
</div>