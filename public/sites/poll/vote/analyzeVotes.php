<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="pollModalTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="pollModalTitle">Abstimmung anlegen</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<?php
				if (isset($_REQUEST['$error']))
					echo '
							<li class="list-group-item">
	              <div class="alert alert-danger" role="alert">
	                <strong>Anlegen fehlgeschlagen!</strong> Bitte überprüfe deine Eingaben nochmals!
	              </div>
	            </li>';
				?>

				<form method="post" action="#">
					<div class="form-group">
						<label for="name">Titel</label>
						<input type="text" class="form-control" id="name" name="name" aria-describedby="titleHelp"
									 placeholder="Titel der Abstimmung eingeben" required>
					</div>

					<div class="form-group">
						<label for="description">Beschreibung</label>
						<input type="text" class="form-control" id="description" name="description" aria-describedby="titleHelp"
									 placeholder="Beschreibung der Abstimmung eingeben (optional)">
					</div>

					<div class="form-group">
						<label for="finishDate">Enddatum</label>
						<input type="text" class="form-control" id="finishDate" name="finishDate" aria-describedby="titleHelp"
									 placeholder="Ende der Abstimmung" required>
					</div>

					<div class="form-group">
						<label for="groupID">Abteilung</label>
						<input type="text" class="form-control" id="groupID" name="groupID" aria-describedby="titleHelp"
									 placeholder="Abteilung (optional)">
					</div>

					<div class="form-group">
						<label for="subgroup">Gruppe</label>
						<input type="text" class="form-control" id="finishDate" name="finishDate" aria-describedby="titleHelp"
									 placeholder="Gruppe (optional)">
					</div>

					<button type="button" class="btn btn-primary" onclick="addPollSubmit(this)">Änderungen speichern</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>

					<input type="hidden" id="action" value="addPoll">
				</form>
			</div>
		</div>
	</div>
</div>