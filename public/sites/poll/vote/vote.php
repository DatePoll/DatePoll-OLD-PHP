<?php
require_once('../../../logic/userHandler.php');
require_once('../../../logic/pollHandler.php');
require_once('../../../logic/parser.php');

if (!canAccessBasicPage()) {
	echo false;
	die();
}
?>
<input id="contentTitle" type="hidden" data-title="polls"/>
<h1 class="card-title" style="text-align: center;"><i class="fas fa-calendar"></i> Abstimmungen</h1>
<div class="card-deck">
	<div class="col-md-6" style="max-height: 100%;">
		<div class="card">
			<div class="card-header" style="text-align: center;">
				unbeantwortete Abstimmungen
			</div>
			<div class="card-body text-center">
				<?php
				$decisions = getYesNoDecisions();
				$cardDeck = '';
				$polls = listOpenPolls();
				if (count($polls) == 0) {
					$cardDeck = parseTag('div', parseAlert('<strong>Du hast keine unbeantworteten Abstimmungen! <br /> Weiter so!</strong>', 'success', false), 'class="col-12"');
				} else
					foreach ($polls as $poll) {
						$header = parseCardHeader(parseCardTitle($poll['title']) . parseCardSubtitle(date("d.m.Y H:i", strtotime($poll['finishDate']))), "text-center");
						$body = parseCardBody($poll['description']);
						$footer = parseCardFooter('<button class="btn btn-success" onclick="voteFor(' . $poll['pollID'] . ', ' . $decisions[1]['decisionID'] . ')">
																							' . $decisions[1]['name'] . '
																						 </button>' . '
																						 <button class="btn btn-danger" onclick="voteFor(' . $poll['pollID'] . ', ' . $decisions[0]['decisionID'] . ')">
																						  ' . $decisions[0]['name'] . '
																						 </button>');

						$cardDeck .= "<div class='col-md-6'>\n\t" . parseCard($header, $body, $footer) . "</div>";
					}
				$cardDeck = parseCardDeck($cardDeck);

				echo $cardDeck;
				?>
			</div>
		</div>
	</div>

	<div class="col-md-6" style="max-height: 100%;">
		<div class="card">
			<div class="card-header" style="text-align: center;">
				beantwortete Abstimmungen
			</div>
			<div class="card-body text-center">
				<?php
				$decisions = getYesNoDecisions();
				$buttonYes = '<button class="btn btn-success">' . $decisions[1]['name'] . '</button>';
				$buttonNo = '<button class="btn btn-danger">' . $decisions[0]['name'] . '</button>';
				$cardDeck = '';
				$polls = listPollsVotedFor();
				if (count($polls) == 0) {
					$cardDeck = parseTag('div', parseAlert('<strong>Du hast noch keine bevorstehende Abstimmung beantwortet!</strong>', 'warning', false), 'class="col-12"');
				} else
					foreach ($polls as $poll) {
						$header = parseCardHeader(parseCardTitle($poll['title']) . parseCardSubtitle(date("d.m.Y H:i", strtotime($poll['finishDate']))), "text-center");
						$body = parseCardBody($poll['description']);
						$footer = parseCardFooter('Meinung ändern:<button class="btn btn-success" onclick="updateVoteFor(' . $poll['pollID'] . ', ' . $decisions[1]['decisionID'] . ')"' . ($poll['decisionID'] == $decisions[0]['decisionID'] ? '' : 'disabled') . '>
																							' . $decisions[1]['name'] . '
																						 </button>' . '
																						 <button class="btn btn-danger" onclick="updateVoteFor(' . $poll['pollID'] . ', ' . $decisions[0]['decisionID'] . ')"' . ($poll['decisionID'] == $decisions[1]['decisionID'] ? '' : 'disabled') . '>
																						  ' . $decisions[0]['name'] . '
																						 </button>');

						$cardDeck .= "<div class='col-md-6'>\n\t" . parseCard($header, $body, $footer, ($poll['decisionID'] == $decisions[1]['decisionID'] ? 'bg-success' : ($poll['decisionID'] == $decisions[0]['decisionID'] ? 'bg-danger' : 'bg-warning'))) . "\n</div>";
					}

				$cardDeck = parseCardDeck($cardDeck);
				echo $cardDeck;
				?>
			</div>
		</div>
	</div>

	<div id="modalContainer"></div>
</div>
<?php
if (isset($_GET['successful'])) {
	switch ($_GET['successful']) {
		case 'voteFor':
			echo parseAlert('<strong>Erfolgreich abgestimmt!</strong>', 'success', true);
			break;
		case 'updateVoteFor':
			echo parseAlert('<strong>Meinung geändert!</strong>', 'success', true);
			break;
	}
}
if (isset($_GET['error'])) {
	switch ($_GET['error']) {
		case 'voteFor':
			echo parseAlert('<strong>Abstimmen fehlgeschlagen!</strong>', 'danger', true);
			break;
		case 'updateVoteFor':
			echo parseAlert('<strong>Meinung ändern fehlgeschlagen!</strong>', 'danger', true);
			break;
	}
}
?>
