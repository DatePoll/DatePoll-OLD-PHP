<?php
require_once(__DIR__ . '/../../logic/userHandler.php');

$canManagePolls = false;
if (getPermissions()['canManagePolls'] == 1 OR getPermissions()['isAdmin'] == 1) {
  $canManagePolls = true;
}

if (!isset($_REQUEST['pollID'])) {
  die();
}

require_once(__DIR__ . '/../../logic/pollHandler.php');
require_once(__DIR__ . '/../../logic/groupHandler.php');
$poll = getPolls($_REQUEST['pollID']);

?>
<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="pollModalTitle"
     aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <?php
        if ($canManagePolls) {
          echo '
            <div class="mr-auto">
            <button class="btn btn-outline-primary" id="switchInputsForPollDetailsButton" onclick="switchInputsForPollDetails()"
                data-toggle="tooltip" data-placement="right" title="Editiere diese Abstimmung">
              <i class="fas fa-edit"></i>
            </button>
            
            <button class="btn btn-success" hidden id="pollDetailSaveButton" 
              onclick="EditPollSave(' . $poll['pollID'] . ')" data-toggle="tooltip" data-placement="top" 
              title="Speichere deine Änderungen"><i class="fas fa-save"></i> Speichern</button>
            </div>
            ';
        }
        ?>
        <h5 class="modal-title" id="pollModalTitle"><?php echo $poll['title'] ?> Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <?php
          if ($canManagePolls) {
            echo parseAlert("<strong>Information!</strong> Änderung an den Gruppen und Abteilungen werden direkt gespeichert!",
              "info", false, 'width: 100%; margin-right: 15px; margin-left: 15px;');
            echo parseStandardModalAlerts();
          }
          ?>
        </div>
        <div class="row">
          <div class="col-12 col-sm-12 col-md-6 col-lg-8 col-xl-8">
            <div class="form-group">
              <label for="inputDetailTitel">Titel:</label>
              <div class="input-group" id="inputGroupDetailTitel">
                <div class="input-group-prepend">
                  <label for="inputDetailTitel" class="input-group-text"><i class="fas fa-tag"></i></label>
                </div>
                <input type="text" class="form-control detailPoll" disabled id="inputDetailTitel"
                       name="inputDetailTitel" oninput="EditPollInputChange('title')"
                       value="<?php echo $poll['title']; ?>">
                <input type="text" title="" hidden id="inputDetailTitel_old" value="<?php echo $poll['title']; ?>">
              </div>
              <div class="row" style="margin-left: 1px;">
                <small id="titleEditIncorrect" hidden class="form-text" style="color: red">
                  Der Title muss größer gleich <strong>1</strong> Zeichen und kleiner gleich <strong>64</strong> Zeichen haben.
                </small>
              </div>
            </div>
          </div>

          <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
            <div class="form-group">
              <label for="inputDetailFinishDate">Enddatum:</label>
              <div class="input-group" id="inputGroupDetailFinishDate">
                <div class="input-group-prepend">
                  <label for="inputDetailFinishDate" class="input-group-text"><i class="fas fa-calendar"></i></label>
                </div>
                <input type="date" class="form-control detailPoll" disabled id="inputDetailFinishDate"
                       name="inputDetailFinishDate" oninput="EditPollInputChange('date')"
                       value="<?php echo substr($poll['finishDate'], 0, 10); ?>">
                <input type="date" title="" hidden id="inputDetailFinishDate_old" value="<?php echo substr($poll['finishDate'], 0, 10); ?>">
              </div>
              <div class="row" style="margin-left: 1px;">
                <small id="finishDateEditIncorrect" hidden class="form-text" style="color: red">
                  Das Datum ist nicht korrekt!
                </small>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="form-group">
              <label for="inputDetailDescription">Beschreibung: </label>
              <div class="input-group" id="inputGroupDetailDescription">
                <div class="input-group-prepend">
                  <label for="inputDetailDescription" class="input-group-text"><i
                        class="fas fa-align-left"></i></label>
                </div>

                <textarea id="inputDetailDescription" class="form-control detailPoll"
                          oninput="this.style.height='24px'; this.style.height = this.scrollHeight + 12 + 'px'; EditPollInputChange('description')"
                          disabled><?php echo $poll['description']; ?></textarea>
                <input type="text" title="" hidden id="inputDetailDescription_old" value="<?php echo $poll['description']; ?>">
                <div class="row" style="margin-left: 1px;">
                  <small id="descriptionEditIncorrect" hidden class="form-text" style="color: red">
                    Die Description ist zu lang!
                  </small>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <?php
            if ($canManagePolls) {
              echo '
            <div class="row">
              <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                <button class="btn btn-success detailPoll" disabled data-toggle="tooltip" data-placement="left"
                        title="Füge eine neue Abteilung für diese Abstimmung hinzu"
                        onclick="EditPollAddGroupToPoll()" style="margin-right: 5px;">
                  <i class="fas fa-plus-circle"></i>
                </button>
              </div>
              <div class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                <select class="custom-select detailPoll" disabled id="detailGroupToPollSelect" title="detailGroupToPollS">
                  <option value="selectGroup" selected>Wähle eine Abteilung welche zur Abstimmung hinzugefügt werden soll</option>
                ';

              $groups = getUnassignedGroupsForPoll($_REQUEST['pollID']);
              foreach ($groups as $group) {
                echo '<option value="' . $group['groupID'] . '" data-name="' . $group['name'] . '" data-pollid="' . $_REQUEST['pollID'] . '">' . $group['name'] . '</option>';
              }

              echo '
                 </select>
              </div>
            </div>
                ';
            }
            ?>
            <div class="row" style="margin-left: 0.05vh; margin-top: 1vh">
              <?php
              $table = parseTableHead(parseTableData('Abteilungen'));

              if ($canManagePolls) {
                $table = parseTableHead(parseTableData('Abteilungen') . parseTableData('Löschen'));
              } else {
                $table = parseTableHead(parseTableData('Abteilungen'));
              }

              $groups = getGroupsForPoll($_REQUEST['pollID']);
              foreach ($groups as $group) {
                $groupDetail = getGroups($group['groupID']);

                $row = '';

                $row .= parseTableData($groupDetail['name']);

                if ($canManagePolls) {
                  $row .= parseTableData('
                          <button class="btn btn-danger detailPoll" disabled 
                            onclick="EditPollRemoveGroupFromPoll(' . $_REQUEST['pollID'] . ',' . $group['groupID'] . ', \'' . $groupDetail['name'] . '\', this)" data-toggle="tooltip" 
                            data-placement="right" title="Entferne diese Abteilung"><i class="fas fa-trash"></i>
                          </button>
                          ');
                }

                $row = parseTableRow($row);

                $table .= $row;
              }

              $table = parseTable($table, 'id="detailGroupTable" class="table table-bordered table-hover"');

              echo $table;
              ?>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <?php
            if ($canManagePolls) {
              echo '
            <div class="row">
              <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                <button class="btn btn-success detailPoll" disabled data-toggle="tooltip" data-placement="left"
                        title="Füge eine neue Gruppe für diese Abstimmung hinzu"
                        onclick="EditPollAddSubGroupToPoll()" style="margin-right: 5px;">
                  <i class="fas fa-plus-circle"></i>
                </button>
              </div>
              <div class="col-10 col-sm-10 col-md-10 col-lg-10 col-xl-10">
                <select class="custom-select detailPoll" disabled id="detailSubGroupToPollSelect" title="detailSubGroupToPollS">
                  <option value="selectSubGroup" selected>Wähle eine Gruppe welche zur Abstimmung hinzugefügt werden soll</option>
              ';

              $subgroups = getUnassignedSubGroupsForPoll($_REQUEST['pollID']);
              foreach ($subgroups as $subgroup) {
                echo '<option value="' . $subgroup['subgroupID'] . '" data-name="' . $subgroup['name'] . '" data-pollid="' . $_REQUEST['pollID'] . '">' . $subgroup['name'] . '</option>';
              }

              echo '
               </select>
              </div>
            </div>
              ';
            }
            ?>

            <div class="row" style="margin-left: 0.05vh; margin-top: 1vh">
              <?php
              if ($canManagePolls) {
                $table = parseTableHead(parseTableData('Gruppen') . parseTableData('Löschen'));
              } else {
                $table = parseTableHead(parseTableData('Gruppen'));
              }

              $subgroups = getSubGroupForPoll($_REQUEST['pollID']);
              foreach ($subgroups as $subgroup) {
                $subgroupDetail = getSubGroups($subgroup['subgroupID']);

                $row = '';

                $row .= parseTableData($subgroupDetail['name']);

                if ($canManagePolls) {
                  $row .= parseTableData('
                          <button class="btn btn-danger detailPoll" disabled 
                            onclick="EditPollRemoveSubGroupFromPoll(' . $_REQUEST['pollID'] . ',' . $subgroup['subgroupID'] . ', \'' . $subgroupDetail['name'] . '\', this)" data-toggle="tooltip" 
                            data-placement="right" title="Entferne diese Gruppe"><i class="fas fa-trash"></i>
                          </button>
                          ');
                }

                $row = parseTableRow($row);

                $table .= $row;
              }

              $table = parseTable($table, 'id="detailSubGroupTable" class="table table-bordered table-hover"');

              echo $table;
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>