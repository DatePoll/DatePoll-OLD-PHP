<?php
if (!isset($_REQUEST['userID'])) {
	echo false;
	die();
}
require_once('../../../logic/userHandler.php');
$user = getUsers($_REQUEST['userID']);
$permissions = getPermissions();
// Darf der Benutzer seine eigenen Notizen sehen?
if ($permissions['isAdmin'] || $_REQUEST['userID'] != $_SESSION['userID']) {
	//Hat der Benutzer eine Berechtigung?
	if ($permissions['isAdmin'] ||
			$permissions['canManageUsers'] ||
			$permissions['canManagePolls'] ||
			$permissions['canManageOrganisation'] ||
			$permissions['canManageGroups'] ||
			$permissions['canManageSubgroups'])
		$permissions = true;
	else
		$permissions = false;
} else
	$permissions = false;

?>
<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="userModalTitle"
     aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="userModalTitle">Benutzer #<?php echo $_REQUEST['userID']; ?> bearbeiten</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<?php
				if (isset($_REQUEST['error']))
					echo '
							<li class="list-group-item">
	              <div class="alert alert-danger alert-dismissible fade show" role="alert">
	                <strong>Bearbeiten fehlgeschlagen!</strong> ' . $_REQUEST['error'] . '
	              </div>
	            </li>';
				?>

				<form action="#">
					<form action="#" method="POST">
						<div class="form-row">
							<div class="col-12 col-sm-12 col-md-8 col-lg-7 col-xl-7">
								<div class="form-group">
									<label for="inputEmail">Email Adresse</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<label for="inputEmail" class="input-group-text">@</label>
										</div>
										<input type="email" class="form-control" id="inputEmail" name="inputEmail"
										       aria-describedby="emailHelp"
										       placeholder="Email Adresse eingeben" value="<?php echo $user['email']; ?>" required>
									</div>
								</div>
							</div>

							<div class="col-12 col-sm-12 col-md-4 col-lg-5 col-xl-5">
								<div class="form-group">
									<label for="inputBirthdate">Geburtsdatum</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<label for="inputBirthdate" class="input-group-text"><i class="fas fa-birthday-cake"></i></label>
										</div>
										<input type="date" class="form-control" id="inputBirthdate" name="inputBirthdate"
										       value="<?php echo $user['birthdate']; ?>" required>
									</div>
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="col-6 col-sm-6 col-md-2 col-lg-3 col-xl-3">
								<div class="form-group">
									<label for="inputTitle">Titel</label>
									<div class="input-group">
										<input type="text" class="form-control" id="inputTitle" name="inputTitle" placeholder="Titel"
										       value="<?php echo $user['title']; ?>">
									</div>
								</div>
							</div>
							<div class="col-6 col-sm-6 col-md-5 col-lg-4 col-xl-4">
								<div class="form-group">
									<label for="inputFirstname">Vorname</label>
									<div class="input-group">
										<input type="text" class="form-control" id="inputFirstname" name="inputFirstname"
										       placeholder="Vorname" value="<?php echo $user['firstname']; ?>" required>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
								<div class="form-group">
									<label for="inputSurname">Nachname</label>
									<div class="input-group">
										<input type="text" class="form-control" id="inputSurname" name="inputSurname"
										       placeholder="Nachname" value="<?php echo $user['surname']; ?>" required>
									</div>
								</div>
							</div>
						</div>

						<hr/>

						<div class="form-row">
							<div class="col-12 col-sm-10 col-md-10">
								<div class="form-group">
									<label for="inputStreetname">Straße</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<label for="inputStreetname" class="input-group-text"><i class="fas fa-road"></i></label>
										</div>
										<input type="text" class="form-control" id="inputStreetname" name="inputStreetname"
										       placeholder="Straßenname" value="<?php echo $user['streetname']; ?>">
									</div>
								</div>
							</div>

							<div class="col-6 col-sm-2 col-md-2">
								<div class="form-group">
									<label for="inputStreetnumber">Nummer</label>
									<input type="text" class="form-control" id="inputStreetnumber" name="inputStreetnumber"
									       placeholder="12" value="<?php echo $user['streetnumber']; ?>">
								</div>
							</div>
						</div>

						<div class="form-row">
							<div class="col-4 col-sm-4 col-md-4">
								<div class="form-group">
									<label for="inputZipcode">Postleitzahl</label>
									<input type="number" class="form-control " id="inputZipcode" name="inputZipcode" placeholder="3730"
									       min="1000" onchange="checkValue()" value="<?php echo $user['zip_code']; ?>">
								</div>
							</div>

							<div class="col-12 col-sm-8 col-md-8">
								<div class="form-group">
									<label for="inputLocation">Ort</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<label for="inputLocation" class="input-group-text"><i class="fab fa-fort-awesome"></i></label>
										</div>
										<input type="text" class="form-control" id="inputLocation" name="inputLocation"
										       placeholder="Eggenburg" value="<?php echo $user['location']; ?>">
									</div>
								</div>
							</div>
						</div>

						<hr/>

						<div class="form-row">
							<div class="col-12 col-sm-12 col-md-6">
								<div class="form-group">
									<label for="inputJoindate">Eintrittsdatum</label>
									<div class="input-group">
										<div class="input-group-prepend">
											<label for="inputJoindate" class="input-group-text"><i class="fas fa-calendar"></i></label>
										</div>
										<input type="date" class="form-control" id="inputJoindate" name="inputJoindate"
										       value="<?php echo $user['joindate']; ?>">
									</div>
								</div>
							</div>

							<div class="col-12 col-sm-12 col-md-3">
								<div class="form-group text-center">
									<label for="inputActive">Aktives Mitglied</label>
									<div class="input-group">
										<input type="checkbox" class="form-control" id="inputActive"
										       name="inputActive" <?php echo($user['active'] == 1 ? 'checked' : ''); ?>>
									</div>
								</div>
							</div>

							<div class="col-12 col-sm-12 col-md-3">
								<div class="form-group text-center">
									<label for="inputVerified">Bestätigtes Mitglied</label>
									<div class="input-group">
										<input type="checkbox" class="form-control" id="inputVerified"
										       name="inputVerified" <?php echo($user['hasBeenActivated'] == 1 ? 'checked' : ''); ?>>
									</div>
								</div>
							</div>
						</div>

						<hr/>

						<div class="col-12 w-100">
							<div class="form-group text-center">
								<label for="userNotes">Beschreibung</label><br/>
								<textarea maxlength="2000" id="userNotes" class="w-100"
								          placeholder="Hier kannst du dich selbs ein wenig beschreiben, wenn du magst :D"><?php echo $user['userNotes']; ?></textarea>
							</div>
						</div>

						<?php if ($permissions){?>
						<div class="col-12 w-100">
							<div class="form-group text-center">
								<label for="adminNotes">Admin-Notizen</label><br/>
								<textarea maxlength="2000" id="adminNotes" class="w-100"
								          placeholder="Hier kannst du wichtige Notizen über den Benutzer eintragen"><?php echo $user['adminNotes']; ?></textarea><br/>
								<small class="small">Diese Notizen können nur Benuter mit Rechten sehen, die nicht dieser Benutzer
									sind (außer Admins ^^)
								</small>
							</div>
						</div>
						<?php } ?>
					</form>

					<button type="button" class="btn btn-primary" onclick="editUserSubmit(this)" data-dismiss="modal">
						<i class="fas fa-save"></i> Speichern
					</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>

					<input type="hidden" id="userID" value="<?php echo $user['userID']; ?>">
					<input type="hidden" id="action" value="editUser">
			</div>
		</div>
	</div>
</div>