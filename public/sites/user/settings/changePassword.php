<?php
require_once('../../../logic/userHandler.php');

if (!canAccessBasicPage()) {
  echo false;
  die();
}
?>

<input id="contentTitle" type="hidden" data-title="edit/profile/password"/>

<div class="card">
  <div class="card-header">
    <i class="fas fa-lock"></i> Passwort ändern
    <button id="sendPasswordVerificationButton" class="btn btn-outline-success float-right" hidden
            onclick="sendPasswordVerificationCode()">
      Code senden
    </button>

    <button id="changePasswordButton" class="btn btn-outline-success float-right" hidden
            onclick="changePassword()">
      Passwort ändern
    </button>

  </div>
  <div class="card-body">

    <div class="row">
      <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
        <div class="form-group">
          <label for="inputOldPassword">Altes Passwort: </label>
          <div class="input-group" id="inputGroupOldPassword">
            <div class="input-group-prepend">
              <label for="inputOldPassword" class="input-group-text"><i id="passwordLock" class="fas fa-lock"></i></label>
            </div>
            <input type="password" class="form-control" id="inputOldPassword" name="inputOldPassword" required
                   oninput="oldPasswordChange()" autofocus>
          </div>
          <div class="row" style="margin-left: 1px;">
            <small id="passwordIncorrect" hidden class="form-text" style="color: red">
              Das Passwort stimmt nicht!</small>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
        <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="form-group">
              <label for="inputNewPassword">Neues Passwort:</label>
              <div class="input-group" id="inputGroupNewPassword">
                <div class="input-group-prepend">
                  <label for="inputNewPassword" class="input-group-text"><i class="fas fa-key"></i></label>
                </div>
                <input type="password" class="form-control" id="inputNewPassword" name="inputNewPassword" required
                       disabled oninput="newPasswordChange()">
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="form-group">
              <label for="inputNewPasswordAgain">Neues Passwort: (erneut) </label>
              <div class="input-group" id="inputGroupNewPasswordAgain">
                <div class="input-group-prepend">
                  <label for="inputNewPasswordAgain" class="input-group-text"><i class="fas fa-key"></i></label>
                </div>
                <input type="password" class="form-control" id="inputNewPasswordAgain" name="inputNewPasswordAgain"
                       disabled oninput="newPasswordChange()" required>
              </div>
              <div class="row" style="margin-left: 1px;">
                <small id="passwordNotEquals" hidden class="form-text" style="color: red">
                  Deine Passwörter stimmen nicht überien.</small>
                <small id="passwordInvalid" hidden class="form-text" style="color: red">
                  Dein Passwort muss mindestens <strong>6</strong> und
                  maximal <strong>512</strong> Zeichen haben. Außerdem darf es <strong>nicht</strong> aus
                  <strong>Leerzeichen</strong> und <strong>Tab-Zeichen</strong> bestehen.
                </small>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
        <div class="form-group">
          <label for="inputPasswordChangeCode">Code: </label>
          <div class="input-group">
            <div class="input-group-prepend">
              <label for="inputPasswordChangeCode" class="input-group-text"><i class="fas fa-chess-king"></i></label>
            </div>
            <input type="number" class="form-control" id="inputPasswordChangeCode" name="inputPasswordChangeCode"
                   required disabled oninput="passwordCodeChange()">
          </div>
          <div class="row" style="margin-left: 1px;">
            <small id="codeIncorrect" hidden class="form-text" style="color: red">
              Der Code ist nicht korrekt.
            </small>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>