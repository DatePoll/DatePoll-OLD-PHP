<?php
require_once(__DIR__ . '/../../../logic/userHandler.php');
require_once(__DIR__ . '/../../../logic/parser.php');

if (!canAccessBasicPage()) {
  echo false;
  die();
}
?>

<div class="card">
  <div class="card-header">
    <i class="fas fa-phone"></i> Telefonnummern verwalten
    <button id="addTelephoneNumberButton" class="btn btn-outline-success float-right" hidden
            onclick="addTelephoneNumber()" type="submit">
      <i class="fas fa-plus-circle"></i> Telefonnummer hinzufügen
    </button>

  </div>
  <div class="card-body">

    <div class="row">
      <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
        <div class="form-group">
          <label for="inputLabel">Label: </label>
          <div class="input-group" id="inputGroupLabel">
            <div class="input-group-prepend">
              <label for="inputLabel" class="input-group-text"><i class="fas fa-tag"></i></label>
            </div>
            <input type="text" class="form-control" id="inputLabel" name="inputLabel" required
                   oninput="telephoneNumberChange()" autofocus>
          </div>
          <div class="row" style="margin-left: 1px;">
            <small id="labelIncorrect" hidden class="form-text" style="color: red">
              Das Label muss zwischen <strong>einem</strong> und <strong>32</strong> Zeichen lang sein!
            </small>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-8 col-xl-8">
        <div class="form-group">
          <label for="inputTelephoneNumber">Telefonnummer: </label>
          <div class="input-group" id="inputGroupTelephoneNumber">
            <div class="input-group-prepend">
              <label for="inputTelephoneNumber" class="input-group-text"><i class="fas fa-phone"></i></label>
            </div>
            <input type="text" class="form-control" id="inputTelephoneNumber" name="inputTelephoneNumber" required
                   oninput="telephoneNumberChange()">
          </div>
          <div class="row" style="margin-left: 1px;">
            <small id="telephoneNumberInvalid" hidden class="form-text" style="color: red">
              Die Telefonnummer ist nicht nicht valide.
            </small>
            <small id="telephoneNumberAlreadyExists" hidden class="form-text" style="color: red">
              Die Telefonnummer exestiert bereits.
            </small>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="table-responsive">
        <?php
        $table = parseTableHead(parseTableData('Label') . parseTableData('Telefonnummern')
          . parseTableData('Löschen'));
        $numbers = getTelephoneNumbers();
        foreach ($numbers as $number) {
          $row = "";

          $row .= parseTableData($number['label']);
          $row .= parseTableData($number['number']);
          $row .= parseTableData('
            <button class="btn btn-danger" data-toggle="tooltip" data-placement="right" 
                title="Lösche diese Telefonnummer" onclick="removeTelephoneNumber('. $number['number'].')">
              <i class="fas fa-trash"></i>
            </button>');

          $row = parseTableRow($row);
          $table .= $row;
        }
        $table = parseTable($table, 'class="table table-bordered table-hover"');

        echo $table;
        ?>
      </div>
    </div>
  </div>
</div>