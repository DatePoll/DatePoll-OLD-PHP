<?php
require_once('../../../logic/userHandler.php');

if (!canAccessBasicPage()) {
  echo false;
  die();
}
?>

<input id="contentTitle" type="hidden" data-title="edit/profile/email"/>

<div class="card">
  <div class="card-header">
    <i class="fas fa-envelope"></i> Email Adresse ändern

    <button id="sendEmailVerificationCodeButton" class="btn btn-outline-success float-right" hidden
            onclick="sendEmailVerificationCode()">
      Code senden
    </button>

    <button id="changeEmailButton" class="btn btn-outline-success float-right" hidden
            onclick="changeEmail()">
      Email-Adresse ändern
    </button>

  </div>
  <div class="card-body">

    <div class="row">
      <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
        <div class="form-group">
          <label for="inputEmailChangePassword">Passwort: </label>
          <div class="input-group" id="inputGroupEmailChangePassword">
            <div class="input-group-prepend">
              <label for="inputEmailChangePassword" class="input-group-text">
                <i class="fas fa-lock" id="passwordLock"></i></label>
            </div>
            <input type="password" class="form-control" id="inputEmailChangePassword" name="inputEmailChangePassword"
                   required
                   oninput="emailChangePasswordChange()" autofocus>
          </div>
          <div class="row" style="margin-left: 1px;">
            <small id="passwordInCorrect" hidden class="form-text" style="color: red">
              Das Passwort ist nicht korrekt.
            </small>
            <small id="passwordToTiny" hidden class="form-text" style="color: red">
              Dein Passwort muss mindestens fünf Zeichen lang sein.
            </small>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
        <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="form-group">
              <label for="inputNewEmail">Neue Email-Adresse:</label>
              <div class="input-group" id="inputGroupNewEmail">
                <div class="input-group-prepend">
                  <label for="inputNewEmail" class="input-group-text"><i class="fas fa-at"></i></label>
                </div>
                <input type="email" class="form-control" id="inputNewEmail" name="inputNewEmail" required disabled
                       oninput="newEmailChange()">
              </div>
              <div class="row" style="margin-left: 1px;">
                <small id="emailNotAnEmail" hidden class="form-text" style="color: red">
                  Die eingegebene Email-Adresse ist nicht valide.
                </small>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
        <div class="form-group">
          <label for="inputEmailChangeCode">Code: </label>
          <div class="input-group">
            <div class="input-group-prepend">
              <label for="inputEmailChangeCode" class="input-group-text"><i class="fas fa-chess-king"></i></label>
            </div>
            <input type="number" class="form-control" id="inputEmailChangeCode" name="inputEmailChangeCode"
                   required disabled oninput="emailCodeChange()">
          </div>
          <div class="row" style="margin-left: 1px;">
            <small id="codeIncorrect" hidden class="form-text" style="color: red">
              Der Code ist nicht korrekt.
            </small>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>