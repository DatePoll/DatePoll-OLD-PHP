<?php
require_once('../../../logic/userHandler.php');

if (!canAccessBasicPage()) {
  echo false;
  die();
}

?>

<a href="#!/profile/edit/data" class="list-group-item list-group-item-action sideMenuItem active" data-name="changeUserdata">
  <i class="fas fa-user-circle"></i> Deine Daten</a>

<a href="#!/profile/edit/password" class="list-group-item list-group-item-action sideMenuItem" data-name="changePassword">
  <i class="fas fa-lock"></i> Passwort ändern</a>

<a href="#!/profile/edit/email" class="list-group-item list-group-item-action sideMenuItem" data-name="changeEmail">
  <i class="fas fa-envelope"></i> Email Adresse ändern</a>

<a href="#!/profile/edit/telephonenumber" class="list-group-item list-group-item-action sideMenuItem" data-name="manageTelephoneNumber">
	<i class="fas fa-phone"></i> Telefonnummern verwalten</a>

<a href="#!/profile/edit/tokens" class="list-group-item list-group-item-action sideMenuItem" data-name="manageTokens">
  <i class="fas fa-address-card"></i> Angemeldete Geräte verwalten</a>

<a href="#!/profile/edit/other" class="list-group-item list-group-item-action sideMenuItem" data-name="changeOther">
  <i class="fas fa-info-circle"></i> Andere Informationen</a>

