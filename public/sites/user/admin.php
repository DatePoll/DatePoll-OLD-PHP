<?php
require_once('../../logic/userHandler.php');
require_once('../../logic/parser.php');

if (!canAccessBasicPage()) {
	echo false;
	die();
}
if (!getPermissions()['canManageUsers'] == 1 and !getPermissions()['isAdmin'] == 1) {
	return false;
	die();
}
?>
<input id="contentTitle" type="hidden" data-title="edit/user"/>

<h1 class="card-title" style="text-align: center;"><i class="fas fa-user"></i> Benutzer verwalten</h1>
<div class="card-deck">
	<div class="col-sm-12" style="max-height: 100%;">
		<div class="card">
			<div class="card-header" style="text-align: center;">
				Benutzer-Liste
				<button class="btn btn-outline-success float-right" onclick="addUser()">
					<i class="fas fa-plus"></i>
				</button>
			</div>
			<div class="card-body text-center">
				<?php
				$table = parseTableHead(parseTableData('userID') . parseTableData('Vorname') . parseTableData('Nachname') . parseTableData('Email-Adresse') . parseTableData(''));
				$users = getUsers();
				foreach ($users as $user) {
					$btnEdit = parseTag("button", parseTag("i", "", "class='fas fa-edit'"),
							"class = 'btn btn-primary' onclick='editUser(" . $user['userID'] . ")'");
					$btnDelete = parseTag("button", parseTag("i", "", "class='fas fa-trash'"),
							"class = 'btn btn-danger' onclick='deleteUser(" . $user['userID'] . ")'");
					$userState; //set the background of the user to green, if he hasn't beem activated yet by an admin or to grey, if he is not an active member
					if(!$user['hasBeenActivated'])
						$userState = 'success';
					elseif (!$user['active'])
						$userState = 'info';
					else
						$userState = 'white';
					$userState = "class='alert-$userState'";

					$row = "";

					$row .= parseTableData($user['userID']);
					$row .= parseTableData($user['firstname']);
					$row .= parseTableData($user['surname']);
					$row .= parseTableData($user['email']);
					$row .= parseTableData($btnEdit . "\n" . $btnDelete);

					$row = parseTableRow($row, $userState);
					$table .= $row;
				}
				$table = parseTable($table, 'class="table table-bordered table-hover"');

				echo $table;
				?>
			</div>
		</div>
		<div class="card" id="detail" hidden></div>
	</div>

	<div class="col-md-6">
		<div class="card">
			<div class="card-header" style="text-align: center;">
				Benutzer-Daten importieren/exportieren
			</div>
			<div class="card-body" style="text-align: center">
				<!--Hier soll es möglich sein die Benutzerdaten per CSV zu imortieren oder zu exportieren-->
					<button class="btn btn-success">exportieren
						<i class="fas fa-upload"></i>
					</button>
					<button class="btn btn-danger">importieren
						<i class="fas fa-download"></i>
					</button>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="card">
			<div class="card-header" style="text-align: center;">
				Benutzer-Verwalter
				<button class="btn btn-outline-success float-right" onclick="addManager('canManageUsers')">
					<i class="fas fa-plus"></i>
				</button>
			</div>
			<div class="card-body text-center">
				<?php
				$table = parseTableHead(parseTableData('Name') . parseTableData('Email-Adresse'));
				$users = getUserManagers();
				foreach ($users as $user) {
					$detail = 'onclick="getUserPermission(' . $user['userID'] . ', \'isAdmin,canManageUsers\')"';
					$row = "";

					$row .= parseTableData($user['name'], $detail);
					$row .= parseTableData($user['email'], $detail);

					$row = parseTableRow($row);
					$table .= $row;
				}
				$table = parseTable($table, 'class="table table-bordered table-hover"');

				echo $table;
				?>
			</div>
		</div>
	</div>

	<div class="col-md-6 col-md-6" style="max-height: 100%; display: none;">
		<div class="card">
			<div class="card-header text-center">
				Rechte
				<button class="btn btn-outline-warning float-right" onclick="hideDetails('permissionDetail')">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="card-body text-center" id="permissionDetail" ></div>
		</div>
	</div>

	<div id="modalContainer"></div>
</div>

<?php
if (isset($_GET['successful'])) {
	switch ($_GET['successful']) {
		case 'add':
			echo parseAlert('<strong>Anlegen erfolgreich!</strong>', 'success', true);
			break;
		case 'edit':
			echo parseAlert('<strong>Bearbeiten erfolgreich!</strong>', 'success', true);
			break;
		case 'remove':
			echo parseAlert("<strong>Löschen erfolgreich!</strong>", 'success', true);
			break;
	}
}
if (isset($_GET['error'])) {
	switch ($_GET['error']) {
		case 'remove':
			echo parseAlert("<strong>Löschen fehlgeschlagen!</strong>", 'danger', true);
			break;
	}
}