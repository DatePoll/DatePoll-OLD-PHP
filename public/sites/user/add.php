<?php
require_once('../../logic/userHandler.php');

?>
<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="userModalTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="userModalTitle">Benutzer anlegen</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<?php
				if (isset($_REQUEST['$error']))
					echo '
							<li class="list-group-item">
	              <div class="alert alert-danger alert-dismissible fade show" role="alert">
	                <strong>Anlegen fehlgeschlagen!</strong> Bitte überprüfe deine Eingaben nochmals!
	              </div>
	            </li>';
				?>

				<form action="#" method="POST">
					<div class="form-row">
						<div class="col-12 col-sm-12 col-md-8 col-lg-7 col-xl-7">
							<div class="form-group">
								<label for="inputEmail">Email Adresse</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<label for="inputEmail" class="input-group-text">@</label>
									</div>
									<input type="email" class="form-control" id="inputEmail" name="inputEmail"
									       aria-describedby="emailHelp"
									       placeholder="Email Adresse eingeben" required>
								</div>
							</div>
						</div>

						<div class="col-12 col-sm-12 col-md-4 col-lg-5 col-xl-5">
							<div class="form-group">
								<label for="inputBirthdate">Geburtsdatum</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<label for="inputBirthdate" class="input-group-text"><i class="fas fa-birthday-cake"></i></label>
									</div>
									<input type="date" class="form-control" id="inputBirthdate" name="inputBirthdate" required>
								</div>
							</div>
						</div>
					</div>

					<div class="form-row">
						<div class="col-6 col-sm-6 col-md-2 col-lg-3 col-xl-3">
							<div class="form-group">
								<label for="inputTitle">Titel</label>
								<div class="input-group">
									<input type="text" class="form-control" id="inputTitle" name="inputTitle" placeholder="Titel">
								</div>
							</div>
						</div>
						<div class="col-6 col-sm-6 col-md-5 col-lg-4 col-xl-4">
							<div class="form-group">
								<label for="inputFirstname">Vorname</label>
								<div class="input-group">
									<input type="text" class="form-control" id="inputFirstname" name="inputFirstname"
									       placeholder="Vorname" required>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
							<div class="form-group">
								<label for="inputSurname">Nachname</label>
								<div class="input-group">
									<input type="text" class="form-control" id="inputSurname" name="inputSurname"
									       placeholder="Nachname" required>
								</div>
							</div>
						</div>
					</div>

					<div class="form-row">
						<div class="col-12 col-sm-10 col-md-10">
							<div class="form-group">
								<label for="inputStreetname">Straße</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<label for="inputStreetname" class="input-group-text"><i class="fas fa-road"></i></label>
									</div>
									<input type="text" class="form-control" id="inputStreetname" name="inputStreetname"
									       placeholder="Straßenname">
								</div>
							</div>
						</div>

						<div class="col-6 col-sm-2 col-md-2">
							<div class="form-group">
								<label for="inputStreetnumber">Nummer</label>
								<input type="text" class="form-control" id="inputStreetnumber" name="inputStreetnumber"
								       placeholder="12">
							</div>
						</div>
					</div>

					<div class="form-row">
						<div class="col-4 col-sm-4 col-md-4">
							<div class="form-group">
								<label for="inputZipcode">Postleitzahl</label>
								<input type="number" class="form-control " id="inputZipcode" name="inputZipcode" placeholder="3730"
								       min="1000" onchange="checkValue()">
							</div>
						</div>

						<div class="col-12 col-sm-8 col-md-8">
							<div class="form-group">
								<label for="inputLocation">Ort</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<label for="inputLocation" class="input-group-text"><i class="fab fa-fort-awesome"></i></label>
									</div>
									<input type="text" class="form-control" id="inputLocation" name="inputLocation"
									       placeholder="Eggenburg">
								</div>
							</div>
						</div>
					</div>

					<div class="form-row">
						<div class="col-12 col-sm-12 col-md-12">
							<div class="form-group">
								<label for="inputStreetname">Eintrittsdatum</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<label for="inputJoindate" class="input-group-text"><i class="fas fa-calendar"></i></label>
									</div>
									<input type="date" class="form-control" id="inputJoindate" name="inputJoindate"
									       value="<?php echo date('Y-m-d') ?>">
								</div>
							</div>
						</div>
					</div>

					<button type="button" class="btn btn-primary" onclick="addUserSubmit(this)" data-dismiss="modal">
						<i class="fas fa-save"></i> Speichern
					</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>

					<input type="hidden" id="action" value="addUser">
				</form>
			</div>
		</div>
	</div>
</div>