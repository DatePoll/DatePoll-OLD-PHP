<?php
require_once('../../logic/userHandler.php');

if (!canAccessBasicPage()) {
  echo false;
  die();
}

?>

<a href="#!/admin/poll" class="list-group-item list-group-item-action sideMenuItem active" data-name="poll"
   onclick="load_main_content_and_change_active('./sites/administration/poll.php', 'poll')">
  <i class="fas fa-calendar-check"></i> Abstimmungen</a>

<a href="#!/admin/user" class="list-group-item list-group-item-action sideMenuItem" data-name="user"
   onclick="load_main_content_and_change_active('./sites/user/admin.php', 'user')">
  <i class="fas fa-user"></i> Benutzer</a>

<a href="#!/admin/group" class="list-group-item list-group-item-action sideMenuItem" data-name="group"
   onclick="load_main_content_and_change_active('./sites/group/admin.php', 'group')">
  <i class="fas fa-users"></i> Gruppen</a>

<a href="#!/admin/organisation" class="list-group-item list-group-item-action sideMenuItem" data-name="organisation"
   onclick="load_main_content_and_change_active('./sites/basic/organisation.php', 'organisation')">
  <i class="fas fa-sitemap"></i> Verein</a>

<a href="#!/admin/permission" class="list-group-item list-group-item-action sideMenuItem" data-name="permission"
   onclick="load_main_content_and_change_active('./sites/permission/admin.php', 'permission')">
  <i class="fas fa-tasks"></i> Rechte</a>

<a href="#!/admin/settings" class="list-group-item list-group-item-action sideMenuItem" data-name="settings"
   onclick="load_main_content_and_change_active('./sites/basic/settings.php', 'settings')">
  <i class="fas fa-cog"></i> Servereinstellungen</a>

