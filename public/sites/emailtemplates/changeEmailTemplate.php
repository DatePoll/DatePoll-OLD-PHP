<?php

require_once (__DIR__ . '/bootstrapTemplate.php');

function getEmailBodyChangeEmail($emailAddress, $name , $code){
  $link = (isset($_SERVER['HTTPS']) ? "https" : "http") . '://'. $_SERVER['HTTP_HOST'].'/reset/'.$emailAddress . '/'.$code;
  $websitelink = (isset($_SERVER['HTTPS']) ? "https" : "http") . '://'. $_SERVER['HTTP_HOST'].'/';

	return '
<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="UTF-8">

	'.getBootstrapCSS().'
	<style>
    .footer {
      position: absolute;
      bottom: 0;
      width: 100%;
      height: 60px;
    }
  </style>
</head>
<body>
<nav class="navbar navbar-dark bg-primary">
  <a class="navbar-brand" href="'.$websitelink.'">DatePoll</a>
  <div class="navbar-nav">
    <div class="nav-item nav-link active">Email-Adresse Änderung</div>
  </div>
</nav>

<div class="card" style="margin-top: 20px; margin-left: 40px; margin-right: 40px;">
  <div class="card-body">
    <h5 class="card-title">Hallo '.$name.'</h5>
    <h6 class="card-subtitle mb-2 text-muted">Für deinen DatePoll Account wurde eine Email-Adresse Änderung angefordert.</h6>
     <p class="card-text">
      Bitte gib folgenden Code auf DatePoll in das vorgesehen Kästchen ein: 
      <code>'.$code.'</code>
    </p>
  </div>
  <div class="card-footer text-muted">
    Du willst deine Email-Adresse gar nicht ändern? Ignoriere diese Email einfach.
  </div>
</div>

<nav class="navbar navbar-dark bg-primary footer">
  <div class="navbar-nav">
    <a class="nav-item nav-link">Impressum</a>
  </div>
</nav>

</body>
</html>
';
}
?>