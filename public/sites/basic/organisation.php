<?php
require_once ('../../logic/userHandler.php');
require_once ('../../logic/organisationHandler.php');
require_once ('../../logic/parser.php');

if (! canAccessBasicPage ()) {
	echo false;
	die ();
}
if (! getPermissions () ['canManageOrganisation'] == 1 and ! getPermissions () ['isAdmin'] == 1) {
	return false;
	die ();
}
?>
<input id="contentTitle" type="hidden" data-title="edit/organisation" />

<h1 class="card-title" style="text-align: center;">
	<i class="fas fa-sitemap"></i> Verein verwalten
</h1>
<div class="card-group">
	<div class="card">
		<div class="card-header" style="text-align: center;">
			Vereinsinfos
			<button class="btn btn-primary float-right" data-toggle='modal'
				data-target='#Modal' onclick="editOrganisation()">
				<i class="fas fa-edit"></i>
			</button>
		</div>
		<div class="card-body">
			<?php
			$organisation = getOrganisation ();
			$table = "";
			
			$table .= parseTableRow ( parseTableData ( "Name" ) . parseTableData ( $organisation ['name'] ) );
			$table .= parseTableRow ( parseTableData ( "Beschreibung" ) . parseTableData ( $organisation ['description'] ) );
			$table .= parseTableRow ( parseTableData ( "Homepage-Link" ) . parseTableData ( parseTag ( "a", $organisation ['homepageLink'], "href=" . $organisation ['homepageLink'] ) ) );
			$table .= parseTableRow ( parseTableData ( "Gründungsdatum" ) . parseTableData ( parseTag ( "time", date_format ( date_create_from_format ( "Y-m-d", $organisation ['foundingDate'] ), "d.m.Y" ), "datetime='" . $organisation ['foundingDate'] . " 00:00'" ) ) );
			
			$table = parseTable ( $table, 'class="table table-bordered table-hover"' );
			echo $table;
			?>
		</div>
	</div>

	<div class="col-md-6">
		<div class="card">
			<div class="card-header" style="text-align: center;">
				Organisations-Verwalter
				<button class="btn btn-outline-success float-right"
					data-toggle='modal' data-target='#Modal'
					onclick="addManager('canManageOrganisation')">
					<i class="fas fa-plus"></i>
				</button>
			</div>
			<div class="card-body text-center">
				<?php
				$table = parseTableHead ( parseTableData ( 'Name' ) . parseTableData ( 'Email-Adresse' ) );
				$users = getOrganisationManagers ();
				foreach ( $users as $user ) {
					$detail = 'onclick="getUserPermission(' . $user ['userID'] . ', \'isAdmin,canManageOrganisation\')"';
					$row = "";
					
					$row .= parseTableData ( $user ['name'], $detail );
					$row .= parseTableData ( $user ['email'], $detail );
					
					$row = parseTableRow ( $row );
					$table .= $row;
				}
				$table = parseTable ( $table, 'class="table table-bordered table-hover"' );
				
				echo $table;
				?>
			</div>
		</div>
	</div>

	<div class="col-md-6 col-md-6" style="max-height: 100%; display: none;">
		<div class="card">
			<div class="card-header text-center">
				Rechte
				<button class="btn btn-outline-warning float-right"
					onclick="hideDetails('permissionDetail')">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="card-body text-center" id="permissionDetail"></div>
		</div>
	</div>


	<div id="modalContainer"></div>
</div>
<?php
if (isset ( $_GET ['successful'] )) {
	if ($_GET ['successful'] == 'edit')
		echo parseAlert ( '<strong>Bearbeiten erfolgreich!</strong>', 'success', true );
}
?>