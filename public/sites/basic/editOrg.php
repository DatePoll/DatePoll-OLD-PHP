<?php
require_once ('../../logic/organisationHandler.php');
$org = getOrganisation();

?>
<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="ModalTitle" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="Modal">Verein bearbeiten</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<?php
				if (isset($_REQUEST['$error']))
					echo '
							<li class="list-group-item">
	              <div class="alert alert-danger alert-dismissible fade show" role="alert">
	                <strong>Bearbeiten fehlgeschlagen!</strong> Bitte überprüfe deine Eingaben nochmals!
	              </div>
	            </li>';
				?>

				<form method="post" action="#">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" id="name" name="name" aria-describedby="titleHelp"
									 placeholder="Name des Vereins eingeben" value="<?php echo $org['name']; ?>" required>
					</div>

					<div class="form-group">
						<label for="description">Beschreibung</label>
						<input type="text" class="form-control" id="description" name="description" aria-describedby="titleHelp"
									 placeholder="Beschreibung des Vereins eingeben (optional)" value="<?php echo (isset($org['description']) ? $org['description'] : ''); ?>">
					</div>

					<div class="form-group">
						<label for="homepageLink">Homepage</label>
						<input type="url" class="form-control" id="homepageLink" name="homepageLink" aria-describedby="titleHelp"
									 placeholder="Homepage-Link des Vereins eingeben" value="<?php echo (isset($org['homepageLink']) ? $org['homepageLink'] : ''); ?>">
					</div>

					<div class="form-group">
						<label for="foundingDate">Gründungsdatum</label>
						<input type="date" class="form-control" id="foundingDate" name="foundingDate" aria-describedby="titleHelp"
									 placeholder="Gründungsdatum des Vereins eingeben" value="<?php echo $org['foundingDate']; ?>" required>
					</div>

					<button type="button" class="btn btn-primary" onclick="editOrganisationSubmit(this)" data-dismiss="modal">Änderungen speichern</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>

					<input type="hidden" id="action" value="edit">
				</form>
			</div>
		</div>
	</div>
</div>