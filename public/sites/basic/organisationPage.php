<?php
require_once('../../logic/userHandler.php');
require_once('../../logic/organisationHandler.php');
require_once('../../logic/parser.php');

if (!canAccessBasicPage()) {
	echo false;
	die();
}

$organisation = getOrganisation();
?>
<input id="contentTitle" type="hidden" data-title="organisation"/>

<h1 class="card-title" style="text-align: center;"><i class="fas fa-sitemap"></i> <?php echo $organisation['name']; ?>
</h1>
<div class="d-flex justify-content-center align-self-center">
	<div class="col-lg-8">
		<div class="card">
			<div class="card-header" style="text-align: center;">
				Vereinsinfos
			</div>
			<div class="card-body">
				<?php
				$table = "";

				$table .= parseTableRow(parseTableData("Name") . parseTableData($organisation['name']));
				$table .= parseTableRow(parseTableData("Beschreibung") . parseTableData($organisation['description']));
				$table .= parseTableRow(parseTableData("Homepage-Link") . parseTableData(parseTag("a", $organisation['homepageLink'], "href=" . $organisation['homepageLink'])));
				$table .= parseTableRow(parseTableData("Gründungsdatum") . parseTableData(parseTag("time",
								date_format(
										date_create_from_format("Y-m-d", $organisation['foundingDate']), "d.m.Y"),
								"datetime='" . $organisation['foundingDate'] . " 00:00'")));

				$table = parseTable($table, 'class="table table-bordered table-hover"');
				echo $table;
				?>
			</div>
		</div>
	</div>
</div>