<?php

require_once(__DIR__ . '/../../logic/userHandler.php');
require_once(__DIR__ . '/../../logic/parser.php');

?>
<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="register"
     aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Registrierung</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <?php echo parseStandardModalAlerts(); ?>

        <div class="collapse" id="enterInputs">
          <div class="form-row">
            <div class="col-12 col-sm-12 col-md-8 col-lg-6 col-xl-6">
              <div class="form-group">
                <label for="inputEmailAddress">Email-Adresse:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputEmailAddress" class="input-group-text"><i class="fas fa-envelope"></i></label>
                  </div>
                  <input type="email" class="form-control" id="inputEmailAddress" name="inputEmailAddress"
                         placeholder="Email Adresse eingeben" oninput="signupValuesChanged()">
                </div>
                <small id="emailIncorrect" hidden class="form-text" style="color: red">
                  Die Email-Adresse ist inkorrekt!
                </small>
                <small id="emailAlreadyUsed" hidden class="form-text" style="color: red">
                  Die Email-Adresse wurde bereits verwendet!
                </small>

                <small id="emailHelp" class="form-text text-muted">Deine Email-Adresse wird nicht an Dritte
                  weitergegeben.
                </small>
              </div>
            </div>

            <div class="col-12 col-sm-12 col-md-4 col-lg-6 col-xl-6">
              <div class="form-group">
                <label for="inputBirthdate">Geburtsdatum: </label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputBirthdate" class="input-group-text"><i class="fas fa-birthday-cake"></i></label>
                  </div>
                  <input type="date" class="form-control" id="inputBirthdate" name="inputBirthdate"
                         oninput="signupValuesChanged()">
                </div>
                <small id="birthdateIncorrect" hidden class="form-text" style="color: red">
                  Das Geburtsdatum ist inkorrekt!
                </small>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">
                <label for="inputPassword">Passwort:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputPassword" class="input-group-text"><i class="fas fa-key"></i></label>
                  </div>
                  <input type="password" class="form-control" id="inputPassword" name="inputPassword"
                         placeholder="Passwort eingeben" required oninput="signupValuesChanged()">
                </div>
                <small id="passwordIncorrect" hidden class="form-text" style="color: red">
                  Das Passwort entspricht nicht den Vorgaben, bitte überprüfe diese!
                </small>
                <small id="passwordNotEquals" hidden class="form-text" style="color: red">
                  Das Passwort stimmen nicht überein!
                </small>
              </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
              <div class="form-group">
                <label for="inputPassword2">Passwort wiederholen:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputPassword2" class="input-group-text"><i class="fas fa-lock"></i></label>
                  </div>
                  <input type="password" class="form-control" id="inputPassword2" name="inputPassword2"
                         placeholder="Passwort erneut eingeben" required oninput="signupValuesChanged()">
                </div>
              </div>
            </div>
          </div>

          <div class="form-row" style="margin-top: -10px; margin-bottom: 15px; margin-left: 2px;">
            <small id="passwordHelp" class="form-text text-muted">Dein Passwort muss mindestens <strong>6</strong> und
              maximal <strong>512</strong> Zeichen haben. Außerdem darf es <strong>nicht</strong> aus
              <strong>Leerzeichen</strong> und <strong>Tab-Zeichen</strong> bestehen.
            </small>
          </div>

          <div class="form-row">
            <div class="col-6 col-sm-6 col-md-2 col-lg-3 col-xl-3">
              <div class="form-group">
                <label for="inputTitle">Titel:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputTitle" class="input-group-text"><i class="fas fa-chess-king"></i></label>
                  </div>
                  <input type="text" class="form-control" id="inputTitle" name="inputTitle"
                         placeholder="Titel" oninput="signupValuesChanged()">
                </div>
                <small id="titleIncorrect" hidden class="form-text" style="color: red">
                  Der Titel darf nicht mehr als <strong>64</strong> Zeichen haben!
                </small>
              </div>
            </div>
            <div class="col-6 col-sm-6 col-md-5 col-lg-4 col-xl-4">
              <div class="form-group">
                <label for="inputFirstname">Vorname:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputFirstname" class="input-group-text"><i class="fas fa-user"></i></label>
                  </div>
                  <input type="text" class="form-control" id="inputFirstname" name="inputFirstname"
                         placeholder="Vorname" required oninput="signupValuesChanged()">
                </div>
                <small id="firstnameIncorrect" hidden class="form-text" style="color: red">
                  Der Vorname darf nicht weniger als <strong>2</strong> und nicht mehr als <strong>128</strong> Zeichen
                  haben!
                </small>
              </div>
            </div>
            <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
              <div class="form-group">
                <label for="inputSurname">Nachname:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputSurname" class="input-group-text"><i class="fas fa-user"></i></label>
                  </div>
                  <input type="text" class="form-control" id="inputSurname" name="inputSurname"
                         placeholder="Nachname" required oninput="signupValuesChanged()">
                </div>
                <small id="surnameIncorrect" hidden class="form-text" style="color: red">
                  Der Nachname darf nicht weniger als <strong>2</strong> und nicht mehr als <strong>128</strong> Zeichen
                  haben!
                </small>
              </div>
            </div>
          </div>

          <div class="form-row">
            <div class="col-12 col-sm-10 col-md-10 col-lg-4 col-xl-4">
              <div class="form-group">
                <label for="inputStreetname">Straße:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputStreetname" class="input-group-text"><i class="fas fa-road"></i></label>
                  </div>
                  <input type="text" class="form-control" id="inputStreetname" name="inputStreetname"
                         placeholder="Straßenname" oninput="signupValuesChanged()">
                </div>
                <small id="streetnameIncorrect" hidden class="form-text" style="color: red">
                  Der Nachname darf nicht weniger als <strong>2</strong> und nicht mehr als <strong>128</strong>Zeichen
                  haben!
                </small>
              </div>
            </div>

            <div class="col-6 col-sm-2 col-md-2 col-lg-2 col-xl-2">
              <div class="form-group">
                <label for="inputStreetnumber">Nummer:</label>
                <input type="text" class="form-control" id="inputStreetnumber" name="inputStreetnumber"
                       placeholder="12" oninput="signupValuesChanged()">
                <small id="streetnumberIncorrect" hidden class="form-text" style="color: red">
                  Die Straßennummer darf nicht mehr als <strong>16</strong>Zeichen haben!
                </small>
              </div>
            </div>

            <div class="col-6 col-sm-6 col-md-6 col-lg-2 col-xl-2">
              <div class="form-group">
                <label for="inputZipcode">Postleitzahl:</label>
                <input type="number" class="form-control " id="inputZipcode" name="inputZipcode"
                       placeholder="3730" min="1000" onchange="checkValue()" oninput="signupValuesChanged()">
                <small id="zipcodeIncorrect" hidden class="form-text" style="color: red">
                  Die Postleitzahl ist invalide!
                </small>
              </div>
            </div>

            <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
              <div class="form-group">
                <label for="inputLocation">Ort:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputLocation" class="input-group-text"><i class="fab fa-fort-awesome"></i></label>
                  </div>
                  <input type="text" class="form-control" id="inputLocation" name="inputLocation"
                         placeholder="Eggenburg" oninput="signupValuesChanged()">
                </div>
                <small id="locationIncorrect" hidden class="form-text" style="color: red">
                  Der Ortsname darf nicht mehr als <strong>128</strong>Zeichen haben!
                </small>
              </div>
            </div>
          </div>

          <a href="" class="float-right" data-dismiss="modal" aria-label="Close"
             onclick="load_modal('sites/authentication/forgotPasswordModal.php', 'modalContainer', function() {
                  $('#enterEmail').collapse('show');
                });">Passwort vergessen?</a>

        </div>

        <div class="collapse" id="enterActivationCode">
          <div class="form-row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div class="form-group">
                <label for="inputCode">Code:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputCode" class="input-group-text"><i class="fas fa-lock"></i></label>
                  </div>
                  <input type="number" class="form-control" id="inputCode" placeholder="Code eingeben" name="inputCode">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="collapse" id="verificationSuccess">
          <?php echo parseAlert("<strong>Heureka!</strong> Deine Email-Adresse wurde verifiziert! Bitte warte noch auf die Aktivierung deines Accounts.", "success", false); ?>
        </div>

      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" id="closeButton" data-dismiss="modal" aria-label="Close">Abbrechen</button>
        <button class="btn btn-primary" id="signupButton" onclick="signup()">Registrieren</button>
        <button class="btn btn-primary" id="activationCodeSendButton" hidden onclick="checkSignupVerificationCode()">Verifizieren</button>
        <button class="btn btn-primary" id="finishButton" hidden data-dismiss="modal" aria-label="Close">Schließen</button>
      </div>
    </div>
  </div>
</div>