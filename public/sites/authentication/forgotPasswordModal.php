<?php

require_once(__DIR__ . '/../../logic/userHandler.php');
require_once(__DIR__ . '/../../logic/parser.php');

?>
<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="pollModalTitle"
     aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Passwort vergessen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <?php echo parseStandardModalAlerts(); ?>

        <div id="passwortResetSuccessAlert" class="alert alert-success" role="alert" hidden>
          <strong>Heureka!</strong> Dein Passwort wurde erfolgreich geändert!
        </div>

        <div class="collapse" id="enterEmail">
          <div id="emailAddressDoesNotExistAlert" class="alert alert-danger" role="alert" hidden>
            <strong>Fehler!</strong> Es exestiert kein Konto für diese Email-Adresse.
          </div>

          <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div class="form-group">
                <label for="inputEmailAddress">Email-Adresse:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputEmailAddress" class="input-group-text"><i class="fas fa-envelope"></i></label>
                  </div>
                  <input type="email" placeholder="Email-Adresse eingeben" class="form-control" id="inputEmailAddress" name="inputEmailAddress">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="collapse" id="enterCode">
          <?php echo parseAlert("<strong>Information!</strong> Bitte gib deine Aktivierungscode ein!", "info")?>

          <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div class="form-group">
                <label for="inputCode">Code:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputCode" class="input-group-text"><i class="fas fa-lock"></i></label>
                  </div>
                  <input type="number" class="form-control" id="inputCode" placeholder="Code eingeben" name="inputCode">
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="collapse" id="enterNewPasswords">
          <?php echo parseAlert("<strong>Heureka!</strong> Dein Code war korrekt! Gib deine neuen Passwörter ein.", "success")?>

          <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div class="form-group">
                <label for="inputPassword">Passwort:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputPassword" class="input-group-text"><i class="fas fa-lock"></i></label>
                  </div>
                  <input type="password" class="form-control" id="inputPassword" placeholder="Passwort eingeben" name="inputPassword">
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
              <div class="form-group">
                <label for="inputPasswordAgain">Passwort (erneut):</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <label for="inputPasswordAgain" class="input-group-text"><i class="fas fa-lock"></i></label>
                  </div>
                  <input type="password" class="form-control" id="inputPasswordAgain" placeholder="Passwort erneut eingeben" name="inputPasswordAgain">
                </div>
              </div>
            </div>
          </div>
        </div>


      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" id="cancelButton" data-dismiss="modal" aria-label="Close">Abbrechen</button>
        <button class="btn btn-primary" id="sendPasswordResetVerificationButton"
                onclick="sendPasswordResetVerification()">Email absenden</button>
        <button class="btn btn-primary" id="checkPasswordResetVerficationCodeButton" hidden
                onclick="checkPasswordResetVerficationCode()">Code überprüfen</button>
        <button class="btn btn-primary" id="resetPasswordButton" hidden
                onclick="resetPassword()">Passwort ändern</button>
        <button class="btn btn-primary" id="closeModalAfterSuccessButton" hidden data-dismiss="modal" aria-label="Close">Schließen</button>
      </div>
    </div>
  </div>
</div>