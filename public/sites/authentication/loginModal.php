<?php

require_once(__DIR__ . '/../../logic/userHandler.php');
require_once(__DIR__ . '/../../logic/parser.php');

?>
<div class="modal fade" id="DatePollModal" tabindex="-1" role="dialog" aria-labelledby="login"
     aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="loginWrongAlert" class="alert alert-danger" role="alert" hidden>
          <strong>Fehler!</strong> Deine Email-Adresse oder dein Passwort sind inkorrekt!
        </div>

        <?php echo parseStandardModalAlerts(); ?>

        <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="form-group">
              <label for="inputEmailAddress">Email-Adresse:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <label for="inputEmailAddress" class="input-group-text"><i class="fas fa-envelope"></i></label>
                </div>
                <input type="email" class="form-control" id="inputEmailAddress" name="inputEmailAddress" placeholder="Email-Adresse">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="form-group">
              <label for="inputPassword">Passwort:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <label for="inputPassword" class="input-group-text"><i class="fas fa-lock"></i></label>
                </div>
                <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Passwort">
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <input id="stayLoggedIn" type="checkbox" aria-label="Checkbox for stay logged in">
            <label for="stayLoggedIn">Angemeldet bleiben</label>
          </div>
        </div>

        <a href="#!" class="float-right" data-dismiss="modal" aria-label="Close"
                onclick="load_modal('sites/authentication/forgotPasswordModal.php', 'modalContainer', function() {
                  $('#enterEmail').collapse('show');
                });">Passwort vergessen?</a>

      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Abbrechen</button>
        <button class="btn btn-primary" onclick="login()">Login</button>
      </div>
    </div>
  </div>
</div>